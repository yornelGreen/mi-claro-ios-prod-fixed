$(function() {

	// Touch View
	// ---------------
	
	app.views.TouchView = app.views.CommonView.extend({

		name:'touch',
		
		// The DOM events specific.
		events: {
            // events
            'pagecreate':                           'pageCreate',
			'click #cancel':						'cancel',
			'click #next':							'next'
		},

		// Render the template elements        
		render: function(callback) {

			const touch = app.touchType == 'face' ? false : true;
			const username = app.utils.Storage.getLocalItem('username');

			var self = this,
				variables = {
					touch: touch,
					username: username
				};
			
			app.TemplateManager.get(self.name, function(code){
		    	var template = cTemplate(code.html());
		    	$(self.el).html(template(variables));	
		    	callback();	
		    	return this;
		    });
            $(document).scrollTop();
		},

        pageCreate: function(e) {
            var self = this;
            $('#btn-back').hide();
        },

		next: function () {
			var self = this;

			var text = '';
			var textError = '';
			if (app.touchType == 'face') {
				text = 'Ponga su rostro por favor';
				textError = 'Por favor coloque el rostro que usa para desbloquear su dispositivo, ' +
					'de caso contrario seleccione cancelar para desactivar el Face ID.'
			} else {
				text = 'Ponga su huella dactilar por favor';
				textError = 'Por favor coloque la huella que usa para desbloquear su dispositivo, ' +
					'de caso contrario seleccione cancelar para desactivar el Touch ID.'
			}
			window.plugins.touchid.verifyFingerprint(
				// this will be shown in the native scanner popup
				text,
				// success handler: fingerprint accepted
				function(msg) {
					app.utils.Storage.setLocalItem('touch-id-enabled', true);
					self.navigateHome();
				},
				// error handler with errorcode and localised reason
				function(msg) {
					app.utils.Storage.setLocalItem('touch-id-enabled', false);
					showAlert('', textError, 'Ok');
				}
			);
		},

		cancel: function () {
			app.utils.Storage.setLocalItem('touch-id-enabled', false);
			this.navigateHome();
		}
	
	});
});
