$(function() {

    // Service View
    // ---------------

    app.views.ConsumptionView = app.views.CommonView.extend({

        name: 'consumption',

        consumptionSwiper: null,

        minute: true,

        data: false,

        // Events declarations
        //
        events: {

            // events
            'pagecreate': 'pageCreate',
            'active': 'active',

            // header
            'click .btn-back': 'back',
            'click .btn-menu': 'menu',
            'click .btn-chat': 'chat',

            // content
            'change #select-account': 'changeAccountInfo',
            'click .btn-minute': 'showMinute',
            'click .btn-data': 'showData',
            'click .btn-sms-mms': 'showSmsMms',
            'click .select-subscriber': 'changeSubscriber',
            'click .manage-notifications': 'notifications',
            'click #btn-add-aditional-data': 'aditionalDataPlan',
            'click .consumption-limit': 'consumptionLimit',

            // toggle
            'click .sectbar': 'toggleClass',
            'click .phonebar': 'toggleClass',

            // menu
            'click .btn-account': 'account',
            'click .btn-consumption': 'consumption',
            'click .btn-service': 'service',
            'click .btn-change-plan': 'changePlan',
            'click .btn-add-aditional-data': 'aditionalDataPlan',
            'click .btn-device': 'device',
            'click .btn-profile': 'profile',
            'click .btn-gift': 'giftSend',
            'click .btn-invoice': 'invoice',
            'click .close-menu': 'closeMenu',
            'click .open-menu': 'openMenu',
            'click .btn-logout': 'logout',
            'click .btn-sva': 'sva',
            'click .btn-gift-send-recharge': 'giftSendRecharge',
			'click .btn-my-order': 'myOrder',
			'click .btn-my-store': 'myStore',

            // footer
            'click #btn-help': 'helpSection'

        },

        // Render the template elements
        //
        render: function(callback) {
            var self = this,
                variables = null,
                firstOffer = '',
                usageOfferDataList = null,
                usage = app.utils.Storage.getSessionItem('usage'),
                selectedAccount = app.utils.Storage.getSessionItem('selected-account');

            // user hasn't logged in
            if (app.utils.Storage.getSessionItem('token') == null) {

                document.location.href = 'index.html';

            } else {

                var subscribers = app.utils.Storage.getSessionItem('subscribers');
                var init = moment(usage.cicleDateStart, 'MM/DD/YYYY'),
                    end = moment(usage.cicleDateEnd, 'MM/DD/YYYY'),
                    totalDays = end.diff(init, 'days'),
                    cicleDaysPast = moment().diff(init, 'days'),
                    cicleDaysLeft = end.diff(moment(), 'days');

                usage.porcDaysPast = (cicleDaysPast * 100) / totalDays;
                usage.cicleDaysLeft = cicleDaysLeft;

                // update usage
                app.utils.Storage.setSessionItem('usage', usage);

                var usageOfferDataList = app.utils.Storage.getSessionItem('selected-subscriber').UsageOfferDataList;

                if (usageOfferDataList.length > 0) {
                    var firstOffer = usageOfferDataList.shift();
                }

                variables = {
                    accounts: app.utils.Storage.getSessionItem('accounts-list'),
                    selectedAccount: app.utils.Storage.getSessionItem('selected-account'),
                    subscribers: subscribers,
                    usage: app.utils.Storage.getSessionItem('usage'),
                    wirelessAccount: (selectedAccount.prodCategory == 'WLS'),
                    mainPlan: firstOffer,
                    usage: app.utils.Storage.getSessionItem('usage'),
                    usageOfferDataList: usageOfferDataList,
                    selectedSubscriberValue: app.utils.Storage.getSessionItem('selected-subscriber-value'),
                    planAttributes: {
                        hasSharedDataPlan: (subscribers[0].GroupId != null && subscribers[0].GroupId != '')
                    },
                    typeOfTelephony: app.utils.tools.typeOfTelephony,
                    subscribersCount: subscribers.length,
                    showBackBth: true
                };


                app.TemplateManager.get(self.name, function(code) {
                    var template = cTemplate(code.html());
                    $(self.el).html(template(variables));
                    callback();
                    return this;
                });
            }
        },

        _updateCircles: function() {
            if (this.consumptionSwiper) {
                // Get the active slide and set the active circle to the corresponding index
                var activeIndex = this.consumptionSwiper.activeIndex;

                var circles = $.mobile.activePage.find('.circle');
                circles.removeClass('pager-item-on').addClass('pager-item-off');
                circles.eq(activeIndex).addClass('pager-item-on').removeClass('pager-item-off');
            }
        },

        // Launch when the page is rendered
        //
        pageCreate: function(e) {
            var self = this;
            var subscribers = app.utils.Storage.getSessionItem('subscribers');
            if (subscribers.length == 1) {
                $('.select-subscriber').eq(0).trigger('click');
            }
        },

        // Change account information
        //
        changeAccountInfo: function() {
            var self = this,
                accountNumber = null,
                analytics = null;

            app.utils.Storage.setSessionItem('selected-account-value', $.mobile.activePage.find('#select-account').val());
            accountNumber = app.utils.Storage.getSessionItem('selected-account-value');

            if (analytics != null) {
                // send GA statistics
                analytics.trackEvent('select', 'change', 'select account', accountNumber);
            }

            $.each(app.utils.Storage.getSessionItem('accounts-list'), function(index, value) {
                if (value.Account == app.utils.Storage.getSessionItem('selected-account-value')) {
                    app.utils.Storage.setSessionItem('selected-account', value);
                }
            });

            if (app.utils.Storage.getSessionItem('subscribers-' + accountNumber) == null) {

                self.options.accountModel.getAccountSubscribers(
                    //parameter
                    app.utils.Storage.getSessionItem('token'),
                    accountNumber,

                    // success callback
                    function(data) {
                        console.log('#success ws service');

                        if (!data.HasError) {

                            app.utils.Storage.setSessionItem('subscribers', data.Subscribers);
                            app.utils.Storage.setSessionItem('subscriber', data.Subscribers[0].subscriber);

                            // default subscriber
                            app.utils.Storage.setSessionItem('selected-subscriber-value', data.Subscribers[0].subscriber);

                            // cache
                            //app.cache.Subscribers[accountNumber] = data.Subscribers;
                            app.utils.Storage.setSessionItem('subscribers-' + accountNumber, data.Subscribers);

                            self.options.accountModel.getAccountUsage(
                                //parameters
                                app.utils.Storage.getSessionItem('token'),
                                accountNumber,
                                app.utils.Storage.getSessionItem('subscriber'),

                                //success callback
                                function(data) {

                                    // destroy the swiper
                                    /*if (self.consumptionSwiper != null) {
                                        self.consumptionSwiper.destroy();
                                        self.consumptionSwiper = null;
                                    }*/

                                    //app.session.Usage = data;
                                    app.utils.Storage.setSessionItem('usage', data);

                                    self.render(function() {
                                        $.mobile.activePage.trigger('pagecreate');
                                    });
                                },

                                // error function
                                app.utils.network.errorFunction
                            );

                        } else {
                            showAlert('Error', data.Desc, 'Aceptar');
                        }
                    },

                    // error function
                    app.utils.network.errorFunction
                );

            } else {
                //cache
                var subscribers = app.utils.Storage.getSessionItem('subscribers-' + accountNumber);
                app.utils.Storage.setSessionItem('subscribers', subscribers);

                // default subscriber
                app.utils.Storage.setSessionItem('selected-subscriber', subscribers[0]);
                app.utils.Storage.setSessionItem('selected-subscriber-value', subscribers[0].subscriber);

                self.options.accountModel.getAccountUsage(
                    //parameters
                    app.utils.Storage.getSessionItem('token'),
                    accountNumber,
                    subscribers[0].subscriber,

                    //success callback
                    function(data) {
                        // destroy the swiper
                        /*if (self.consumptionSwiper != null) {
                            self.consumptionSwiper.destroy();
                            self.consumptionSwiper = null;
                        }*/

                        //app.session.Usage = data;
                        app.utils.Storage.setSessionItem('usage', data);

                        self.render(function() {
                            $.mobile.activePage.trigger('pagecreate');
                        });
                    },

                    // error function
                    app.utils.network.errorFunction
                );
            }
        },

        // Show minute usage information
        //
        showMinute: function(e) {

            var activeSlide = $('#phone' + $(e.currentTarget).data('index')); //this.consumptionSwiper.activeSlide();

            // Hide the shared data buttons
            // http://taiga.e4gslab.com:8000/project/dthompson-miclaro-pcrf/us/5?kanban-status=9
            $('#shared-data-buttons-container').hide();

            // change buttom
            $(activeSlide).find('.btn-consumption-option').removeClass('on');

            $(activeSlide).find('.btn-minute').addClass('on');

            // hide others consumption usage sections
            $(activeSlide).find('.consumption-content').hide();

            // Hide empty data
            $('.consumption-empty-data').hide();

            // Show minute usage
            $(activeSlide).find('.consumption-minutes-content').show();

            // flat
            //activeSlide.data('minute', true);

            //activeSlide.data('data', false);

            //activeSlide.data('sms', false);

            // send GA statistics FIXME: Uncoment
            //analytics.trackEvent('button', 'click', 'minutes information button');

        },

        showSmsMms: function(e) {

            var self = this,
                activeSlide = $('#phone' + $(e.currentTarget).data('index')); //this.consumptionSwiper.activeSlide();

            // Hide the shared data buttons
            // http://taiga.e4gslab.com:8000/project/dthompson-miclaro-pcrf/us/5?kanban-status=9
            $('#shared-data-buttons-container').hide();

            // change buttom
            $(activeSlide).find('.btn-consumption-option').removeClass('on')

            $(activeSlide).find('.btn-sms-mms').addClass('on');

            // hide others consumption usage sections
            $(activeSlide).find('.consumption-content').hide();

            // Show minute usage
            $(activeSlide).find('.consumption-sms-mms-content').show();

            // Hide empty data
            $('.consumption-empty-data').hide();

            // flat
            //$(activeSlide).data('minute', false);

            //$(activeSlide).data('data', false);

            //$(activeSlide).data('sms', true);

        },

        // Show data usage information
        //
        showData: function(e) {

            var usage = app.utils.Storage.getSessionItem('usage'),
                analytics = null,
                activeSlide = $('#phone' + $(e.currentTarget).data('index')); //this.consumptionSwiper.activeSlide();

            // Show the shared data buttons
            // http://taiga.e4gslab.com:8000/project/dthompson-miclaro-pcrf/us/5?kanban-status=9
            $('#shared-data-buttons-container').show();

            $(activeSlide).find('.btn-consumption-option').removeClass('on')

            $(activeSlide).find('.btn-data').addClass('on');

            // hide others consumption usage sections
            $(activeSlide).find('.consumption-content').hide();

            $(activeSlide).find('.consumption-data-content').show();

            if (!usage.Top) {
                // Show empty data
                $(activeSlide).find('.consumption-empty-data').show();
            } else {
                // Hide empty data
                $(activeSlide).find('.consumption-empty-data').hide();
            }

            if (analytics != null) {
                // send GA statistics
                analytics.trackEvent('button', 'click', 'data information button');
            }

            // flat
            //activeSlide.data('minute', false);

            //activeSlide.data('data', true);

            //activeSlide.data('sms', false);

        },

        getSubscriber: function(subscriberValue) {
            var subscribers = app.utils.Storage.getSessionItem('subscribers');

            for (var i = 0; i < subscribers.length; i++) {
                if (subscribers[i].subscriber == subscriberValue) {
                    return subscribers[i];
                }
            }
        },

        _adjustSwiperHeight: function() {
            var contentHeight = $(this.consumptionSwiper.activeSlide()).children().first().height();
            // Hack to obtain the jQuery parent div for the slide
            var $div = $(this.consumptionSwiper.activeSlide()).children().first().parent();
            while (!$div.hasClass('swiper-container')) {
                $div.height(contentHeight);
                $div = $div.parent();
            }
            $div.height(contentHeight);
        },

        percentUsageAsWidth: function(usageRatio) {
            var percentage = 100.0 * usageRatio;
            return parseInt(Math.round(percentage)) + '%';
        },

        changeSubscriber: function(e) {

            var subscriberValue = $(e.currentTarget).data('subscriber'),
                currentIndex = $(e.currentTarget).data('index'),
                subscribers = app.utils.Storage.getSessionItem('subscribers'),
                usage = app.utils.Storage.getSessionItem('usage'),
                activeSubscriber,
                usageActiveSubscriber,
                mainPlan,
                aditionalDataPlan = '',
                wirelessAccount = app.utils.Storage.getSessionItem('selected-account').prodCategory == 'WLS',
                planAttributes = {
                    hasSharedDataPlan: (subscribers[0].GroupId != null && subscribers[0].GroupId != '')
                },
                self = this;

            $.each(subscribers, function(index, subscriber) {
                if (subscriber.subscriber == subscriberValue) {
                    activeSubscriber = subscriber;
                    usageActiveSubscriber = activeSubscriber.UsageOfferDataList;
                    console.log(activeSubscriber);
                    return;
                }
            });

            if ($(e.currentTarget).data('search-info') == true) {
                // set flag search data
                $(e.currentTarget).data('search-info', false);
            } else {
                // set flag search data
                $(e.currentTarget).data('search-info', true);

                self.options.accountModel.getAccountUsage(

                    //parameters
                    app.utils.Storage.getSessionItem('token'),
                    app.utils.Storage.getSessionItem('selected-account-value'),
                    subscriberValue,

                    //success callback
                    function(data) {

                        if (!data.HasError) {

                            var init = moment(usage.cicleDateStart, 'MM/DD/YYYY'),
                                end = moment(usage.cicleDateEnd, 'MM/DD/YYYY'),
                                totalDays = end.diff(init, 'days'),
                                cicleDaysPast = moment().diff(init, 'days'),
                                cicleDaysLeft = end.diff(moment(), 'days');

                            data.porcDaysPast = (cicleDaysPast * 100) / totalDays;
                            data.cicleDaysLeft = cicleDaysLeft;

                            // update usage
                            app.utils.Storage.setSessionItem('usage', data);

                            mainPlan = usageActiveSubscriber[0];

                            // find subscriber
                            var subscriber = self.getSubscriber(subscriberValue);

                            // update the subscriber
                            app.utils.Storage.setSessionItem('selected-subscriber-value', subscriber);

                            if (usageActiveSubscriber.length > 0) {
                                var firstOffer = usageActiveSubscriber.shift();
                                var mainUsageRatio = firstOffer.Used / firstOffer.Quota,
                                    percentage = 100.0 * mainUsageRatio,
                                    widthSize = parseInt(Math.round(percentage)) + '%';
                            }

                            var htmlMinutes = '';
                            htmlMinutes += '<div class="autobar">' +
                                '<div class="container">' +
                                '<div class="basicrow f-little din-b f-gray">Minutos Usados</div>' +

                                '<div class="basicrow m-top-ii">' +
                                '<div class="contspace full f-med f-black text-justify f-bold">' +
                                +data.MinutesCount +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +

                                '<div class="autobar">' +
                                '<div class="container">' +
                                '<div class="basicrow f-little din-b f-gray">Larga Distancia</div>' +

                                '<div class="basicrow m-top-ii">' +
                                '<div class="contspace full f-med f-black text-justify f-bold">' +
                                +data.LDQty +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +

                                '<div class="autobar">' +
                                '<div class="container">' +
                                '<div class="basicrow f-little din-b f-gray">Larga Distancia Internacional</div>' +

                                '<div class="basicrow m-top-ii">' +
                                '<div class="contspace full f-med f-black text-justify f-bold">' +
                                +data.LDIQty +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>';

                            $('#minutesInformation' + currentIndex).html(htmlMinutes);

                            var htmlSmsMms = '';

                            if (wirelessAccount) {
                                htmlSmsMms += '<div class="autobar">' +
                                    '<div class="container">' +
                                    '<div class="basicrow f-little din-b f-gray">SMS</div>' +

                                    '<div class="basicrow m-top-ii">' +
                                    '<div class="contspace full f-med f-black text-justify f-bold">' +
                                    data.SMSQty +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +

                                    '<div class="autobar">' +
                                    '<div class="container">' +
                                    '<div class="basicrow f-little din-b f-gray">SMS Premium</div>' +

                                    '<div class="basicrow m-top-ii">' +
                                    '<div class="contspace full f-med f-black text-justify f-bold">' +
                                    data.SMSPremiumQty +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +

                                    '<div class="autobar">' +
                                    '<div class="container">' +
                                    '<div class="basicrow f-little din-b f-gray">MMS</div>' +

                                    '<div class="basicrow m-top-ii">' +
                                    '<div class="contspace full f-med f-black text-justify f-bold">' +
                                    data.TextMessageCount +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>';
                            } else {
                                htmlSmsMms += '<div class="nbott din-b f-little text-center f-red"><br>Informaci&oacute;n no disponible para servicios de telefon&iacute;a fija.<br><br></div>';
                            }

                            $('#smsMmsInformation' + currentIndex).html(htmlSmsMms);

                            var htmlData = '';

                            if (wirelessAccount) {
                                htmlData += '<div class="autobar">' +
                                    '<div class="container">' +
                                    '<div class="basicrow text-center">' +
                                    '<span class="f-little f-dgray din-b">Ciclo de Facturaci&oacute;n</span><br/>' +
                                    '<span class="f-med f-black din-b">' + data.CicloFact + '</span><br/><!-- <div class="radius-a consmpt-gray cicle-progress-bar" style="width: <%= usage.porcDaysPast %>%;"></div> -->' +
                                    '<div class="c100 p' + Math.round(data.porcDaysPast) + ' text-center center vcenter">' +
                                    '<span class="change f-black">' +
                                    '<span class="f-little">Quedan</span>' +
                                    '<span class="f-big-m din-b">' + data.cicleDaysLeft + '</span>' +
                                    '<span class="f-little">D&iacute;as</span>' +
                                    '</span>' +
                                    '<div class="slice">' +
                                    '<div class="bar"></div>' +
                                    '<div class="fill"></div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>';

                                htmlData += '<div class="autobar">' +
                                    '<div class="container">';

                                if (mainPlan) {

                                    htmlData += '<div class="basicrow f-little din-b f-gray">Consumo Actual</div>' +

                                        '<div class="basicrow m-top-ii">' +
                                        '<div class="contspace full f-med f-black text-left f-bold">' +
                                        mainPlan.DisplayName +
                                        '</div>' +
                                        '</div>';

                                    var mainUsageRatio = mainPlan.Used / mainPlan.Quota;

                                    htmlData += '<div class="basicrow m-top-ii">' +
                                        '<div class="contspace full f-med f-dgray text-justify f-bold">' +
                                        mainPlan.UsedText + ' de ' + mainPlan.QuotaText + ' usados' +
                                        '</div>' +
                                        '</div>' +

                                        '<div class="basicrow m-top-ii">' +
                                        '<div class="consmpt-bar">';
                                    /*if (parseInt(subscriber.PUJ_USED) >= parseInt(subscriber.PUJ_LIMIT)) {
                                        htmlData += '<div class="consmpt-in data-progress-bar" style="width:75%"></div>';
                                    } else {
                                        htmlData += '<div class="consmpt-in data-progress-bar" style="width: '+ self.percentUsageAsWidth(mainUsageRatio) +'"></div>';
                                    }*/

                                    if (mainUsageRatio <= 0.8) {
                                        htmlData += '<div class="consmpt-green" style="width: ' + self.percentUsageAsWidth(mainUsageRatio) + '"></div>';
                                    } else if (mainUsageRatio > 0.8 && mainUsageRatio < 0.9) {
                                        htmlData += '<div class="consmpt-yellow" style="width: ' + self.percentUsageAsWidth(mainUsageRatio) + '"></div>';
                                    } else if (mainUsageRatio >= 0.9 && mainUsageRatio < 1.0) {
                                        htmlData += '<div class="consmpt-in " style="width: ' + self.percentUsageAsWidth(mainUsageRatio) + '"></div>';
                                    } else if (mainUsageRatio > 1.0) {
                                        mainUsageRatio = 1.0;
                                        htmlData += '<div class="consmpt-in" style="width: ' + self.percentUsageAsWidth(mainUsageRatio) + '"></div>';
                                    }

                                    htmlData += '</div>' +
                                        '</div>';
                                } else {

                                    htmlData += '<div class="basicrow f-little din-b f-gray">Consumo Actual</div>' +

                                        '<div class="basicrow m-top-ii">' +
                                        '<div class="contspace full f-med f-black text-justify f-bold">' +
                                        usage.DPlanName +
                                        '</div>' +
                                        '</div>' +

                                        '<div class="basicrow m-top-ii">' +
                                        '<div class="contspace full f-med f-dgray text-justify f-bold">' +
                                        usage.DLocal + ' de ' + usage.DLoaclTop + ' U ' +
                                        '</div>' +
                                        '</div>';

                                    var mainUsageRatio = mainPlan.Used / mainPlan.Quota;

                                    htmlData += '<div class="basicrow m-top-ii">' +
                                        '<div class="consmpt-bar">';
                                    /*if (parseInt(subscriber.PUJ_USED) >= parseInt(subscriber.PUJ_LIMIT)) {
                                        htmlData += '<div class="consmpt-in data-progress-bar" style="width:75%"></div>';
                                    } else {
                                        htmlData += '<div class="consmpt-in data-progress-bar" style="width: '+ usage.DLoaclPorc.toString() +'%' +'"></div>';
                                    }*/

                                    if (mainUsageRatio <= 0.8) {
                                        htmlData += '<div class="consmpt-green" style="width: ' + mainUsageRatio.toString() + '%' + '"></div>';
                                    } else if (mainUsageRatio > 0.8 && mainUsageRatio < 0.9) {
                                        htmlData += '<div class="consmpt-yellow" style="width: ' + mainUsageRatio.toString() + '%' + '"></div>';
                                    } else if (mainUsageRatio >= 0.9 && mainUsageRatio < 1.0) {
                                        htmlData += '<div class="consmpt-in" style="width: ' + mainUsageRatio.toString() + '%' + '"></div>';
                                    } else if (mainUsageRatio > 1.0) {
                                        mainUsageRatio = 1.0;
                                        htmlData += '<div class="consmpt-in" style="width: ' + mainUsageRatio.toString() + '%' + '"></div>';
                                    }

                                    htmlData += '</div>' +
                                        '</div>';
                                }

                                htmlData += '</div>' +
                                    '</div>' +

                                    '<div class="autobar">' +
                                    '<div class="container">' +
                                    '<div class="basicrow f-little din-b f-gray" style="display:none">Consumo por suscriptor</div>' +
                                    '<div class="content-aditional-data-package">';

                                if (usageActiveSubscriber != null && usageActiveSubscriber.length > 0) {

                                    htmlData += '<div class="basicrow f-little din-b f-gray aditional-package-title">Consumo de Paquetes Adicionales</div>';

                                    $.each(usageActiveSubscriber, function(index, usageOfferData) {

                                        htmlData += '<div class="basicrow m-top-ii">' +
                                            '<div class="contspace full f-med f-black text-justify f-bold">' +
                                            usageOfferData.DisplayName +
                                            '</div>' +
                                            '</div>' +

                                            '<div class="basicrow m-top-ii">' +
                                            '<div class="contspace full f-med f-dgray text-justify f-bold">' +
                                            usageOfferData.UsedText + ' de ' + usageOfferData.QuotaText + ' usados' +
                                            '</div>' +
                                            '</div>';

                                        var usageRatio = usageOfferData.Used / usageOfferData.Quota;

                                        htmlData += '<div class="basicrow m-top-ii">' +
                                            '<div class="consmpt-bar">';
                                        /*if (usageRatio <= 0.8) {
                                            htmlData += '<div class="filler-green" style="width: '+ self.percentUsageAsWidth(usageRatio) +'"></div>';
                                        } else if (usageRatio > 0.8 && usageRatio < 0.9)  {
                                            htmlData += '<div class="filler-yellow" style="width: '+ self.percentUsageAsWidth(usageRatio) +'"></div>';
                                        } else {
                                            htmlData += '<div class="consmpt-in" style="width: '+ self.percentUsageAsWidth(usageRatio) +'"></div>';
                                        }*/

                                        if (usageRatio <= 0.8) {
                                            htmlData += '<div class="consmpt-green" style="width: ' + self.percentUsageAsWidth(usageRatio) + '"></div>';
                                        } else if (usageRatio > 0.8 && usageRatio < 0.9) {
                                            htmlData += '<div class="consmpt-yellow" style="width: ' + self.percentUsageAsWidth(usageRatio) + '"></div>';
                                        } else if (usageRatio >= 0.9 && usageRatio < 1.0) {
                                            htmlData += '<div class="consmpt-in" style="width: ' + self.percentUsageAsWidth(usageRatio) + '"></div>';
                                        } else if (usageRatio > 1.0) {
                                            usageRatio = 1.0;
                                            htmlData += '<div class="consmpt-in" style="width: ' + self.percentUsageAsWidth(usageRatio) + '"></div>';
                                        }

                                        htmlData += '</div>' +
                                            '</div>';

                                    });
                                }

                                htmlData += '</div>';

                                if (planAttributes.hasSharedDataPlan) {

                                    htmlData += '<div class="basicrow f-little din-b f-gray">Consumo por suscriptor</div>';

                                    $.each(subscribers, function(index, subscriber) {
                                        htmlData += '<div class="tabcont-e border-b">' +
                                            '<div class="basicrow f-bold f-graydark">' +
                                            '</div>' +

                                            '<div class="pill-tankcont">' +
                                            '<div class="pill-tank-text">' +
                                            subscriber.subscriber +
                                            '</div>' +

                                            '<div class="pill-tank-b">' +
                                            //'<div class="filler-green"></div>'+
                                            '<div class="consmpt-green"></div>' +
                                            '</div>' +
                                            '</div>' +

                                            '<div class="basicrow text-right"></div>' +
                                            '</div>';
                                    });
                                }

                                htmlData += '</div>' +
                                    '</div>' +
                                    '</div>';

                            }
                            /*else {
                                                           //htmlData += '<div class="nbott din-b f-little text-center f-red">Informaci&oacute;n no disponible para servicios de telefon&iacute;a fija.</div>';
                                                           htmlData += '<div class="nbott din-b f-little text-center f-red"><br>Informaci&oacute;n no disponible para servicios de telefon&iacute;a fija.<br><br></div>';
                                                       }*/

                            $('#dataInformation' + currentIndex).html(htmlData);

                        } else {
                            showAlert('Error', data.Desc, 'Aceptar');
                        }

                    },

                    // error function
                    app.utils.network.errorFunction
                );

            }

        },

        notifications: function(e) {
            app.router.navigate('manage_notifications', {
                trigger: true,
                replace: true
            });
        },

        aditionalDataPlan: function(e) {
            app.router.navigate('data_plan', {
                trigger: true,
                replace: true
            });
        },

        consumptionLimit: function(e) {
            app.router.navigate('consumption_limit', {
                trigger: true,
                replace: true
            });
        }
    });
});
