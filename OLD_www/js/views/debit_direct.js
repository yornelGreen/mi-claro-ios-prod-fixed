$(function() {

    // Debit Direct View
    // ---------------

    app.views.DebitDirectView = app.views.CommonView.extend({

        name: 'debit_direct',

        // The DOM events specific.
        events: {

            // event
            'pagecreate': 'pageCreate',
            'active': 'active',

            //header
            'click .btn-back': 'back',

            // content
            'change #select-account': 'changeAccountInfo',
            'click .payment-step-2': 'goToPaymentStep2',
            'click .btn-card': 'selectCreditCard',
            'focusout #credit-card': 'updateCreditCard',
            'focusin #card_expiration_month': 'hideFooter',
            'focusout #card_expiration_month': 'showFooter',
            'focusin #card_expiration_year': 'hideFooter',
            'focusout #card_expiration_year': 'showFooter',
            'click select': 'fixedSelectInput',
            'change #debit_direct_type': 'changeDebitDirect',
            'click select': 'fixedSelectInput',
            'click #continue-1': 'goToPaymentStep2',
            'click #continue-2': 'goToPaymentStep2',
            'click #cancel-1': 'cancelDebitDirect',
            'click #editform': 'showFormToEdit',

            // menu
            'click .btn-account': 'account',
            'click .btn-consumption': 'consumption',
            'click .btn-service': 'service',
            'click .btn-change-plan': 'changePlan',
            'click .btn-add-aditional-data': 'aditionalDataPlan',
            'click .btn-device': 'device',
            'click .btn-profile': 'profile',
            'click .btn-gift': 'giftSend',
            'click .btn-invoice': 'invoice',
            'click .close-menu': 'closeMenu',
            'click .open-menu': 'openMenu',
            'click .btn-logout': 'logout',
            'click .btn-notifications': 'notifications',
            'click .btn-sva': 'sva',
            'click .btn-gift-send-recharge': 'giftSendRecharge',
			'click .btn-my-order': 'myOrder',
			'click .btn-my-store': 'myStore',

            // footer
            'click #btn-help': 'helpSection'

        },

        // Render the template elements
        render: function(callback) {
            var now = moment(),
                months = [],
                years = [];

            // Years
            for (var i = 0; i < 10; i++) {
                years.push(now.format('YYYY'));
                now.add(1, 'years');
            }

            // Months
            for (var j = 1; j <= 12; j++) {
                months.push((j < 10) ? '0' + j : j);
            }
            
            var banksCode = new Object(); 
            banksCode["021502011"] = {"bank": "Banco Popular of Puerto Rico"};
            banksCode["021502095"] = {"bank": "Banco Popular of Puerto Rico"};
            banksCode["021502341"] = {"bank": "Banco Santander of Puerto Rico"};
            banksCode["221571473"] = {"bank": "Firstbank, P.R."};
            banksCode["021502804"] = {"bank": "Scotiabank of Puerto Rico"};
            banksCode["221571415"] = {"bank": "Oriental Bank"};
            banksCode["221582284"] = {"bank": "COOP. MANATI"};
            banksCode["021582785"] = {"bank": "COOP. ISABELA"};
            banksCode["021583658"] = {"bank": "COOP.ENERGIA ELECTRICA"};
            banksCode["021582727"] = {"bank": "COOP. SAGRADA FAMILIA"};
            banksCode["021582853"] = {"bank": "COOP. LAS PIEDRAS"};
            banksCode["221582226"] = {"bank": "COOP. CAMUY"};
            banksCode["221581706"] = {"bank": "COOP. MOCA"};
            banksCode["021583247"] = {"bank": "COOP. SABANENA"};
            banksCode["221582459"] = {"bank": "COOP.LA PUERTORRIQUENA"};
            banksCode["267089712"] = {"bank": "FIRST BANK US V.G."};
            banksCode["221581641"] = {"bank": "COOP. ARECIBO"};
            banksCode["021582743"] = {"bank": "COOP. SAN JOSE"};
            banksCode["021584217"] = {"bank": "COOP. LOS HERMANOS"};
            banksCode["021582976"] = {"bank": "COOP. AGUADA"};
            banksCode["021583373"] = {"bank": "COOP.BARRIO QUEBRADA"};
            banksCode["021583593"] = {"bank": "COOP. VILLALBA"};
            banksCode["021583137"] = {"bank": "COOP. ANASCO"};
            banksCode["021583360"] = {"bank": "COOP. MAYAGUEZ"};
            banksCode["221672851"] = {"bank": "FIRST BANK US V.I."};
            banksCode["221581751"] = {"bank": "COOP. VALENCIANO"};
            banksCode["221581722"] = {"bank": "COOP. MAUNABO"};
            banksCode["221582077"] = {"bank": "COOP. JAYUYA"};
            banksCode["021583111"] = {"bank": "COOP. SAN BLAS"};
            banksCode["021583140"] = {"bank": "COOP. NAGUABENA"};
            banksCode["021582662"] = {"bank": "COOP. CAGUAS"};
            banksCode["021583328"] = {"bank": "COOP. CAPARRA"};
            banksCode["021583056"] = {"bank": "COOP. CIALES"};
            banksCode["021583027"] = {"bank": "COOP. VEGA ALTA"};
            banksCode["021582756"] = {"bank": "COOP. AGUAS BUENAS"};
            banksCode["221581748"] = {"bank": "COOP. ORIENTAL"};
            banksCode["221582310"] = {"bank": "COOP. CABO ROJO"};
            banksCode["021582701"] = {"bank": "COOP. GURABO"};
            banksCode["021583483"] = {"bank": "COOP. PARROCOOP"};
            banksCode["221582174"] = {"bank": "COOP. YAUCO"};
            banksCode["021502040"] = {"bank": "CITIBANK"};
            banksCode["221582323"] = {"bank": "HATICOOP"};
            
            var self = this,
                variables = {
                    accounts: app.utils.Storage.getSessionItem('accounts-list'),
                    selectedAccount: app.utils.Storage.getSessionItem('selected-account'),
                    selectedAccountValue: app.utils.Storage.getSessionItem('selected-account-value'),
                    paymentData: app.utils.Storage.getSessionItem('payment-data'),
                    debitDirectData: app.utils.Storage.getSessionItem('pay-debit-direct'),
                    debitDirectDataEdit: app.utils.Storage.getSessionItem('pay-debit-direct-edit'),
                    selectedOffer: app.utils.Storage.getSessionItem('selected-offer'),
                    typeDebitDirect: app.utils.Storage.getSessionItem('selected-debit-direct-type'),
                    months: months,
                    years: years,
                    wirelessAccount: (selectedAccount.prodCategory == 'WLS'),
                    showBackBth: true,
                    banksCodeList: banksCode,
                    methodsType: {
                        'VS':'Visa',
                        'MC':'Master',
                        'AM':'Amex',
                    }
                };

                                                            
            app.TemplateManager.get(self.name, function(code) {
                var template = cTemplate(code.html());
                $(self.el).html(template(variables));
                callback();
                return this;
            });

        },

        changeAccountInfo: function(e) {
            
            var self = this;
            app.utils.Storage.setSessionItem('selected-account-value', $.mobile.activePage.find('#select-account').val());
            $(this).blur();

            $.each(app.utils.Storage.getSessionItem('accounts-list'), function(index, value) {
                if (value.Account == app.utils.Storage.getSessionItem('selected-account-value')) {
                    app.utils.Storage.setSessionItem('selected-account', value);
                }
            });
            // Clear Debit Direct info
            console.log("Elimino data de pay-debit-direct");
            app.utils.Storage.removeSessionItem('pay-debit-direct');

            var accountModel = new app.models.Account();

            //data invoice
            accountModel.getDirectDebitInfo(

                //parameters
                app.utils.Storage.getSessionItem('token'),
                data = {
                    'accountNumber': app.utils.Storage.getSessionItem('selected-account-value'),
                },
                //success callback
                function(data) {
                    // CORREGIR PARAM HAS ERROR AL DE MAYUSCULA
                    // if (!data.HasError) {
                    if (!data.HasError) {
                        // Falta logica para un caso que no devuelva informacion

                        console.log(data.mType);
                        var bool = (data.mType != "" && data.mType != null)? true : false;
                        // False -> No se tiene Data, entro en Form
                        // True -> Se tiene data y muestro contenido
                        // var bool = true;

                        app.utils.Storage.setSessionItem('pay-debit-direct-edit', bool);
                        // Pendiente
                        // Data Test de un Update
                        console.log("Llamada al servicio getDirectDebitInfo");
                        var paydirect = { 
                            "ErrorDesc": "",
                            "ErrorNum": 0,
                            "ErrorType": "",
                            "HasError": false,
                            "mAccountNo": "",
                            "mBankCode": "",
                            "mCreditCardExpDate": "2019-06-01T00:00:00.000-04:00",
                            "mCreditCardMemberName": "Alex Tejena",
                            "mCreditCardNo": "0136",
                            "mCreditCardType": "VS",
                            "mEndDate": "",
                            "mReason": "",
                            "mStartDate": "2018-12-05T00:00:00.000-04:00",
                            "mStatus": "A",
                            "mType": "CC",
                            "paymentid": "",
                        };

                        var payCC = {
                            "ErrorDesc": "",
                            "ErrorNum": 0,
                            "ErrorType": "",
                            "HasError": false,
                            "mAccountNo": "",
                            "mBankCode": "",
                            "mCreditCardExpDate": "2019-06-01T00:00:00.000-04:00",
                            "mCreditCardMemberName": "Alex Tejena",
                            "mCreditCardNo": "0136",
                            "mCreditCardType": "VS",
                            "mEndDate": "",
                            "mReason": "",
                            "mStartDate": "2018-12-05T00:00:00.000-04:00",
                            "mStatus": "A",
                            "mType": "CC",
                            "paymentid": "",
                        };

                        var payCH = {
                            "ErrorDesc": "",
                            "ErrorNum": 0,
                            "ErrorType": "",
                            "HasError": false,
                            "mAccountNo": "4444",
                            "mBankCode": "021502095",
                            "mCreditCardExpDate": "",
                            "mCreditCardMemberName": "",
                            "mCreditCardNo": "",
                            "mCreditCardType": "",
                            "mEndDate": "",
                            "mReason": "",
                            "mStartDate": "2018-12-05T00:00:00.000-04:00",
                            "mStatus": "A",
                            "mType": "CH",
                            "paymentid": "",
                        };                    
                        
                        // Cable test
                        // console.log(paydirect);
                        // app.utils.Storage.setSessionItem('pay-debit-direct-edit', true);
                        // app.utils.Storage.setSessionItem('pay-debit-direct', paydirect);

                        app.utils.Storage.setSessionItem('pay-debit-direct', data);

                        self.render(function() {
                            $.mobile.activePage.trigger('pagecreate');
                        });

                    } else {
                        showAlert('Error', data.ErrorDesc, 'Aceptar');
                    }

                },

                // error function
                app.utils.network.errorFunction
            );

        },

        changeDebitDirect: function(e) {
            // Clear Debit Direct info
            console.log("Paso por changeDebitDirect");
            var self = this;
            if ($.mobile.activePage.find('#debit_direct_type').val()) {
                app.utils.Storage.setSessionItem('selected-debit-direct-type', $.mobile.activePage.find('#debit_direct_type').val());
                self.render(function() {
                    $.mobile.activePage.trigger('pagecreate');
                });
            } else {
                showAlert('Error', 'Debe seleccionar el tipo de afiliación a Débito Directo', 'Aceptar');
            }
        },

        goToPaymentStep2: function(e) {
            // add custom validation methods
            var self = this;
            if (app.utils.Storage.getSessionItem('selected-debit-direct-type') == 'CC') {
                $.validator.addMethod('creditCard', function() {

                    var creditCardNumber = $('input[name=card_number]').val(),
                        creditCardType = app.utils.Storage.getSessionItem('credit-card-type');

                    switch (creditCardType) {
                        case 'MC':
                            pattern = /^5[1-5][0-9]{14}$/;
                            break;
                        case 'VS':
                            pattern = /^4[0-9]{12}(?:[0-9]{3})?$/;
                            break;
                        case 'AM':
                            pattern = /^3[4,7][0-9]{13}$/;
                            break;
                        default:
                            return;
                    }
                    console.log(creditCardNumber);
                    console.log(creditCardType);
                    console.log(pattern.test(creditCardNumber));
                    return pattern.test(creditCardNumber);
                });

                $('#form-payment-step-1').validate({
                    ignore: '',
                    rules: {
                        'card_type': {
                            required: true
                        },
                        'full_name': {
                            required: true
                        },
                        'card_number': {
                            required: true,
                            creditCard: true
                        },
                        'card_expiration_month': {
                            required: true,
                            digits: true,
                            range: [1, 12]
                        },
                        'card_expiration_year': {
                            required: true,
                            digits: true
                        },
                        'security_code': {
                            required: true,
                            digits: true,
                            rangelength: [3, 4]
                        }
                    },

                    messages: {
                        'card_type': {
                            required: 'Por favor seleccione un método de pago'
                        },
                        'full_name': {
                            required: 'Por favor indique su nombre y apellido'
                        },
                        'card_number': {
                            required: 'Por favor indique un número de tarjeta válido',
                            creditCard: 'Por favor indique un número de tarjeta válido'
                        },
                        'card_expiration_month': {
                            required: 'Por favor indique el mes de vencimiento de la tarjeta',
                            digits: 'Por favor indique un mes válido',
                            range: 'Por favor indique un mes válido'
                        },
                        'card_expiration_year': {
                            required: 'Por favor indique el año de vencimiento de la tarjeta',
                            digits: 'Por favor indique un año válido'
                        },
                        'security_code': {
                            required: 'Por favor indique un código de verificación válido',
                            digits: 'Por favor indique un código de verificación válido',
                            rangelength: 'Por favor indique un código de verificación válido'
                        }
                    },

                    invalidHandler: function(event, validator) {

                        if (validator.errorList.length > 0) {
                            var err = validator.errorList[0];
                            showAlert('Error', err.message, 'Aceptar');
                            $(err.element).focus();
                        }
                    },

                    submitHandler: function(form) {
                        console.log("submitHandler from CC");
                        var expDate = '20'+$('select[name=card_expiration_year]').val()+'-'+$('select[name=card_expiration_month]').val()+'-01T01:01:01.000-04:00';

                        var actualMonth = moment().format('MM');
                        var actualYear = moment().format('YY');
                        var actualDay = moment().format('DD');

                        var startDate = '20'+actualYear+'-'+actualMonth+'-'+actualDay+'T01:01:01.000-04:00';
                        var requestDirectDebit = '';
                        if(app.utils.Storage.getSessionItem('selected-debit-direct-type') == 'CC'){
                            requestDirectDebit = {
                                "token": app.utils.Storage.getSessionItem('token'),
                                "Ban": ''+app.utils.Storage.getSessionItem('selected-account-value'),
                                "mAccountNo": "",
                                "mBankCode": "021502011",
                                "mCreditCardMemberName": $('input[name=full_name]').val(), 
                                "mCreditCardNo": $('input[name=card_number]').val(), 
                                "mCreditCardType": app.utils.Storage.getSessionItem('credit-card-type'), 
                                "mCreditCardExpDate": expDate,
                                "mReason": "", 
                                "mStartDate": startDate, 
                                "mEndDate": "", 
                                "mStatus": "A", 
                                "mType": app.utils.Storage.getSessionItem('selected-debit-direct-type')
                            };                    
                        }
                        else{
                            requestDirectDebit = {
                                "token": app.utils.Storage.getSessionItem('token'),
                                "Ban": ''+app.utils.Storage.getSessionItem('selected-account-value'),
                                "mAccountNo": ''.$('input[name=account_bank_number]').val(),
                                "mBankCode": $('select[name=bank_select]').val(),
                                "mCreditCardMemberName": "", 
                                "mCreditCardNo": "", 
                                "mCreditCardType": "", 
                                "mCreditCardExpDate": "",
                                "mReason": "", 
                                "mStartDate": startDate, 
                                "mEndDate": "", 
                                "mStatus": "A", 
                                "mType": app.utils.Storage.getSessionItem('selected-debit-direct-type')
                            };                         
                        }
                        console.log(requestDirectDebit);                                            
                        var accountModel = new app.models.Account();                    
                        accountModel.updateDirectDebit(
                            // parameters
                            requestDirectDebit,
                            // success callback
                            function(success) {
                                console.log(success);
                                                        
                                if (!success.HasError) {
                                    app.utils.Storage.setSessionItem('pay-debit-direct', requestDirectDebit);
                                    showAlert('Afiliación exitosa', '', 'Ok');
                                    // render payment step 2
                                    self.render(function() {
                                        $.mobile.activePage.trigger('pagecreate');
                                    });
                                } else {
                                    showAlert('Error', success.ErrorDesc, 'Aceptar');
                                }                                          
                            },
                            // error callback
                            app.utils.network.errorFunction
                        );
                        return false;
                    },
                    showErrors: function(errorMap, errorList) {
                        // next  . . .
                    }
                });

                $('#form-payment-step-1').submit();                
            } else {
                $('#form-account').validate({
                    ignore: '',
                    rules: {
                        'bank_select': {
                            required: true
                        },
                        'account-bank-number': {
                            required: true,
                            digits: true,
                        },
                    },

                    messages: {
                        'bank_select': {
                            required: 'Por favor seleccione su banco'
                        },
                        'account-bank-number': {
                            required: 'Por favor indique el número de cuenta',
                            digits: 'Por favor indique un número de cuenta válido',                            
                        },
                    },

                    invalidHandler: function(event, validator) {

                        if (validator.errorList.length > 0) {
                            var err = validator.errorList[0];
                            showAlert('Error', err.message, 'Aceptar');
                            $(err.element).focus();
                        }
                    },

                    submitHandler: function(form) {
                        console.log("submitHandler from CH");
                        var expDate = '20'+$('select[name=card_expiration_year]').val()+'-'+$('select[name=card_expiration_month]').val()+'-01T01:01:01.000-04:00';

                        
                        var actualMonth = moment().format('MM');
                        var actualYear = moment().format('YY');
                        var actualDay = moment().format('DD');

                        var startDate = '20'+actualYear+'-'+actualMonth+'-'+actualDay+'T01:01:01.000-04:00';
                        var requestDirectDebit = '';
                        if(app.utils.Storage.getSessionItem('selected-debit-direct-type') == 'CC'){
                            requestDirectDebit = {
                                "token": app.utils.Storage.getSessionItem('token'),
                                "Ban": ''+app.utils.Storage.getSessionItem('selected-account-value'),
                                "mAccountNo": "",
                                "mBankCode": "021502011",
                                "mCreditCardMemberName": $('input[name=full_name]').val(), 
                                "mCreditCardNo": $('input[name=card_number]').val(), 
                                "mCreditCardType": app.utils.Storage.getSessionItem('credit-card-type'), 
                                "mCreditCardExpDate": expDate,
                                "mReason": "", 
                                "mStartDate": startDate, 
                                "mEndDate": "", 
                                "mStatus": "A", 
                                "mType": app.utils.Storage.getSessionItem('selected-debit-direct-type')
                            };                    
                        }
                        else{
                            requestDirectDebit = {
                                "token": app.utils.Storage.getSessionItem('token'),
                                "Ban": ''+app.utils.Storage.getSessionItem('selected-account-value'),
                                "mAccountNo": $('input[name=account_bank_number]').val(),
                                "mBankCode": $('select[name=bank_select]').val(),
                                "mCreditCardMemberName": "",
                                "mCreditCardNo": "",
                                "mCreditCardType": "",
                                "mCreditCardExpDate": "",
                                "mReason": "",
                                "mStartDate": startDate,
                                "mEndDate": "",
                                "mStatus": "A", 
                                "mType": app.utils.Storage.getSessionItem('selected-debit-direct-type')
                            };                         
                        }
                        console.log(requestDirectDebit);  

                        // var data1 = "hola";                                        
                        
                        var accountModel = new app.models.Account();                    
                        accountModel.updateDirectDebit(
                            // parameters
                            requestDirectDebit,
                            // data1,
                            // success callback
                            function(success) {
                                console.log(success);
                                                        
                                if (!success.HasError) {
                                    app.utils.Storage.setSessionItem('pay-debit-direct', requestDirectDebit);
                                    showAlert('Afiliación exitosa', '', 'Ok');
                                    // render payment step 2
                                    self.render(function() {
                                        $.mobile.activePage.trigger('pagecreate');
                                    });
                                } else {
                                    showAlert('Error', success.ErrorDesc, 'Aceptar');
                                }                                          
                            },
                            // error callback
                            app.utils.network.errorFunction
                        );
                        return false;
                    },
                    showErrors: function(errorMap, errorList) {
                        // next  . . .
                    }
                });
                $('#form-account').submit();
            }
        },

        cancelDebitDirect: function(e) {
            // Mismos datos recibidos del get se pasan al Cancell
            var self = this;

            var actualMonth = moment().format('MM');
            var actualYear = moment().format('YY');
            var actualDay = moment().format('DD');

            var endDate = '20'+actualYear+'-'+actualMonth+'-'+actualDay+'T01:01:01.000-04:00';
            var debitInfo = app.utils.Storage.getSessionItem('pay-debit-direct');
            requestDirectDebit = {
                "token": app.utils.Storage.getSessionItem('token'),
                "Ban": ''+app.utils.Storage.getSessionItem('selected-account-value'),
                "mAccountNo": debitInfo.mAccountNo,
                "mBankCode": debitInfo.mBankCode,
                "mCreditCardMemberName": debitInfo.mCreditCardMemberName, 
                "mCreditCardNo": debitInfo.mCreditCardNo, 
                "mCreditCardType": debitInfo.mCreditCardType, 
                "mCreditCardExpDate": debitInfo.mCreditCardExpDate,
                "mReason": debitInfo.mReason, 
                "mStartDate": debitInfo.mStartDate, 
                "mEndDate": endDate, 
                "mStatus": "C", 
                "mType": debitInfo.mType
            }; 

            var accountModel = new app.models.Account();                    
            accountModel.updateDirectDebit(
                // parameters
                requestDirectDebit,
                // data1,
                // success callback
                function(success) {
                    console.log(success);
                                            
                    if (!success.HasError) {
                        app.utils.Storage.setSessionItem('pay-debit-direct', requestDirectDebit);
                        showAlert('Debito Directo cancelado exitosamente', '', 'Ok');
                        // render payment step 2
                        self.render(function() {
                            $.mobile.activePage.trigger('pagecreate');
                        });
                    } else {
                        showAlert('Error', success.ErrorDesc, 'Aceptar');
                    }                                          
                },
                // error callback
                app.utils.network.errorFunction
            );

        },

        goToDataPlan: function(e) {

            app.router.navigate('data_plan', {
                trigger: true
            });

        },

        selectCreditCard: function(e) {

            var cardType = $(e.currentTarget).data('cardType');

            $('.btn-card').removeClass('on');

            $('#card-' + cardType).addClass('on');

            $('input[name=card_type]').val(cardType);

            app.utils.Storage.setSessionItem('credit-card-type', cardType);

        },

        updateCreditCard: function(e) {

            console.log("Validando TDC");
            $('#credit-card').validateCreditCard(function(result) {

                if (result.valid) {

                    $('.btn-card').removeClass('on');

                    var creditCardType = result.card_type.name;

                    switch (creditCardType) {
                        case 'mastercard':
                            $('#card-MC').addClass('on');
                            $('input[name=card_type]').val('MC');
                            app.utils.Storage.setSessionItem('credit-card-type', 'MC');
                            break;

                        case 'visa':
                            $('#card-VS').addClass('on');
                            $('input[name=card_type]').val('VS');
                            app.utils.Storage.setSessionItem('credit-card-type', 'VS');
                            break;

                        case 'amex':
                            $('#card-AM').addClass('on');
                            $('input[name=card_type]').val('AM');
                            app.utils.Storage.setSessionItem('credit-card-type', 'AM');
                            break;
                        default:
                            return;
                    }

                } else {

                    $('.btn-card').removeClass('on');
                    $('input[name=card_type]').val('');
                    app.utils.Storage.setSessionItem('credit-card-type', '');
                }

            });

        },

        hideFooter: function(e) {
            $('#btn-help').hide();
        },

        showFooter: function(e) {
            $('#btn-help').show();
        },

        showFormToEdit: function(e) {
            var self = this;
            console.log('Entre en Edit');
            // False -> No se tiene Data, entro en Form
            // True -> Se tiene data y muestro contenido
            var bool = false;
            var debitDirectData = app.utils.Storage.getSessionItem('pay-debit-direct')
            app.utils.Storage.setSessionItem('pay-debit-direct-edit', bool);
            app.utils.Storage.setSessionItem('selected-debit-direct-type', debitDirectData.mType);

            self.render(function() {
                $.mobile.activePage.trigger('pagecreate');
            });
        },

    });
});
