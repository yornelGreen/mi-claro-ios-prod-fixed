$(function() {

	// Profile View
	// ---------------

	app.views.AddAccountView = app.views.CommonView.extend({

		name : 'add_accounts',

		// The DOM events specific.
		events : {
			'pagecreate' : 'pageCreate',
			'show.bs.popover div[data-toggle="popover"]': 'showPopOver',

			// header
			'click .btn-back' : 'back',
			'click .btn-menu' : 'menu',
			'click .btn-chat' : 'chat',

			'click #btn-add-accounts' : 'showAddAccounts',
			'click .input-check-container' : 'changeSubscriber',
			'click #btn-save-account' : 'addAccount',
			'click #btn-cancel-save-account': 'cancelSaveAccount',
			'click .account-name' : 'showAccountDetail',
			'click .delete-account': 'deleteAccount',
			'click .default-account': 'setDefaultAccount',
			'click .save-changes': 'setDefaultSubscriber',
			'click .btn-gift-send-recharge'       :'giftSendRecharge',
			
			// content
			// 'click #btn-change-plan': 'changePlan',

            // toggle
		    'click .sectbar':							'toggleClass',
	        'click .phonebar':							'toggleClass',

			// menu
			'click .btn-account': 'account',
			'click .btn-consumption': 'consumption',
			'click .btn-service': 'service',
			'click .btn-change-plan': 'changePlan',
			'click .btn-add-aditional-data': 'aditionalDataPlan',
			'click .btn-device': 'device',
			'click .btn-profile': 'profile',
			'click .btn-gift': 'giftSend',
			'click .btn-invoice': 'invoice',
            'click .close-menu': 'closeMenu',
            'click .open-menu':	'openMenu',
            'click .btn-logout': 'logout',
            'click .btn-notifications':	'notifications',
            'click .btn-sva': 'sva',
			'click .btn-my-order': 'myOrder',
			'click .btn-my-store': 'myStore',

			// footer
			'click #btn-help' : 'helpSection'
		},
		
		render : function(callback) {
			var self = this, 
				accounts = app.utils.Storage.getSessionItem('add-accounts-list'),
				variables = {};

			// user hasn't clicked add-accounts in account
            if (!app.utils.Storage.getSessionItem('add-accounts')) {
                document.location.href = 'index.jsp';
            } else {	
				variables = {
					accounts : accounts,
					accountNumber : '',
					ssn : '',
					showBackBth: true,
					wirelessAccount: (app.utils.Storage.getSessionItem('selected-account').prodCategory=='WLS')?true:false
				};
	
				var findDefault = _.find(accounts, function(account){ 
					return account.defaultAccount == 'Y'; 
				});
				
				_.each(accounts, function(account, index) {
					//SET DEFAULT SUBSCRIBER
					if (account.defaultSubscriber == '') {
						console.log(account.subscribers[0].subscriberNumber);
						account.defaultSubscriber = account.subscribers[0].subscriberNumber;
					}
				});
					
				if (findDefault == undefined) {
					showAlert('Error', 'Estimado usuario es importante marcar una de sus cuentas como la Cuenta Principal.' +
							'Hágalo ahora mismo despleglandolas dando clic.', 'Aceptar', function(){});
				}else {
					app.utils.Storage.setSessionItem('default-subscriber', findDefault.defaultSubscriber);
				}
					
				app.TemplateManager.get(self.name, function(code) {
					var template = cTemplate(code.html());
					$(self.el).html(template(variables));
					callback();
					return this;
				});
            }
		},
		
		pageCreate: function(e) {
	        
            // enable tooltips
			$('[data-toggle="popover"]').popover({
				animation: false
			});			
			
			// allow only number
			$.mobile.activePage.on("input", "#account-number, #ssn-input", function() {
			    this.value = this.value.replace(/[^0-9]/g,'');
			});		
		},		

		showAddAccounts : function(e) {
			e.preventDefault();
			$("#add-accounts").toggle();
			$("#add-accounts-btn").hide();
		},
		
		showAccountDetail: function(e) {
			var id = e.currentTarget.dataset.id;
			$("#account-detail-" + id).toggle();
			if ($("#arrow-" + id).is(":visible")) {
				$("#arrow-" + id).hide();
				$("#arrow-i-" + id).show();
			}else {
				$("#arrow-" + id).show();
				$("#arrow-i-" + id).hide();
			}
		},

		cancelSaveAccount: function(e) {
			e.preventDefault();
			$("#add-accounts").toggle();
			$("#add-accounts-btn").show();
			
			$.mobile.activePage.find('#account-number').val('');
			$.mobile.activePage.find('#ssn-input').val('');
		},
		
		addAccount : function(e) {
			var self = this;
			
			var accountNumber = $.mobile.activePage.find('#account-number').val(),
				ssn = $.mobile.activePage.find('#ssn-input').val(),
				defaultAccount = $.mobile.activePage.find('#default-account-check').is(":checked");

			if (accountNumber == '' || ssn == ''){ //Not empty
				showAlert('Error', 'Estimado usuario debe completar todos los campos solicitados.', 'Aceptar', function(){});
				return;
			}
			if (accountNumber.length < 9 || accountNumber.length > 9){
				showAlert('Error', 'El número de cuenta debe tener 9 dígitos.', 'Aceptar', function(){});
				return;
			}
			if (ssn.length < 4 || ssn.length > 4) {
				showAlert('Error', 'El ssn debe tener 4 dígitos.', 'Aceptar', function(){});
				return;
			}
			if ((!/^\d*$/.test(accountNumber)) || (!/^\d*$/.test(accountNumber))){
				showAlert('Error', 'Ambos campos deben contener dígitos.', 'Aceptar', function(){});
				return;
			}
			
			self.options.accountModel.addAccount(
				//parameters
				accountNumber, 
				ssn, 
				defaultAccount,
				
				// succes callback
				function(data) {
					if (data.hasError == false) {			
						$("#add-accounts").toggle();
						$("#add-accounts-btn").show();
							
							self.options.accountModel.getAccountsList(
						    	// success callback
						    	function(data) {
						    		var accounts = data.object,
						    			accountList = [];
						    					
						    		app.utils.Storage.setSessionItem('add-accounts-list', accounts);
						    		
						    		// generate accounts
						    		for (var i=0 ; i<accounts.length ; i++) {
						    			
						    			var account = {
						    				Account: accounts[i].accountNumber,
						    				AmtDue: accounts[i].amtDue,
						    				BillCycle: accounts[i].billCycle,
						    				BillDateEnd: accounts[i].billDueDate,
						    				cicleDateEnd: 0,
						    				cicleDateStart: 0,
						    				CreditClass: accounts[i].creditClass,
						    				mAccountType: accounts[i].accountType,
						    				mAccountSubType: accounts[i].accountSubType,
						    				prodCategory: (accounts[i].subscribers[0].productType=='G' || 
						    						accounts[i].subscribers[0].productType=='C')? 'WLS' : 'WRL',
						    				Subscribers: []
						    			};
						    			
						    			// generate subscribers
						    			for (var j=0 ; j<accounts[i].subscribers.length ; j++) {
						    				var subscriber = {
						    					subscriber: accounts[i].subscribers[j].subscriberNumber,
						    					ProductType: accounts[i].subscribers[j].productType,
						    					status: 'A'
						    				};
						    				account.Subscribers.push(subscriber);
						    			}
						    			
						    			if (i == 0 && defaultAccount) {
											// update widget token
											self.updateWidgetToken(
													accounts[i].accountNumber, 
													accounts[i].subscribers[0].subscriberNumber, 
													accounts[i].subscribers[0].productType);
						    			}
						    			
						    			// set account
						    			accountList.push(account);
						    		}
						    		
						    		//update accounts list
						    		app.utils.Storage.setSessionItem('accounts-list', accountList);
						    
						    		if (accounts[0].defaultSubscriber == '') {
						    			console.log(accounts[0].subscribers[0].subscriberNumber);
						    			accounts[0].defaultSubscriber = accounts[0].subscribers[0].subscriberNumber;
						    		}
						    		
						   			showAlert('Información', 'La cuenta fue agregada existosamente', 'Ok', function(){});
						    			
						   			var variables = {
										accounts : accounts,
										accountNumber : '',
										ssn : '',
										wirelessAccount: (app.utils.Storage.getSessionItem('selected-account').prodCategory=='WLS')?true:false
									};
										
									app.TemplateManager.get(self.name, function(code) {
										var template = cTemplate(code.html());
										$(self.el).html(template(variables));
										return this;
									});
				    			},
					    			
				    			// error function
				    			app.utils.network.errorFunction
						    );
						}
					},

					// error function
					app.utils.network.customErrorFunction
				);
		},

		setDefaultAccount : function(e) {
			
			e.preventDefault();
			var self = this,
				accounts = app.utils.Storage.getSessionItem('add-accounts-list'),
				accountNumber = e.currentTarget.dataset.account,
				subscriber = null;
			
			var account = _.find(accounts, function(account, index) {
				return account.accountNumber == accountNumber;
			});
			
			var subscriberNumber = account.defaultSubscriber != '' ? account.defaultSubscriber : account.subscribers[0].subscriberNumber ;
			
			this.options.accountModel.setDefault(
					//parameters /account/default/<accountNumber>/<subscriberNumber>
					accountNumber, 
					subscriberNumber,
					
					// success callback
					function(data) {
						
						self.options.accountModel.getAccountsList(
						    // success callback
						    function(data) {
						    	var accounts = data.object;

						    	//Save new account list in session
								app.utils.Storage.setSessionItem('add-accounts-list', accounts);
					
								//Set new default account value in session
								app.utils.Storage.setSessionItem('selected-account-value', accountNumber);
								
								//Set new default account in session
								var accountList = app.utils.Storage.getSessionItem('accounts-list'),							
									selectedAccount = _.find(accountList, function(account){ 
										return account.Account == accountNumber; 
									});
								
			                    app.utils.Storage.setSessionItem('selected-account', selectedAccount),
								
			                    // update subscriber value
			    				app.utils.Storage.setSessionItem('default-subscriber', subscriberNumber);
			    				app.utils.Storage.setSessionItem('selected-subscriber-value', subscriberNumber);

			    				$.each(account.subscribers, function(index, value){
			    					if(value.subscriberNumber == parseInt(subscriberNumber)){
										// update widget token
										self.updateWidgetToken(accountNumber, subscriberNumber, value.productType);
			    					}
			    				});
			    				
			    				// update default subscriber
			    				$.each(accounts, function(index, value){
			    					if(value.accountNumber == parseInt(accountNumber)){
			    						accounts[index].defaultSubscriber = subscriberNumber;
			    					}
			    				});
								
						   		var variables = {
									accounts : accounts,
									accountNumber : '',
									ssn : '',
									wirelessAccount: (app.utils.Storage.getSessionItem('selected-account').prodCategory=='WLS')?true:false
								};
						   		
						   		showAlert('Información', 'Se ha cambiado con éxito', 'Ok', function(){});
						   		
                                //setting initial values
                                self.render(function () {
                                     $.mobile.activePage.trigger('pagecreate');
                                });
                                                                  
								app.TemplateManager.get(self.name, function(code) {
									var template = cTemplate(code.html());
									$(self.el).html(template(variables));
									return self;
								});
				    		},
					    			
				    		// error function
				    		app.utils.network.errorFunction
						);	
					},

					// error function
					app.utils.network.errorFunction
				);
		},

		changeSubscriber: function(e) {
			
			var checkbox = $(e.currentTarget).find('input[type="checkbox"]')
				container = $(e.currentTarget);
			
			if (!checkbox.prop('checked') || checkbox.prop('checked')) {
				
				// uncheck all the inputs
				var index = checkbox.data('index');
				
				// uncheck all the inputs				
				$('.input-check-container').removeClass('on');
				$('.input-check-container input[type="checkbox"]').prop('checked', false);
				
				//check the input
				checkbox.prop('checked', true);
				container.addClass('on');
				
				app.utils.Storage.setSessionItem('default-subscriber', checkbox.data('subscriber'));
				app.utils.Storage.setSessionItem('selected-subscriber-value', checkbox.data('subscriber'));

				if (app.utils.Storage.getSessionItem('subscribers') != null) {
					$.each(app.utils.Storage.getSessionItem('subscribers'), function(index, value){
						if(value.subscriber==app.utils.Storage.getSessionItem('selected-subscriber-value')){
							app.utils.Storage.setSessionItem('selected-subscriber', value);
						}
					});
				}				
				
			}
		},
		
		setDefaultSubscriber: function(e) {
			e.preventDefault();
			var self = this,
				accounts = app.utils.Storage.getSessionItem('add-accounts-list'),
				index = e.currentTarget.dataset.index,
				accountNumber = e.currentTarget.dataset.account,
				account = accounts[index]
				subscriberNumber = app.utils.Storage.getSessionItem('default-subscriber');
			
			this.options.accountModel.setDefault(
				//parameters /account/default/<accountNumber>/<subscriberNumber>
				accountNumber, 
				subscriberNumber,
					
				// success callback
				function(data) {
						
					//Change old default to 'N'
					_.each(accounts, function(account, index) {
						if (account.defaultAccount == 'Y') {
							account.defaultAccount = 'N';
							return;
						} 
					});
						
					// change new default to 'Y'
					accounts[index].defaultAccount = 'Y';
					
					// set new subscriber
					accounts[index].defaultSubscriber = subscriberNumber;
					
					// save new account list in session
					app.utils.Storage.setSessionItem('add-accounts-list', accounts)
					
					// set new default account value in session
					app.utils.Storage.setSessionItem('selected-account-value', accounts[index].accountNumber);
					
					showAlert('Información', 'Se ha cambiado con éxito.', 'Aceptar', function(){});
					
    				$.each(account.subscribers, function(index, value){
    					if(value.subscriberNumber == parseInt(subscriberNumber)){
							// update widget token
							self.updateWidgetToken(accountNumber, subscriberNumber, value.productType);
    					}
    				});
					
					// render view
					var variables = {
						accounts : accounts,
						accountNumber : '',
						ssn : '',
						wirelessAccount: (app.utils.Storage.getSessionItem('selected-account').prodCategory=='WLS')?true:false
					};
							
					app.TemplateManager.get(self.name, function(code) {
						var template = cTemplate(code.html());
						$(self.el).html(template(variables));
						return this;
					});				
				},

				// error function
				app.utils.network.errorFunction
			);
		},
		
		deleteAccount : function(e) {
			
			e.preventDefault();
			var self = this,
				accountNumber = e.currentTarget.dataset.account,
				accounts = app.utils.Storage.getSessionItem('add-accounts-list'),
				index = e.currentTarget.dataset.index;
		
			//Get account
			var account = accounts[index];
				
			if (account.defaultAccount == 'Y') {
				showConfirm('Error', 'Estimado usuario no es posible eliminar la cuenta principal.', 'Aceptar', function(){});
			} else if (accounts.length == 1) {
				showConfirm('Error', 'Estimado usuario no es posible eliminar todas sus cuentas.', 'Aceptar', function(){});	
			}else {
				
				showConfirm(
					'Confirmación', 
					'Esta seguro que desea eliminar la cuenta '+accountNumber+' ?', 
					'No, Si', 
					function (respond) {
						
						if(respond == 2) {
							
							self.options.accountModel.deleteAccount(
									// account Number 
									accountNumber,
									// success callback
									function(data) {
										
										showAlert(
											'Información', 
											'La cuenta fue eliminada existosamente', 
											'Ok', 
											function(){}); 
										
										//Delete account from array
										accounts = _.without(accounts, account);
									
										//Save new account list in session
										app.utils.Storage.setSessionItem('add-accounts-list', accounts);
										
										var accountList = app.utils.Storage.getSessionItem('accounts-list'),
											deleteFromAccount = _.find(accountList, function(account){ 
												return account.Account == accountNumber; 
											});
										
										
										accountList = _.without(accountList, deleteFromAccount);
										
										//update accounts list
										app.utils.Storage.setSessionItem('accounts-list', accountList);
									
										//Render view
										var variables = {
											accounts : accounts,
											accountNumber : '',
											ssn : '',
											wirelessAccount: (app.utils.Storage.getSessionItem('selected-account').prodCategory=='WLS')?true:false
										};
										
										app.TemplateManager.get(self.name, function(code) {
											var template = cTemplate(code.html());
											$(self.el).html(template(variables));
											return this;
										});		
									},
									
									// error function
									app.utils.network.errorFunction
								);								
							
						}
						
					}
				);
				
			}
		},
		
		showPopOver: function(e) {
			setTimeout(function(){
				$.mobile.activePage.find('[data-toggle="popover"]').popover('hide');				
			},4000);			
		},
		
		updateWidgetToken: function(account, subscriber, productType){
			
			var token = account + '||' + subscriber +
        		'||' + moment().format('DD-MM-YYYY HH:mm:ss');
        
	        // set values for widget
	        window.aes(token, function(cipher) {

            	var prefs = plugins.appPreferences.iosSuite(app.appGroup);
                                
                // set url
                prefs.store (
                    // success callback
                    function(e){},
                    // error callback
                    function(e){},
                    app.appGroup,
                    'api-url',
                    app.apiUrl + 'widgets/getInvoiceInfo'
                );
                       
                // store key => value pair
                prefs.store (
                    // success callback
                    function(e){},
                    // error callback
                    function(e){},
                    app.appGroup,
                    'login',
                    cipher
                );
                       
                // set productType
                prefs.store (
                    // success callback
                    function(e){},
                    // error callback
                    function(e){},
                    app.appGroup,
                    'productType',
                    productType
                );
                
//                console.log('------------------------------------');
//                console.log('account='+account+' subscriber='+subscriber);
//                console.log('------------------------------------');
//                console.log(cipher);
//                console.log('------------------------------------');
	        
	        });
			
		},		
		
	});
});
