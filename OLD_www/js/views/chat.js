$(function() {

    // Chat View
    // ---------------

    app.views.ChatView = app.views.CommonView.extend({

        name: 'chat',

        // The DOM events specific.
        events: {

            //header
            'click .btn-back': 'back',
            'click .btn-menu': 'back',

            // evets
            'active': 'active',

            //event
            'iframeload': 'resizeIframe',

            // menu
            'click .btn-account': 'account',
            'click .btn-consumption': 'consumption',
            'click .btn-service': 'service',
            'click .btn-change-plan': 'changePlan',
            'click .btn-add-aditional-data': 'aditionalDataPlan',
            'click .btn-device': 'device',
            'click .btn-profile': 'profile',
            'click .btn-gift': 'giftSend',
            'click .btn-invoice': 'invoice',
            'click .close-menu': 'closeMenu',
            'click .open-menu': 'openMenu',
            'click .btn-logout': 'logout',
            'show.bs.offcanvas .navmenu': 'openMenu',
            'click .btn-notifications': 'notifications',
            'click .btn-sva': 'sva',
            'click .btn-gift-send-recharge': 'giftSendRecharge',
			'click .btn-my-order': 'myOrder',
			'click .btn-my-store': 'myStore',

            // footer
            'click #btn-help': 'helpSection'
        },


        // Render the template elements        
        render: function(callback) {

            var self = this;

            var variables = {
                url: app.chatURL,
                showBackBtn: (app.router.history[app.router.history.length - 1] !== 'help_section' && app.router.history[app.router.history.length - 1] !== 'login'),
                isLogued: app.utils.Storage.getSessionItem('token') != null,
                showBackBth: true
            };

            app.TemplateManager.get(self.name, function(code) {
                var template = cTemplate(code.html());
                $(self.el).html(template(variables));
                app.router.refreshPage();
                callback();
                return this;
            });

        },

        resizeIframe: function(e) {

            $('html, body').css('height', '100%');

        }

    });

});
