$(function() {

	// Plan Change View
	// ---------------
	
	app.views.ChangePlan1View = app.views.CommonView.extend({

		name:'change_plan_1',
		
		// The DOM events specific.
		events: {
			
            // header
            'click .btn-back': 'back',
            'click .btn-menu': 'menu',
            'click .btn-chat': 'chat',
            'click .input-check-container': 'selectNewPlan',
            
            //body
			'click #bth-continue': 'nextStep',
            'change #select-account': 'changeAccount',
            'click .select-subscriber': 'changeSubscriber',

            // toggle
            'click .sectbar': 'toggleClass',
            'click .phonebar': 'toggleClass',

			// menu
			'click .btn-account': 'account',
			'click .btn-consumption': 'consumption',
			'click .btn-service': 'service',
			'click .btn-change-plan': 'changePlan',
			'click .btn-add-aditional-data': 'aditionalDataPlan',
			'click .btn-device': 'device',
			'click .btn-profile': 'profile',
			'click .btn-gift': 'giftSend',
			'click .btn-invoice': 'invoice',
            'click .close-menu': 'closeMenu',
            'click .open-menu':	'openMenu',
            'click .btn-logout': 'logout',
            'click .btn-notifications':	'notifications',
            'click .btn-sva': 'sva',
			'click .btn-gift-send-recharge'       :'giftSendRecharge',
			'click .btn-my-order': 'myOrder',
			'click .btn-my-store': 'myStore',

            // footer
   		    'click #btn-help': 'helpSection'
				
		},
		
		// Render the template elements        
		render:function (callback) {
			
			// set spanish as default language
			moment.locale('es');
			
			var account = app.utils.Storage.getSessionItem('selected-account-value');
				now = moment().toDate(),
				planEffectiveDate = moment(app.utils.Storage.getSessionItem('selected-subscriber').PlanEffectiveDate,'MM-DD-YYYY').add(30, 'day').toDate(), 
				actualCicle = moment(app.utils.Storage.getSessionItem('selected-account').cicleDateStart,'MM-DD-YYYY').toDate(),
				nextCicle = moment(app.utils.Storage.getSessionItem('selected-account').cicleDateEnd,'MM-DD-YYYY').toDate(),
				accountSuspend = false;
			
			// suspend account
			var subscribers = app.utils.Storage.getSessionItem('subscribers');
			for (var i = 0; i < subscribers.length; i++) {
                if (subscribers[i].status == 'S') {
                    accountSuspend = true;
                    break;
                }
            }				
			
			var self = this,
				user = app.utils.Storage.getLocalItem('user'),
				account = app.utils.Storage.getSessionItem('selected-account'),
				accountData = this.getAccountByBAN(account.Account),
				variables = {
				    accounts: app.utils.Storage.getSessionItem('accounts-list'),
	                selectedAccountValue: app.utils.Storage.getSessionItem('selected-account-value'),
	                selectedAccount: app.utils.Storage.getSessionItem('selected-account'),
	                subscribers: app.utils.Storage.getSessionItem('subscribers'),
	                selectedSubscriber: app.utils.Storage.getSessionItem('selected-subscriber'),
	                offers: app.utils.Storage.getSessionItem('offers'),
					tops: app.utils.Storage.getSessionItem('tops'),
                    svas: app.utils.Storage.getSessionItem('svas'),
                    nextCicleDate: moment(app.utils.Storage.getSessionItem('selected-subscriber').PlanEffectiveDate,'MM-DD-YYYY').add(30, 'day').format('MM-DD-YYYY'),
                    wirelessAccount: (app.utils.Storage.getSessionItem('selected-subscriber').ProductType=='G' ||
	                    app.utils.Storage.getSessionItem('selected-subscriber').ProductType=='C'),
                    currentMonthSOC: now.getTime() < planEffectiveDate.getTime() ,
                    validEffectiveDate: moment(app.utils.Storage.getSessionItem('selected-subscriber').PlanEffectiveDate,'MM-DD-YYYY').isValid(),
                    accountSuspend: accountSuspend, // validate suspend account
                    familyPlan: app.utils.Storage.getSessionItem('selected-subscriber').PlanUpdate, // validate family plan
                    typeOfTelephony: app.utils.tools.typeOfTelephony,
                    showBackBth: true
				};
			
			app.TemplateManager.get(self.name, function(code){
		    	var template = cTemplate(code.html());
		    	$(self.el).html(template(variables));	
		    	callback();	
		    	return this;
		    });					
			
		},
		
        changeAccount: function (e) {

            var self = this,
                analytics = null,
                offertModel = new app.models.Offer(),
				accountModel = new app.models.Account(),
				subscriberModel = new app.models.Subscriber();

            app.utils.Storage.setSessionItem('selected-account-value', $.mobile.activePage.find('#select-account').val());
            var accountNumber = app.utils.Storage.getSessionItem('selected-account-value');

            if (analytics != null) {
                // send GA statistics
                analytics.trackEvent('select', 'change', 'select account number', accountNumber);
            }

            app.utils.Storage.setSessionItem('selected-account', app.utils.Storage.getSessionItem(accountNumber));

            $.each(app.utils.Storage.getSessionItem('accounts-list'), function (index, value) {
                if (value.Account === app.utils.Storage.getSessionItem('selected-account-value')) {
                    app.utils.Storage.setSessionItem('selected-account', value);
                }
            });

            if (app.utils.Storage.getSessionItem(accountNumber) == null) {

                this.options.accountModel.getAccountInfo(
                    //parameter
                    app.utils.Storage.getSessionItem('token'),

                    accountNumber,

                    //success callback
                    function (data) {
                        console.log(data);

                        if (!data.HasError) {

                            // session
                            app.utils.Storage.setSessionItem('selected-account', data);

                            // cache
                            //app.cache.Accounts[accountNumber] = data;
                            app.utils.Storage.setSessionItem(accountNumber, data);

                            self.options.accountModel.getAccountSubscribers(
                                //parameter
                                app.utils.Storage.getSessionItem('token'),
                                data.Account,

                                //success callback
                                function (data) {
                                    console.log('#success ws service');

                                    if (!data.HasError) {

                                        app.utils.Storage.setSessionItem('subscribers', data.Subscribers);
                                        app.utils.Storage.setSessionItem('selected-subscriber-value', data.Subscribers[0].subscriber)
                                        app.utils.Storage.setSessionItem('selected-subscriber', data.Subscribers[0]);

                                        var accountSuspend = false;

                                        for (var i = 0; i < data.Subscribers.length; i++) {
                                            if (data.Subscribers[i].status == 'S') {
                                                accountSuspend = true;
                                                break;
                                            }
                                        }

                                        // cache
                                        app.utils.Storage.setSessionItem('subscribers-' + accountNumber, data.Subscribers);

                                        if (!app.utils.Storage.getSessionItem('suspend-account') &&
                                            accountSuspend) {
                                                                            
                                            self.showSuspendedAccount();
                                            
                                        }
                                        
                                        // soc code
            							var subscriber = data.Subscribers[0],
            								subscriberValue = data.Subscribers[0].subscriber,
            								soc = subscriber.mSocCode,
            								account = app.utils.Storage.getSessionItem('selected-account'),
            								accountType = account.mAccountType,
            								accountSubType = account.mAccountSubType,
            								technology = subscriber.Tech,
            								offerID = 0;

            							// get offers
            							offertModel.getOffers(
        									//parameters
        									soc.replace("'",''), 
        									technology, 
        									accountType,
        									accountSubType,
        									subscriber.PlanRate,
        									account.CreditClass,
        									
        									//success callback
        									function(data){
        										
        										app.utils.Storage.setSessionItem('offers', data.object);
        										
        										subscriberModel.getSVA(
    												//parameter
    												app.utils.Storage.getSessionItem('token'),
    												app.utils.Storage.getSessionItem('selected-account-value'),
    												subscriberValue,
    												offerID,
    												
    												//success callback
    												function(data){
    													
    													//TOPs
    													app.utils.Storage.setSessionItem('tops', data.TOPs);

    													//SVAs
    													//app.session.SVAs = data.SVAs;
    													app.utils.Storage.setSessionItem('svas',data.SVAs);
    													
    													//Subscriber
    													$.each(app.utils.Storage.getSessionItem('subscribers'), function(index, value){
    														if(value.subscriber==subscriber){
    															app.utils.Storage.setSessionItem('selected-subscriber-result', value);
    														}
    													});
    													
    			                                        self.render(function () {
    			                                            $.mobile.activePage.trigger('pagecreate');
    			                                        });
    													
    												},
    												
    												// error function
    												app.utils.network.errorFunction	
    											);
        										
        									},
        									
        									// error callback
        									function(error){
        										
        										showAlert('Error', data.Desc, 'Aceptar');
        										
        									}
        								);                                        


                                    } else {

                                        showAlert('Error', data.Desc, 'Aceptar');

                                    }

                                },

                                // error function
                                app.utils.network.errorFunction
                            );

                        } else {

                            showAlert('Error', data.Desc, 'Aceptar');

                        }

                    },

                    // error function
                    app.utils.network.errorFunction
                );


            } else {

                // cache
                app.utils.Storage.setSessionItem('selected-account', app.utils.Storage.getSessionItem(accountNumber));
                var selectedAccount = app.utils.Storage.getSessionItem('selected-account');

                // search if the subscriber dont exists
                if (app.utils.Storage.getSessionItem('subscribers-' + accountNumber) == null) {
                    this.options.accountModel.getAccountSubscribers(
                        //parameter
                        app.utils.Storage.getSessionItem('token'),
                        selectedAccount.Account,

                        //success callback
                        function (data) {

                            if (!data.HasError) {

                                app.utils.Storage.setSessionItem('subscribers', data.Subscribers);
                                app.utils.Storage.setSessionItem('selected-subscriber', data.Subscribers[0]);

                                // cache
                                app.utils.Storage.setSessionItem('subscribers-' + accountNumber, data.Subscribers);
                                
                                // soc code
    							var subscriber = data.Subscribers[0],
    								subscriberValue = data.Subscribers[0].subscriber,
    								soc = subscriber.mSocCode,
    								account = app.utils.Storage.getSessionItem('selected-account'),
    								accountType = account.mAccountType,
    								accountSubType = account.mAccountSubType,
    								technology = subscriber.Tech,
    								offerID = 0;
                                
                                // get offers
    							offertModel.getOffers(
									//parameters
									soc.replace("'",''), 
									technology, 
									accountType,
									accountSubType,
									subscriber.PlanRate,
									account.CreditClass,
									
									//success callback
									function(data){
										
										app.utils.Storage.setSessionItem('offers', data.object);
										
										subscriberModel.getSVA(
											//parameter
											app.utils.Storage.getSessionItem('token'),
											app.utils.Storage.getSessionItem('selected-account-value'),
											subscriberValue,
											offerID,
											
											//success callback
											function(data){
												
												//TOPs
												app.utils.Storage.setSessionItem('tops', data.TOPs);

												//SVAs
												//app.session.SVAs = data.SVAs;
												app.utils.Storage.setSessionItem('svas',data.SVAs);
												
												//Subscriber
												$.each(app.utils.Storage.getSessionItem('subscribers'), function(index, value){
													if(value.subscriber==subscriber){
														app.utils.Storage.setSessionItem('selected-subscriber-result', value);
													}
												});
												
		                                        self.render(function () {
		                                            $.mobile.activePage.trigger('pagecreate');
		                                        });
												
											},
											
											// error function
											app.utils.network.errorFunction	
										);
										
									},
									
									// error callback
									function(error){
										
										showAlert('Error', data.Desc, 'Aceptar');
										
									}
								);
                                
                            } else {

                                showAlert('Error', data.Desc, 'Aceptar');

                            }

                        },

                        // error function
                        app.utils.network.errorFunction
                    );
                } else {

                    //load cache 
                    var subscribers = app.utils.Storage.getSessionItem('subscribers-' + accountNumber);

                    // set cache
                    app.utils.Storage.setSessionItem('subscribers', subscribers);
                    app.utils.Storage.setSessionItem('selected-subscriber', subscribers[0]);
                    
                    // soc code
					var subscriber = subscribers[0],
						subscriberValue = subscribers[0].subscriber,
						soc = subscriber.mSocCode,
						account = app.utils.Storage.getSessionItem('selected-account'),
						accountType = account.mAccountType,
						accountSubType = account.mAccountSubType,
						technology = subscriber.Tech,
						offerID = 0;
                    
                    // get offers
					offertModel.getOffers(
						//parameters
						soc.replace("'",''), 
						technology, 
						accountType,
						accountSubType,
						subscriber.PlanRate,
						account.CreditClass,
						
						//success callback
						function(data){
							
							app.utils.Storage.setSessionItem('offers', data.object);
							
							subscriberModel.getSVA(
								//parameter
								app.utils.Storage.getSessionItem('token'),
								app.utils.Storage.getSessionItem('selected-account-value'),
								subscriberValue,
								offerID,								
								
								//success callback
								function(data){
									
									//TOPs
									app.utils.Storage.setSessionItem('tops', data.TOPs);

									//SVAs
									//app.session.SVAs = data.SVAs;
									app.utils.Storage.setSessionItem('svas',data.SVAs);
									
									//Subscriber
									$.each(app.utils.Storage.getSessionItem('subscribers'), function(index, value){
										if(value.subscriber==subscriber){
											app.utils.Storage.setSessionItem('selected-subscriber-result', value);
										}
									});
									
                                    self.render(function () {
                                        $.mobile.activePage.trigger('pagecreate');
                                    });
									
								},
								
								// error function
								app.utils.network.errorFunction	
							);
							
						},
						
						// error callback
						function(error){
							
							showAlert('Error', data.Desc, 'Aceptar');
							
						}
					);                    

                }

            }

        },

        changeSubscriber: function (e) {

            var self = this,
                analytics = null,
                offertModel = new app.models.Offer(),
                accountModel = new app.models.Account(),
                subscriberModel = new app.models.Subscriber();

            app.utils.Storage.setSessionItem('selected-subscriber-value', $(e.currentTarget).data('value'));

            $.each(app.utils.Storage.getSessionItem('subscribers'), function (index, value) {
                if (value.subscriber == app.utils.Storage.getSessionItem('selected-subscriber-value')) {
                    app.utils.Storage.setSessionItem('selected-subscriber', value);
                }
            });

            if (analytics != null) {
                // send GA statistics
                analytics.trackEvent('select', 'change', 'select subscriber', app.utils.Storage.getSessionItem('selected-subscriber'));
            }

            if($(e.currentTarget).data('search-info') == true){
                // set flag search data
                $(e.currentTarget).data('search-info', false);
            }else{
                // set flag search data
                $(e.currentTarget).data('search-info', true);

                // soc code
                var subscriber = app.utils.Storage.getSessionItem('selected-subscriber'),
                    subscriberValue = app.utils.Storage.getSessionItem('selected-subscriber').subscriber,
                    soc = subscriber.mSocCode,
                    account = app.utils.Storage.getSessionItem('selected-account'),
                    accountType = account.mAccountType,
                    accountSubType = account.mAccountSubType,
                    technology = subscriber.Tech,
                    offerID = 0;

                 // validate if exits dsl order
                 if(subscriber.DSLQualification !== undefined &&
                    subscriber.DSLQualification !== null && subscriber.DSLQualification.HasError){

                    var html = '';
                    html += '<div id="errorCont">'+
                                '<div class="subtitle nbott din-b f-little text-center f-black vcenter">'+
                                '<div class="tabcell">'+
                                    'AVISO'+
                                '</div>'+
                            '</div>'+

                            '<div class="nbott din-b f-little text-center f-red">'+ subscriber.DSLQualification.ErrorDesc +'</div>'+
                        '</div>';

                        // put html
                        $('#phone'+$(e.currentTarget).data('index')).html(html);
                 } else {

                    if(account.mAccountSubType == 'W' && account.mAccountType == 'I' && app.utils.Storage.getSessionItem('selected-subscriber').ProductType == 'O'){
                    
                    	// DSL months
                    	app.utils.Storage.setSessionItem('dslMonths', subscriber.CommitmentOriginalMonths);
                        
                        // get offers
                        offertModel.getOffersDSL(

                            //parameters
                            app.utils.Storage.getSessionItem('token'),
                            subscriber.subscriber,

                            //success callback
                            function(data){

                                var offersDSL = new Array();

                                $.each(data.DSLCatalogItems, function(index, value){
                                    offersDSL[index] = {
                                        productType	    :data.DSLCatalogItems[index].MB1_UPLOAD,
                                        contract	    :data.DSLCatalogItems[index].SOLO_GPON,
                                        productId       :data.DSLCatalogItems[index].ADA_PRODUCT_ID,
                                        rent			:data.DSLCatalogItems[index].PRICE,
                                        soc				:data.DSLCatalogItems[index].ALPHA_CODE,
                                        socDescription	:data.DSLCatalogItems[index].PRODUCT_NAME
                                    }
                                });

                                app.utils.Storage.setSessionItem('offers', offersDSL);

                                    //Subscriber
                                    $.each(app.utils.Storage.getSessionItem('subscribers'), function(index, value){
                                        if(value.subscriber==subscriber){
                                            app.utils.Storage.setSessionItem('selected-subscriber-result', value);
                                        }
                                    });

                                    // New code E
                                    var accountSuspend = false;

                                    // suspend account
                                    var subscribers = app.utils.Storage.getSessionItem('subscribers');
                                    for (var i = 0; i < subscribers.length; i++) {
                                        if (subscribers[i].status == 'S') {
                                            accountSuspend = true;
                                            break;
                                        }
                                    }

                                    var now = moment().toDate(),
                                        selectedSubscriber = app.utils.Storage.getSessionItem('selected-subscriber'),
                                        nextCicleDate = moment(selectedSubscriber.PlanEffectiveDate,'MM-DD-YYYY').add(30, 'day').format('MM-DD-YYYY'),
                                        planEffectiveDate = moment(selectedSubscriber.PlanEffectiveDate,'MM-DD-YYYY').add(30, 'day').toDate(),
                                        wirelessAccount = (selectedSubscriber.ProductType=='G' || selectedSubscriber.ProductType=='C'),
                                        currentMonthSOC = now.getTime() < planEffectiveDate.getTime(),
                                        validEffectiveDate = moment(selectedSubscriber.PlanEffectiveDate,'MM-DD-YYYY').isValid(),
                                        accountSuspend = accountSuspend,
                                        familyPlan = selectedSubscriber.PlanUpdate,
                                        offers = app.utils.Storage.getSessionItem('offers'),
                                        currentIndex = $(e.currentTarget).data('index'),
                                        html = '';

                                    if(!accountSuspend) {

                                        // Hide error msg
                                        $("#errorCont").hide();

                                        html += '<div class="basicrow">'+
                                        '<div class="autobar">'+
                                            '<div class="container">'+
                                                '<div class="basicrow f-little din-b f-gray">Plan Actual</div>'+

                                                '<div class="basicrow m-top-ii">';

                                                        if(selectedSubscriber.Plan != '') {
                                                            html += '<div class="contspace f-med f-black text-left f-bold">' + selectedSubscriber.Plan + '</div>';
                                                        }else{
                                                            html += '-';
                                                        }

                                                        html += '<div class="phonespace f-lmed f-red din-b text-right">';
                                                        if(selectedSubscriber.PlanRate != '') {
                                                            html += '$'+ parseFloat(selectedSubscriber.PlanRate.replace('$','')).toFixed(2);
                                                        }else{
                                                            html += '';
                                                        }

                                                    html += '</div>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>';


                                        // New format amount value
                                        var newAmountValue;
                                        var totalRate = parseFloat(selectedSubscriber.TotalRate.replace('$','')).toFixed(2);

                                        if (totalRate != null && totalRate != '' && totalRate.indexOf('.') >= 0) {
                                            newAmountValue = totalRate.split('.');
                                        } else {
                                            newAmountValue = parseFloat(app.utils.Storage.getSessionItem('selected-account').AmtDue.replace('$','')).toFixed(2).split('.');
                                        }

                                        html += '<div class="big-bal difst text-center">'+
                                        '<div class="theight">';

                                            if(selectedSubscriber.PlanRate != '') {
                                                html += '<span class="tmed-s din-b f-red">$</span>'+
                                                '<span class="big-s din-b f-red">'+newAmountValue[0]+'</span>'+
                                                '<span class="tmed-s din-b f-red"><span class="tabcell">'+newAmountValue[1]+'</span></span>';
                                            }else{
                                                html += '<span>-</span>'+
                                                '<span class="tmed-s"></span>';
                                            }

                                            html += '</div>'+

                                            '<div class="basicrow f-little m-bottom f-bold">Renta Mensual</div>'+
                                        '</div>';

                                        if (offers.length > 0) {

                                            html += '<div class="subtitle nbott din-b f-little text-center f-black vcenter">'+
                                                '<div class="tabcell">'+
                                                    'CAMBIO DE PLAN'+
                                                '</div>'+
                                            '</div>';

                                            $.each(offers, function (index, offer) {

                                                html += '<div class="planspc input-check-container">'+
                                                    '<div class="container norel">'+
                                                        '<div class="spc-i vcenter">'+
                                                            '<div class="tabcell">'+
                                                                '<input type="checkbox" name="checkbox_offers" id="checkbox_'+ index +'" class="css-checkbox2 css-checkbox" value="'+ offer.soc +'" data-soc-desc="'+ offer.socDescription +'" data-soc-rent="'+ parseFloat(offer.rent).toFixed(2) +'" data-soc-index="'+ index +'"  data-soc-desc="'+ offer.socDescription +'"  data-product-type="'+ offer.productType +'"  data-contract="'+ offer.contract +'" data-product-id="'+ offer.productId +'" /><label for="checkboxG7" class="css-label2 radGroup1"></label>'+
                                                            '</div>'+
                                                        '</div>'+

                                                        '<div class="spc-ii f-little f-bold text-justify vcenter">'+
                                                            '<div class="tabcell">'+
                                                                offer.socDescription +
                                                            '</div>'+
                                                        '</div>'+

                                                        '<div class="spc-iii f-med text-right din-b vcenter">'+
                                                            '<div class="tabcell">'+
                                                                '$' + parseFloat(offer.rent).toFixed(2) +
                                                            '</div>'+
                                                        '</div>'+
                                                    '</div>'+
                                                '</div>';

                                            });

                                            html += '<div class="autobar diff-i">'+
                                                '<div class="container">'+
                                                    '<div class="r-btn text-center f-med f-white vcenter">'+
                                                        '<div class="tabcell" id="bth-continue">'+
                                                            'Continuar'+
                                                        '</div>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</div>';

                                        } else {

                                            html += '<div class="subtitle nbott din-b f-little text-center f-black vcenter">'+
                                                '<div class="tabcell">'+
                                                    'AVISO'+
                                                '</div>'+
                                            '</div>';

											if (data.ErrorDesc !== undefined && data.ErrorDesc.length > 0) {
                                                html += '<div class="planspc input-check-container">'+
                                                    '<div class="container norel">'+
                                                        '<div class="nbott din-b f-little text-center f-red"><br/><br/>' + data.ErrorDesc + '</div>'
                                                    '</div>'+
                                                '</div>';
                                            } else {
                                            	html += '<div class="planspc input-check-container">'+
                                                    '<div class="container norel">'+
                                                        '<div class="nbott din-b f-little text-center f-red m-error"><br/><br/>No hay planes disponibles.</div>'
                                                    '</div>'+
                                                '</div>';
                                            }

                                        }

                                    html += '</div>';

                                    } else if (currentMonthSOC && validEffectiveDate && !accountSuspend) {
                                        $("#errorContent").hide();
                                        html += '<div id="errorCont">'+
                                                    '<div class="subtitle nbott din-b f-little text-center f-black vcenter">'+
                                                    '<div class="tabcell">'+
                                                        'AVISO'+
                                                    '</div>'+
                                                '</div>'+

                                                '<div class="nbott din-b f-little text-center f-red m-error">Estimado Cliente, s&oacute;lo es permitido un cambio de plan durante el ciclo de facturaci&oacute;n. Debe esperar hasta el '+ nextCicleDate +' para poder realizar el cambio nuevamente.</div>'+
                                            '</div>';
                                    } else if (!validEffectiveDate && !accountSuspend) {
                                        $("#errorContent").hide();
                                        html += '<div id="errorCont">'+
                                                    '<div class="subtitle nbott din-b f-little text-center f-black vcenter">'+
                                                    '<div class="tabcell">'+
                                                        'AVISO'+
                                                    '</div>'+
                                                '</div>'+

                                                '<div class="nbott din-b f-little text-center f-red m-error">En estos momentos no es posible aplicar un cambio de plan. Por favor intente nuevamente m&aacute;s tarde o contacte a un representante.</div>'+
                                            '</div>';
                                    } else if (familyPlan && !accountSuspend) {
                                        $("#errorContent").hide();
                                        html += '<div id="errorCont">'+
                                                    '<div class="subtitle nbott din-b f-little text-center f-black vcenter">'+
                                                    '<div class="tabcell">'+
                                                        'AVISO'+
                                                    '</div>'+
                                                '</div>'+

                                                '<div class="nbott din-b f-little text-center f-red m-error">Estimado cliente, su transacci&oacute;n no puede ser completada.  Favor acceder a nuestro chat para que un representante de servicio le asista en completar su solicitud.</div>'+
                                            '</div>';
                                    } else {
                                        $("#errorContent").hide();
                                        html += '<div id="errorCont">'+
                                                    '<div class="subtitle nbott din-b f-little text-center f-black vcenter">'+
                                                    '<div class="tabcell">'+
                                                        'AVISO'+
                                                    '</div>'+
                                                '</div>'+

                                                '<div class="nbott din-b f-little text-center f-red m-error">Su cuenta esta suspendida. Para activar la misma, favor realice su pago.</div>'+
                                            '</div>';
                                    }

                                    // put html
                                    $('#phone'+currentIndex).html(html);



                            },

                            // error callback
                            function(error){

                                showAlert('Error', data.Desc, 'Aceptar');

                            }
                        );

                    }else{
                        // get offers
                        offertModel.getOffers(

                            //parameters
                            soc.replace("'",''),
                            technology,
                            accountType,
                            accountSubType,
                            subscriber.PlanRate,
                            account.CreditClass,

                            //success callback
                            function(data){

                                app.utils.Storage.setSessionItem('offers', data.object);

                                subscriberModel.getSVA(
                                    //parameter
                                    app.utils.Storage.getSessionItem('token'),
                                    app.utils.Storage.getSessionItem('selected-account-value'),
                                    subscriberValue,
                                    offerID,

                                    //success callback
                                    function(data){

                                        //TOPs
                                        app.utils.Storage.setSessionItem('tops', data.TOPs);

                                        //SVAs
                                        //app.session.SVAs = data.SVAs;
                                        app.utils.Storage.setSessionItem('svas',data.SVAs);

                                        //Subscriber
                                        $.each(app.utils.Storage.getSessionItem('subscribers'), function(index, value){
                                            if(value.subscriber==subscriber){
                                                app.utils.Storage.setSessionItem('selected-subscriber-result', value);
                                            }
                                        });

                                        // New code E
                                        var accountSuspend = false;

                                        // suspend account
                                        var subscribers = app.utils.Storage.getSessionItem('subscribers');
                                        for (var i = 0; i < subscribers.length; i++) {
                                            if (subscribers[i].status == 'S') {
                                                accountSuspend = true;
                                                break;
                                            }
                                        }

                                        var now = moment().toDate(),
                                            selectedSubscriber = app.utils.Storage.getSessionItem('selected-subscriber'),
                                            nextCicleDate = moment(selectedSubscriber.PlanEffectiveDate,'MM-DD-YYYY').add(30, 'day').format('MM-DD-YYYY'),
                                            planEffectiveDate = moment(selectedSubscriber.PlanEffectiveDate,'MM-DD-YYYY').add(30, 'day').toDate(),
                                            wirelessAccount = (selectedSubscriber.ProductType=='G' || selectedSubscriber.ProductType=='C'),
                                            currentMonthSOC = now.getTime() < planEffectiveDate.getTime(),
                                            validEffectiveDate = moment(selectedSubscriber.PlanEffectiveDate,'MM-DD-YYYY').isValid(),
                                            accountSuspend = accountSuspend,
                                            familyPlan = selectedSubscriber.PlanUpdate,
                                            offers = app.utils.Storage.getSessionItem('offers'),
                                            currentIndex = $(e.currentTarget).data('index'),
                                            html = '';

                                        if(wirelessAccount &&
                                                !currentMonthSOC &&
                                                validEffectiveDate &&
                                                !accountSuspend &&
                                                !familyPlan) {

                                            // Hide error msg
                                            $("#errorCont").hide();

                                            html += '<div class="basicrow">'+
                                            '<div class="autobar">'+
                                                '<div class="container">'+
                                                    '<div class="basicrow f-little din-b f-gray">Plan Actual</div>'+

                                                    '<div class="basicrow m-top-ii">'+
                                                        '<div class="contspace f-med f-black text-left f-bold">'+
                                                            selectedSubscriber.Plan +
                                                        '</div>'+

                                                        '<div class="phonespace f-lmed f-red din-b text-right">'+
                                                            '$'+ parseFloat(selectedSubscriber.PlanRate.replace('$','')).toFixed(2) +
                                                        '</div>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</div>';

                                            if(data.TOPs != '') {
                                                html += '<div class="autobar">'+
                                                    '<div class="container">'+
                                                        '<div class="basicrow f-little din-b f-gray">Descripci&oacute;n del Plan:</div>';
                                                            $.each(data.TOPs, function (index, value) {
                                                                html += '<div class="basicrow m-top">'+
                                                                    '<div class="contspace f-med f-black text-justify f-bold">'+
                                                                        '&bullet;' + value.PlanName +
                                                                    '</div>'+
                                                                '</div>';
                                                            });
                                                    html += '</div>'+
                                                '</div>';
                                            }

                                            if(data.SVAs != '') {
                                                html += '<div class="autobar">'+
                                                    '<div class="container">'+
                                                        '<div class="basicrow f-little din-b f-gray">Servicios Adicionales</div>';
                                                            $.each(data.SVAs, function (index, value) {
                                                                html += '<div class="basicrow m-top-ii">'+
                                                                    '<div class="contspace f-med f-black text-left f-bold">'+
                                                                        value.Description +
                                                                    '</div>'+

                                                                    '<div class="phonespace f-lmed f-red din-b text-right">'+
                                                                        '$' + parseFloat(value.Rent).toFixed(2) +
                                                                    '</div>'+
                                                                '</div>';
                                                            });
                                                    html += '</div>'+
                                                '</div>';
                                            }

                                            // New format amount value
                                            var newAmountValue;
                                            var totalRate = parseFloat(selectedSubscriber.TotalRate.replace('$','')).toFixed(2);

                                            if (totalRate != null && totalRate != '' && totalRate.indexOf('.') >= 0) {
                                                newAmountValue = totalRate.split('.');
                                            } else {
                                                newAmountValue = parseFloat(app.utils.Storage.getSessionItem('selected-account').AmtDue.replace('$','')).toFixed(2).split('.');
                                            }

                                            html += '<div class="big-bal difst text-center">'+
                                                '<div class="theight">'+
                                                    /*'<span class="tmed-s din-b f-red">$</span>'+
                                                    '<span class="big-s din-b f-red">'+ parseFloat(selectedSubscriber.PlanRate.replace('$','')).toFixed(2) +'</span>'+
                                                    '<span class="tmed-s din-b f-red"><span class="tabcell">00</span></span>'+*/

                                                    '<span class="tmed-s din-b f-red">$</span>'+
                                                    '<span class="big-s din-b f-red">'+newAmountValue[0]+'</span>'+
                                                    '<span class="tmed-s din-b f-red"><span class="tabcell">'+newAmountValue[1]+'</span></span>'+
                                                '</div>'+

                                                '<div class="basicrow f-little m-bottom f-bold">Renta Mensual</div>'+
                                            '</div>';

                                            if (offers.length > 0) {

                                                html += '<div class="subtitle nbott din-b f-little text-center f-black vcenter">'+
                                                    '<div class="tabcell">'+
                                                        'CAMBIO DE PLAN'+
                                                    '</div>'+
                                                '</div>';

                                                $.each(offers, function (index, offer) {

                                                    html += '<div class="planspc input-check-container">'+
                                                        '<div class="container norel">'+
                                                            '<div class="spc-i vcenter">'+
                                                                '<div class="tabcell">'+
                                                                    '<input type="checkbox" name="checkbox_offers" id="checkbox_'+ index +'" class="css-checkbox2 css-checkbox" value="'+ offer.soc +'" data-soc-desc="'+ offer.socDescription +'" data-soc-rent="'+ parseFloat(offer.rent).toFixed(2) +'" data-soc-index="'+ index +'" /><label for="checkboxG7" class="css-label2 radGroup1"></label>'+
                                                                '</div>'+
                                                            '</div>'+

                                                            '<div class="spc-ii f-little f-bold text-justify vcenter">'+
                                                                '<div class="tabcell">'+
                                                                    offer.socDescription +
                                                                '</div>'+
                                                            '</div>'+

                                                            '<div class="spc-iii f-med text-right din-b vcenter">'+
                                                                '<div class="tabcell">'+
                                                                    '$' + parseFloat(offer.rent).toFixed(2) +
                                                                '</div>'+
                                                            '</div>'+
                                                        '</div>'+
                                                    '</div>';

                                                });

                                                html += '<div class="autobar diff-i">'+
                                                    '<div class="container">'+
                                                        '<div class="r-btn text-center f-med f-white vcenter">'+
                                                            '<div class="tabcell" id="bth-continue">'+
                                                                'Continuar'+
                                                            '</div>'+
                                                        '</div>'+
                                                    '</div>'+
                                                '</div>';

                                            } else {

                                                html += '<div class="subtitle nbott din-b f-little text-center f-black vcenter">'+
                                                    '<div class="tabcell">'+
                                                        'AVISO'+
                                                    '</div>'+
                                                '</div>';

                                                 html += '<div class="planspc input-check-container">'+
                                                        '<div class="container norel">'+
                                                            '<div class="nbott din-b f-little text-center f-red m-error"><br/<br/>No hay planes disponibles.</div>'
                                                    '</div>'+
                                                 '</div>';

                                            }

                                        html += '</div>';

                                        } else if (currentMonthSOC && validEffectiveDate && !accountSuspend) {
                                            $("#errorContent").hide();
                                            html += '<div id="errorCont">'+
                                                        '<div class="subtitle nbott din-b f-little text-center f-black vcenter">'+
                                                        '<div class="tabcell">'+
                                                            'AVISO'+
                                                        '</div>'+
                                                    '</div>'+

                                                    '<div class="nbott din-b f-little text-center f-red m-error">Estimado Cliente, s&oacute;lo es permitido un cambio de plan durante el ciclo de facturaci&oacute;n. Debe esperar hasta el '+ nextCicleDate +' para poder realizar el cambio nuevamente.</div>'+
                                                '</div>';
                                        } else if (!validEffectiveDate && !accountSuspend) {
                                            $("#errorContent").hide();
                                            html += '<div id="errorCont">'+
                                                        '<div class="subtitle nbott din-b f-little text-center f-black vcenter">'+
                                                        '<div class="tabcell">'+
                                                            'AVISO'+
                                                        '</div>'+
                                                    '</div>'+

                                                    '<div class="nbott din-b f-little text-center f-red m-top m-error">En estos momentos no es posible aplicar un cambio de plan. Por favor intente nuevamente m&aacute;s tarde o contacte a un representante.</div>'+
                                                '</div>';
                                        } else if (familyPlan && !accountSuspend) {
                                            $("#errorContent").hide();
                                            html += '<div id="errorCont">'+
                                                        '<div class="subtitle nbott din-b f-little text-center f-black vcenter">'+
                                                        '<div class="tabcell">'+
                                                            'AVISO'+
                                                        '</div>'+
                                                    '</div>'+

                                                    '<div class="nbott din-b f-little text-center f-red m-error">Estimado cliente, su transacci&oacute;n no puede ser completada.  Favor acceder a nuestro chat para que un representante de servicio le asista en completar su solicitud.</div>'+
                                                '</div>';
                                        } else if (accountSuspend) {
                                            $("#errorContent").hide();
                                            html += '<div id="errorCont">'+
                                                        '<div class="subtitle nbott din-b f-little text-center f-black vcenter">'+
                                                        '<div class="tabcell">'+
                                                            'AVISO'+
                                                        '</div>'+
                                                    '</div>'+

                                                    '<div class="nbott din-b f-little text-center f-red m-error">Su cuenta esta suspendida. Para activar la misma, favor realice su pago.</div>'+
                                                '</div>';
                                        } else {
                                            $("#errorContent").hide();
                                            html += '<div id="errorCont">'+
                                                        '<div class="subtitle nbott din-b f-little text-center f-black vcenter">'+
                                                        '<div class="tabcell">'+
                                                            'AVISO'+
                                                        '</div>'+
                                                    '</div>'+

                                                    '<div class="nbott din-b f-little text-center f-red m-error">Los cambios de plan no se pueden aplicar a suscriptores de telefon&iacute;a fija.</div>'+
                                                '</div>';
                                        }

                                        // put html
                                        $('#phone'+currentIndex).html(html);

                                    },

                                    // error function
                                    app.utils.network.errorFunction
                                );

                            },

                            // error callback
                            function(error){

                                showAlert('Error', data.Desc, 'Aceptar');

                            }
                        );
                    }

                }

            }

        },		
		
		nextStep: function (e) {
			
			var self = this,
			    socCode = $.trim($('input[type="checkbox"]:checked').val()),
				socIndex = $('input[type="checkbox"]:checked').data('socIndex'),
			 	socDesc = $('input[type="checkbox"]:checked').data('socDesc'),
			 	socRent = $('input[type="checkbox"]:checked').data('socRent'),
			 	offers = app.utils.Storage.getSessionItem('offers'),
			 	currentPlanRate = app.utils.Storage.getSessionItem('selected-subscriber').PlanRate.replace('$',''),
                offertModel = new app.models.Offer(),
                productType = $('input[type="checkbox"]:checked').data('productType'),
                contract = $('input[type="checkbox"]:checked').data('contract'),
                productId = $('input[type="checkbox"]:checked').data('productId');

			if(socCode !== ''){
				
				app.utils.Storage.setSessionItem('soc', offers[socIndex]);
				app.utils.Storage.setSessionItem('soc-code', $.trim(socCode));
				app.utils.Storage.setSessionItem('soc-desc', $.trim(socDesc));
				app.utils.Storage.setSessionItem('soc-rent', $.trim(socRent));
				app.utils.Storage.setSessionItem('product-type', $.trim(productType));
                app.utils.Storage.setSessionItem('contract', $.trim(contract));
                app.utils.Storage.setSessionItem('product-id', $.trim(productId));
                
                // create object to Credit Limit Data
                var creditLimitData = {};
                creditLimitData.ban = app.utils.Storage.getSessionItem('selected-account').Account;
                creditLimitData.productPrice = parseFloat(socRent).toFixed(2);
                creditLimitData.accountType = app.utils.Storage.getSessionItem('selected-account').mAccountType;

                self.showPasswordPrompt(function() {
                    // validateCreditLimit
                    offertModel.validateCreditLimit(
                        // parameters
                        creditLimitData,

                        // success callback
                        function(validateResponse){

                            var availableCredit = parseFloat(currentPlanRate) + parseFloat(validateResponse.AvailableCredit.replace('$',''));

                            if(!validateResponse.HasError){

                                subscriberHasAvailableCredit = availableCredit >= parseFloat(creditLimitData.productPrice);

                                if(false){ //FIXME esta línea va en la validación = !subscriberHasAvailableCredit

                                    var confirmMessage = 'Estimado cliente el plan seleccionado no califica para cargo en factura, por favor seleccione un plan de menor costo.';

                                    showAlert(

                                    'Cambio de Plan',
                                    confirmMessage,
                                    'Aceptar',

                                    function(result){
                                    });

                                } else {
                                    app.router.navigate('change_plan_2', {trigger: true});
                                }
                            }
                        },

                        // error callback
                        app.utils.network.errorFunction
                    );
                });

            } else {
				
				showAlert('Error', 'Debe seleccionar uno nuevo plan.', 'Aceptar');
				
			}

		},
		
		selectNewPlan: function(e) {
			
			// remove all selected input
			$('.input-check-container').removeClass('on');
			
			// uncheck all the inputs
			$('input[type="checkbox"].css-checkbox').each(function(){
				$(this).prop('checked', false);
			});
			
			//check the input
			$(e.currentTarget).find('input[type="checkbox"].css-checkbox').prop('checked', true);
			
			$(e.currentTarget).addClass('on');
			
		}
	
	});

});
