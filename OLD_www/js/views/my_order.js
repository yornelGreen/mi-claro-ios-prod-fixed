$(function() {

    // Sva View
    // ---------------

    app.views.MyOrderView = app.views.CommonView.extend({

        name: 'my_order',

        selectedOffer: {
            'offerId': '',
            'displayName': '',
            'price': ''
        },

        // The DOM events specific.
        events: {

            // evets
            'active': 'active',
            'pagecreate': 'pageCreate',
            

            //header
            'click .btn-back': 'back',
            'click .btn-menu': 'menu',
            'click .btn-chat': 'chat',

            'change #select-account': 'changeAccount',

            // toggle
            'click .sectbar': 'toggleClass',
            'click .phonebar': 'toggleClass',

            // menu
            'click .btn-account': 'account',
            'click .btn-consumption': 'consumption',
            'click .btn-service': 'service',
            'click .btn-change-plan': 'changePlan',
            'click .btn-add-aditional-data': 'aditionalDataPlan',
            'click .btn-device': 'device',
            'click .btn-profile': 'profile',
            'click .btn-gift': 'giftSend',
            'click .btn-invoice': 'invoice',
            'click .close-menu': 'closeMenu',
            'click .open-menu': 'openMenu',
            'click .btn-logout': 'logout',
            'click .btn-notifications': 'notifications',
            'click .btn-sva': 'sva',
            'click .btn-gift-send-recharge': 'giftSendRecharge',
			'click .btn-my-order': 'myOrder',
			'click .btn-my-store': 'myStore',

            // content
            'switchChange.bootstrapSwitch .notification-switch': 'changeRenewal',
            
            // footer
            'click #btn-help': 'helpSection'

        },

        // Render the template elements
        render: function(callback) {

            var self = this,
                variables = null;

            // user hasn't logged in
            if (app.utils.Storage.getSessionItem('token') == null) {

                document.location.href = 'index.html';

            } else {

                var subscribers = app.utils.Storage.getSessionItem('subscribers'),
                    accountPackagesInfo = app.utils.Storage.getSessionItem('accountPackagesInfo'),
                    selectedAccountValue = app.utils.Storage.getSessionItem('selected-subscriber-value'),
                    hasPackage = false,
                    hasPackageCont = false,
                    packageShow = new Object(),
                    selectedAccount = app.utils.Storage.getSessionItem('selected-account');

                $.each(accountPackagesInfo.Subscribers, function(index, value) {
                
                    if(value.Packages && value.Packages[0]) {
                        $.each(value.Packages, function(index, package) {
                            
                            if(index == 0 || ( package.OfferId != value.Packages[index-1].OfferId) ){
                                hasPackageCont = false;
                            }
                            if (hasPackageCont == false) {

                                if (package.renewal == true) {
                                    console.log("renewal true");
                                    console.log(package);
                                    packageShow[value.SubcriberNumber+'-'+package.OfferId] = {
                                        "renewal": package.renewal,
                                        "renewalActive": package.renewalActive,
                                        "renewalEnable": package.renewalEnable,
                                        "subscriber": value.SubcriberNumber,
                                    };                                                                      

                                    hasPackageCont = true;
                                    
                                } else {
                                    console.log("renewal false");
                                    console.log(package);
                                    packageShow[value.SubcriberNumber+'-'+package.OfferId] = {
                                        "renewal": package.renewal,
                                        "renewalActive": package.renewalActive,
                                        "renewalEnable": package.renewalEnable,
                                        "subscriber": value.SubcriberNumber,
                                    };                                    
                                }
                            }
                            
                            // Si no existen paquetes valido en front para mostrar alert
                            hasPackage = true;

                        });
                    }
                    console.log("arrayPackages RENDER");
                    console.log(packageShow);
                });

                $.each(packageShow, function(keys, value) {
                    $('[name="'+keys+'"]').bootstrapSwitch('state', value.renewalActive , true);                                                                            
                    // For disable button                     
                    if (!value.renewalEnable) { 
                        $('[name="'+keys+'"]').bootstrapSwitch('toggleDisabled',true,true);
                    }
                });
                                                        
                if (selectedAccountValue != null) {
                    app.utils.Storage.setSessionItem('selected-subscriber-value', selectedAccountValue);
                } else {
                    app.utils.Storage.setSessionItem('selected-subscriber-value', subscribers[0].subscriber);
                }

                var variables = {
                    accounts: app.utils.Storage.getSessionItem('accounts-list'),
                    selectedAccountValue: app.utils.Storage.getSessionItem('selected-account-value'),
                    selectedAccount: app.utils.Storage.getSessionItem('selected-account'),
                    subscribers: app.utils.Storage.getSessionItem('subscribers'),
                    accountPackagesInfo: app.utils.Storage.getSessionItem('accountPackagesInfo'),
                    selectedSubscriberValue: app.utils.Storage.getSessionItem('selected-subscriber-value'),
                    selectedOfferId: app.utils.Storage.getSessionItem('selected-offer-id'),
                    availableOffers: app.utils.Storage.getSessionItem('select-offer-to-subscriber'),
                    wirelessAccount: (selectedAccount.prodCategory == 'WLS'),
                    typeOfTelephony: app.utils.tools.typeOfTelephony,
                    hasPackages: hasPackage,
                    packageShowItems: packageShow,
                    showBackBth: true
                };

                app.TemplateManager.get(self.name, function(code) {
                    var template = cTemplate(code.html());
                    $(self.el).html(template(variables));
                    callback();
                    return this;
                });

            }

        },
        
        pageCreate: function() {
                                                        
            var self = this,
                account = app.utils.Storage.getSessionItem('selected-account'),
                notificationsState = app.utils.Storage.getSessionItem('notifications-state'),
                accountPackagesInfo = app.utils.Storage.getSessionItem('accountPackagesInfo'),
                hasPackage = false,
                hasPackageCont = false,
                packageShow = new Object(),
                subscribers = app.utils.Storage.getSessionItem('subscribers');
                               
            $.each(accountPackagesInfo.Subscribers, function(index, value) {
                if(value.Packages && value.Packages[0]) {
                    $.each(value.Packages, function(index, package) {
                        hasPackage = true;

                        if(index == 0 || ( package.OfferId != value.Packages[index-1].OfferId) ){
                            hasPackageCont = false;
                        }
                        if (hasPackageCont == false) {

                            if (package.renewal == true) {

                                packageShow[value.SubcriberNumber+'-'+package.OfferId] = {
                                    "renewal": package.renewal,
                                    "renewalActive": package.renewalActive,
                                    "renewalEnable": package.renewalEnable,
                                    "subscriber": value.SubcriberNumber,
                                };
                                
                                hasPackageCont = true;
                                
                            } else {
                                packageShow[value.SubcriberNumber+'-'+package.OfferId] = {
                                    "renewal": package.renewal,
                                    "renewalActive": package.renewalActive,
                                    "renewalEnable": package.renewalEnable,
                                    "subscriber": value.SubcriberNumber,
                                };
                            }
                        }
                    });
                }
                console.log("arrayPackages CREATE");
                console.log(packageShow);
            });

            $.each(packageShow, function(keys, value) {
                $('#'+keys).bootstrapSwitch('state', value.renewalActive , true);
                // For disable button
                if (!value.renewalEnable) {
                    $('#'+keys).bootstrapSwitch('toggleDisabled',true,true);
                }                                                                          
            });
                                                        
        },

        changeAccount: function(e){

            var self = this,
                analytics = null;

            app.utils.Storage.setSessionItem('selected-account-value',$.mobile.activePage.find('#select-account').val());
            var accountNumber = app.utils.Storage.getSessionItem('selected-account-value');
            app.utils.Storage.setSessionItem('selected-account', app.utils.Storage.getSessionItem(accountNumber));

            $(this).blur();

            if (analytics != null) {
                // send GA statistics
                analytics.trackEvent('select', 'change', 'select account number', accountNumber);
            }

            $.each(app.utils.Storage.getSessionItem('accounts-list'), function(index, value){
                if(value.Account==app.utils.Storage.getSessionItem('selected-account-value')){
                    app.utils.Storage.setSessionItem('selected-account',value);
                }
            });


            //data accountPackagesInfo
            this.options.accountModel.accountPackagesInfo(
                //parameter
                app.utils.Storage.getSessionItem('token'),
                accountNumber,

                //success callback
                function(data){
                    console.log('#success ws service');

                    if(!data.HasError){

                        app.utils.Storage.setSessionItem('accountPackagesInfo', data);

                        // render view
                        self.render(function() {
                            $.mobile.activePage.trigger('pagecreate');
                        });

                    }else{

                        showAlert('Error', data.Desc, 'Aceptar');

                    }
                },

                // error function
                app.utils.network.errorFunction
            );

        },

        changeRenewal: function(e) {
            // For disable button 
            // console.log('ingrese a changeRenewal function');
            // return 1;

            var username = app.utils.Storage.getSessionItem('username'),
                accounts = app.utils.Storage.getSessionItem('accounts-list'),
                userModel = new app.models.User(),
                switchChanged,
                switchState,
                previousState,
                jData = {},
                OfferId = $(e.currentTarget).data('offerid'),
                subscriber = $(e.currentTarget).data('subscriber'),
                baseOfferId = $(e.currentTarget).data('baseofferid'),
                switchId = $(e.currentTarget).attr('id'),
                switchValue = null,
                doNotCallData = app.utils.Storage.getSessionItem('notifications-not-to-call');

                //  ID de switch seleccionado
                switchChanged = '#'+switchId;

                switchState = $(switchChanged).bootstrapSwitch('state');

                jData.OfferId = OfferId;
                jData.subscriber = subscriber;
                jData.baseOfferId = baseOfferId;

                if (switchState == true) {
                    // Call Renewal
                    console.log('llamo a Renewal');

                    //data accountPackagesInfo
                    this.options.accountModel.subscriptionAutomaticRenewalAdd(
                        //parameter
                        app.utils.Storage.getSessionItem('token'),
                        jData,

                        //success callback
                        function(data){
                            console.log('#success ws service');

                            if(!data.HasError){

                                console.log('success callback renewal');
                                console.log(data);

                            }else{
                                console.log('error renewal');
                                console.log(data);
                                showAlert('Error', data.ErrorDesc, 'Aceptar');
                                previousState = ($(switchChanged).bootstrapSwitch('state')) ? true : false;
                                $(e.currentTarget).bootstrapSwitch('state', !previousState, true);
                            }
                        },
                        // error function
                        app.utils.network.errorFunction
                    );

                } else {
                    // Call Remove

                    //data accountPackagesInfo
                    this.options.accountModel.subscriptionAutomaticRenewalRemove(
                        //parameter
                        app.utils.Storage.getSessionItem('token'),
                        jData,

                        //success callback
                        function(data){
                            console.log('#success ws service');

                            if(!data.HasError){

                                console.log('success renewal remove');
                                console.log(data);

                            }else{
                                console.log('error renewal remove');
                                console.log(data);
                                previousState = ($(switchChanged).bootstrapSwitch('state')) ? true : false;
                                $(e.currentTarget).bootstrapSwitch('state', !previousState, true);
                                showAlert('Error', data.ErrorDesc, 'Aceptar');
                            }
                        },
                        // error function
                        app.utils.network.errorFunction
                    );
                }
        },

        changeSwitchState: function(e) {
            console.log('ingrese a changeSwitchState function');
            var switchState = $('#data-switch').bootstrapSwitch('state');

            if (switchState) {
                $('#data-switch').bootstrapSwitch('state', false, true);
            }
        },

        hasPackagesFun: function(e) {
            console.log('ingrese a hasPackagesFun function');
        },

    });
});
