$(function() {

    // Device View
    // ---------------

    app.views.DeviceView = app.views.CommonView.extend({

        name: 'device',

        deviceSwiper: null,

        // The DOM events specific.
        events: {

            // events
            'pagecreate': 'pageCreate',
            'active': 'active',
            // header
            'click .btn-back': 'menu',
            'click .btn-menu': 'menu',
            'click .btn-chat': 'chat',

            // content
            'change #select-account': 'changeAccountInfo',
            'click .select-subscriber': 'changeSubscriber',
            'click .btn-fixed-failure': 'goFixedFailureReport',
            'click #btn-pay-quota': 'goPayQuota',

            // toggle
            'click .sectbar': 'toggleClass',
            'click .phonebar': 'toggleClass',

            // menu
            'click .btn-account': 'account',
            'click .btn-consumption': 'consumption',
            'click .btn-service': 'service',
            'click .btn-change-plan': 'changePlan',
            'click .btn-add-aditional-data': 'aditionalDataPlan',
            'click .btn-device': 'device',
            'click .btn-profile': 'profile',
            'click .btn-gift': 'giftSend',
            'click .btn-invoice': 'invoice',
            'click .close-menu': 'closeMenu',
            'click .open-menu': 'openMenu',
            'click .btn-logout': 'logout',
            'click .btn-notifications': 'notifications',
            'click .btn-sva': 'sva',
            'click .btn-gift-send-recharge': 'giftSendRecharge',
			'click .btn-my-order': 'myOrder',
			'click .btn-my-store': 'myStore',

            // footer
            'click #btn-help': 'helpSection'

        },

        // Render the template elements
        render: function(callback) {

            var self = this,
                selectedAccount = app.utils.Storage.getSessionItem('selected-account');

            console.log(app.utils.Storage.getSessionItem('selected-account'));
            console.log(app.utils.Storage.getSessionItem('selected-account').prodCategory);

            // user hasn't logged in
            if (app.utils.Storage.getSessionItem('token') == null) {

                document.location.href = 'index.html';

            } else {

                var subscribers = app.utils.Storage.getSessionItem('subscribers');

                // format amount
                $.each(subscribers, function(index, value) {
                    var totalRate = value.TotalRate;
                    totalRate = totalRate.replace('$', '');

                    // New format amount value
                    var newAmountValue = totalRate.split(".");

                    value.NewAmountValue = newAmountValue;
                });

                app.utils.Storage.setSessionItem('subscribers', subscribers);

                var self = this,
                    variables = {
                        accounts: app.utils.Storage.getSessionItem('accounts-list'),
                        subscribers: app.utils.Storage.getSessionItem('subscribers'),
                        selectedAccountValue: app.utils.Storage.getSessionItem('selected-account-value'),
                        selectedSubscriber: app.utils.Storage.getSessionItem('selected-subscriber-value'),
                        wirelessAccount: (selectedAccount.prodCategory == 'WLS'),
                        typeOfTelephony: app.utils.tools.typeOfTelephony,
                        hasFailureReport: app.utils.Storage.getSessionItem('fixed-failure-report-exist'),
                        showBackBth: true
                    };

                app.TemplateManager.get(self.name, function(code) {
                    var template = cTemplate(code.html());
                    $(self.el).html(template(variables));
                    callback();
                    return this;
                });
            }
        },

        pageCreate: function(e) {
            var self = this,
                selectedAccount = app.utils.Storage.getSessionItem('selected-account');
            console.log('wirelessAccount', selectedAccount.prodCategory);
            console.log(app.utils.Storage.getSessionItem('selected-subscriber'));
        },

        goFixedFailureReport: function(e) {
            //Go to Fixed Failure Report
            app.router.navigate('fixed_failure_report', {
                trigger: true
            });
        },

        goPayQuota: function(e) {
            //Go to Pay Quotas Device
            app.router.navigate('pay_quota', {
                trigger: true
            });
        },

        changeAccountInfo: function(e) {

            var self = this,
                accountNumber = null,
                analytics = null;

            app.utils.Storage.setSessionItem('selected-account-value', $.mobile.activePage.find('#select-account').val());
            accountNumber = app.utils.Storage.getSessionItem('selected-account-value');
                                                       
            if (analytics != null) {
                // send GA statistics
                analytics.trackEvent('select', 'change', 'select account number', accountNumber);
            }

            $.each(app.utils.Storage.getSessionItem('accounts-list'), function(index, value) {
                if (value.Account == app.utils.Storage.getSessionItem('selected-account-value')) {
                    app.utils.Storage.setSessionItem('selected-account', value);
                }
            });

            if (app.utils.Storage.getSessionItem('subscribers-' + accountNumber) == null) {

                this.options.accountModel.getAccountSubscribers(
                    //parameter
                    app.utils.Storage.getSessionItem('token'),
                    app.utils.Storage.getSessionItem('selected-account-value'),

                    //success callback
                    function(data) {
                        console.log('#success ws service');

                        if (!data.HasError) {
                            // destroy the swiper
                            /*if(self.deviceSwiper != null){
                            	self.deviceSwiper.destroy();

                            }
                            self.deviceSwiper = null;*/
                            app.utils.Storage.setSessionItem('subscribers', data.Subscribers);

                            app.utils.Storage.setSessionItem('subscribers-' + accountNumber, data.Subscribers);

                            self.render(function() {
                                $.mobile.activePage.trigger('pagecreate');
                            });

                        } else {

                            showAlert('Error', data.Desc, 'Aceptar');

                        }

                    },

                    // error function
                    app.utils.network.errorFunction
                );

            } else {

                app.utils.Storage.setSessionItem('subscribers', app.utils.Storage.getSessionItem('subscribers-' + accountNumber));

                self.render(function() {
                    $.mobile.activePage.trigger('pagecreate');
                });

            }

        },

        changeSubscriber: function(e) {
            var subscriberValue = $(e.currentTarget).data('subscribers'),
                subscriberModel = new app.models.Subscriber();
                                                       
            var productType = $(e.currentTarget).data('producttype');

            app.utils.Storage.setSessionItem('fixed-failure-report-exist', false);

			var index = $(e.currentTarget).data('index');

            app.utils.Storage.setSessionItem('selected-subscriber-value', subscriberValue);

            if ($(e.currentTarget).data('search-info') == true) {
                // set flag search data
                $(e.currentTarget).data('search-info', false);
            } else {
                // set flag search data
                $(e.currentTarget).data('search-info', true);

                subscriberModel.getStatusGuarantee(
                    subscriberValue,

                    function(data) {
                        if (!data.hasError) {
                            //Status Guarantee
                            if (data.object.flag) {
                                var htmlData = '<!-- Estatus de Garantia  -->' +
                                	'<div class="autobar">' +
                                    	'<div class="container">' +
                                    		'<div class="basicrow rel gicon-pad">' +
                                                '<img width="100%" src="images/sig.png">' +
                                                '<div class="basicrow f-little din-b f-gray">Estatus de Garant&iacutea</div>' +
                                                '<div class="basicrow m-top">' +
                                                    '<div class="f-med f-black text-justify f-bold">' +
                                                        data.object.statusDescription +
                                                    '</div>' +
                                                '</div>' +
                                            '</div>' +
                                    	'</div>' +
									'</div>';
                                $('#phone' + index +  ' .statusGuarantee').html(htmlData);
                            }
                        }
                    },
                    // error function
                    app.utils.network.errorFunction
                );

                var requestFailureReport = {
                    subscriber: subscriberValue //Cablear acá para pruebas
                };
                                                       
                if (productType != 'G') {
                  subscriberModel.getFailure(
                      // parameters
                      requestFailureReport,
                      // success callback
                      function(success) {

                          if (!success.hasError) {

                            if(success.data.failureInfo.openTicket == "Y"){
                                             
                              $('#btn-fixed-failure_' +index).hide();

                              var htmlData = '<!-- Estatus de Reporte de Avería  -->' +
                                  '<div class="autobar">' +
                                  '<div class="container">' +
                                  '<div class="basicrow f-little din-b f-gray">Aver&iacute;a</div>' +
                                  '<div class="basicrow m-top">' +
                                  '<div class="f-med f-black text-left f-bold">El servicio telef&oacute;nico presenta un reporte de aver&iacute;a abierto. Para m&aacute;s informaci&oacute;n puede comunicarse con nosotros en <a href="#" class="cdef">Facebook @claroqueteayudo</a> o <a href="#" class="cdef">Twitter @claroqueteayudo.</a></div>' +
                                  '</div>' +
                                  '</div>' +
                                  '</div>';

                                  app.utils.Storage.setSessionItem('fixed-failure-report-exist', true);
                                $('#phone' + index +  ' .statusAveria').html(htmlData);
                            } else {
                              $('#btn-fixed-failure_' +index).show();
                            }
                          } else {
                              // show alert
                              showAlert(
                                  'Error',
                                  success.desc,
                                  'Aceptar',
                                  function(e) {}
                              );
                          }

                      },
                      // error callback
                      app.utils.network.errorFunction
                  );
                }

            }
        }

    });
});
