$(function() {

    // Login View
    // ---------------

    app.views.LoginView = app.views.CommonView.extend({

        name: 'login',

        // The DOM events specific.
        events: {

            // event
            'pagecreate': 'pageCreate',
            'active': 'loginTouchId',

            // content
            'click #btn-login-help': 'help',
            'click #btn-login': 'login',
            'click #btn-login-touch-id': 'loginTouchId',
            'click #btn-forgot': 'forgot',
            'click #btn-signin': 'signin',
            'click #touchId': 'clickTouchId',

            // footer
            'click #btn-help': 'helpSection'

        },

        // Render the template elements
        render: function(callback) {

            var outdatedApp = app.utils.Storage.getSessionItem('outdated-app');

            // if app is outdated
            if (outdatedApp) {

                navigator.app.exitApp();

            } else {

                var self = this,
                    user = (app.utils.Storage.getLocalItem('user') != null) ? app.utils.Storage.getLocalItem('user') : '',
                    passw = (app.utils.Storage.getLocalItem('password') != null) ? app.utils.Storage.getLocalItem('password') : '',
                    variables = {
                        login: (user != null) ? user : '',
                        password: passw,
                        remember: (app.utils.Storage.getLocalItem('remember') !== null && app.utils.Storage.getLocalItem('remember')),
                        touch: app.isTouchIdAvailable,
                        touchChecked: (app.utils.Storage.getLocalItem('touch-id') != null && app.utils.Storage.getLocalItem('touch-id'))
                    };
                var touchIdEnabled = app.utils.Storage.getSessionItem('touch-id-enabled'),
                    navegationPath = app.utils.Storage.getSessionItem('navegation-path');

                //clear session
                app.utils.Storage.clearSessionStorage();

                // set touch id variable
                if (touchIdEnabled != null) {
                    app.utils.Storage.setSessionItem('touch-id-enabled', touchIdEnabled);
                } else {
                    app.utils.Storage.setSessionItem('touch-id-enabled', true);
                }


                // set navegation path
                app.utils.Storage.setSessionItem('navegation-path', navegationPath);

                //Delete Register Data
                app.utils.Storage.removeSessionItem('register-data');
                app.utils.Storage.removeSessionItem('register-alert');

                app.TemplateManager.get(self.name, function(code) {
                    var template = cTemplate(code.html());
                    $(self.el).html(template(variables));

                    callback();
                    return this;
                });
            }

        },

        pageCreate: function() {

            var self = this,
                userLogin = null,
                password = null,
                touchId = $.mobile.activePage.find('#touchId');

            userLogin = $.mobile.activePage.find('#login').val();
            password = (app.utils.Storage.getLocalItem('password') != null) ?
                app.utils.Storage.getLocalItem('password') : $.mobile.activePage.find('#password').val();

            // remove event
            $('body').unbind('keypress');

            // set enter event
            $('body').on('keypress', function(e) {
                var touchChecked = (app.utils.Storage.getLocalItem('password') != null);
                if (e.which === 13 || e.keyCode === 13) {
                    // check login method
                    if (!touchChecked) {
                        self.login();
                    }
                }
            });

            $.mobile.activePage.find('#remember').on('change', function(e) {
                var checked = $.mobile.activePage.find('#remember').is(':checked');
                if (checked) {
                    var buttonLabels = 'Aceptar, Cancelar',
                        message = 'Usted permanecera conectado hasta que cierre la sesión';

                    showConfirm('Mantener Autenticado', message, buttonLabels,
                        function(callbackIndex) {
                            switch (callbackIndex) {
                                case 1:
                                    break;
                                case 2:
                                    $.mobile.activePage.find('#remember').prop('checked', false);
                                    break;
                            }
                        }
                    );
                }
            });

            var login = $.mobile.activePage.find('#login'),
                password = $.mobile.activePage.find('#password');

            // Disable the inputs
            if (app.utils.Storage.getLocalItem('password') !== null) {
                login.prop('disabled', true);
                password.prop('disabled', true);
            } else {
                login.prop('disabled', false);
                password.prop('disabled', false);
            }


            if (((app.utils.Storage.getSessionItem('touch-id-enabled') &&
                        app.utils.Storage.getLocalItem('touch-id') !== null &&
                        app.utils.Storage.getLocalItem('touch-id')) ||
                    app.utils.Storage.getLocalItem('remember')) &&
                userLogin.length > 0 && password.length > 0) {
                self.loginTouchId();
            }

        },

        help: function(element) {

            //Go to help
            app.router.navigate('help', {
                trigger: true
            });

        },

        login: function(element) {

            var userLogin = null,
                password = null,
                remember = null,
                touchId = false,
                deviceData,
                self = this;

            touchId = $.mobile.activePage.find('#touchId').is(':checked');
            remember = $.mobile.activePage.find('#remember').is(':checked');
            userLogin = $.mobile.activePage.find('#login').val().trim();
            password = (app.utils.Storage.getLocalItem('password') != null) ?
                app.utils.Storage.getLocalItem('password') : $.mobile.activePage.find('#password').val();

            // validate
            if (!userLogin.length > 0) {
                message = 'Nombre de usuario y/o contraseña incorrectos';
                showAlert('Error', message, 'Aceptar');
                return;
            } else if (!password.length > 0) {
                message = 'Nombre de usuario y/o contraseña incorrectos';
                showAlert('Error', message, 'Aceptar');
                return;
            }

            // cipher the password
            window.aes(password, function(cipherText) {

                self.options.userModel.login(userLogin, cipherText,

                    // success login function
                    function(response) {

                        if (response.hasError || response.accounts == null) {
                            var loginInput = $.mobile.activePage.find('#login'),
                                passwordInput = $.mobile.activePage.find('#password'),
                                rememberInput = $.mobile.activePage.find('#remember');

                            if (response.desc.toLowerCase().search('bloqueado') > 0) {

                                var buttonLabels = 'Recuperar,Chat,Cancelar',
                                    message = 'Estimado cliente su usuario se encuentra bloqueado';

                                showConfirm('Información', message, buttonLabels,
                                    function(callbackIndex) {
                                        switch (callbackIndex) {
                                            case 1:
                                            	cordova.InAppBrowser.open(app.registerAppUrl + '/#recovery_password_1', '_system', null);
                                                break;
                                            case 2:
                                                app.router.navigate('chat', {
                                                    trigger: true
                                                });
                                            case 3:
                                                break;
                                        }
                                    }
                                );

                            } else {

                                if (app.utils.Storage.getLocalItem('remember') != null &&
                                    app.utils.Storage.getLocalItem('remember')) {
                                    // delete credentials if the password was saved
                                    app.utils.Storage.removeLocalItem('password');
                                    app.utils.Storage.removeLocalItem('user');
                                    app.utils.Storage.removeLocalItem('remember');
                                }

                                showAlert('Error', response.desc, 'Aceptar');
                                app.router.navigate('login', {
                                    trigger: true
                                });
                            }

                            loginInput.prop('disabled', false);
                            passwordInput.prop('disabled', false);
                            rememberInput.prop('checked', false);

                        } else {

                            userLogin = userLogin.trim();
                            password = password.trim();

                            // save user info
                            var userInfo = {
                                name: response.name,
                                createdDate: response.CreatedDate,
                                resetPassword: response.ResetPassword,
                                deleted: response.deleted,
                                emailInvalid: response.emailInvalid,
                                accountUpdate: response.accountUpdate,
                                lastLogin: response.lastLogin,
                                passwordChange: response.passwordChange,
                                preRegister: response.preRegister
                            };

                            app.utils.Storage.setSessionItem('user-info', userInfo);

                            name = response.name;
                            name = name.replace('MR. ', ' ');
                            name = name.replace('MRS. ', ' ');
                            name = name.replace('MS. ', ' ');
                            name = name.substring(1);
                            name = name.toLowerCase();

                            if (name.length >= 50) {
                                name = name.substring(0, 50);
                                name = name + '...';
                            }

                            app.utils.Storage.setSessionItem('name', name.substring(0, name.indexOf(' ')));

                            // remove touch-id
                            if (!touchId) {
                                app.utils.Storage.removeLocalItem('touch-id');
                            }

                            var prepaid = null,
                                data = {
                                    LoginAccounts: [],
                                    Success: response.hasError != true,
                                    Message: response.desc,
                                    token: response.token
                                };

                            var defaultSubscriber = null;

                            $.each(response.accounts, function(index, object) {

                                var account = {
                                    Account: object.account,
                                    mAccountType: object.accountType,
                                    mAccountSubType: object.accountSubType,
                                    prodCategory: (object.subscribers[0].productType == 'G' ||
                                        object.subscribers[0].productType == 'C') ? 'WLS' : 'WRL',
                                    AmtDue: object.amtDue,
                                    cicleDateStart: object.cicleDateStart,
                                    cicleDateEnd: object.cicleDateEnd,
                                    BillDateEnd: object.billDueDate,
                                    BillCycle: object.billCycle,
                                    CreditClass: object.creditClass,
                                    Gift1GB: object.Gift1GB
                                }

                                // check if the account have a prepaid account
                                if (prepaid == null && object.accountSubType == 'P') {
                                    prepaid = true;
                                }

                                // account subscribers
                                var subscribers = [];
                                $.each(object.subscribers, function(j, subscriberObj) {

                                    var subscriber = {
                                        subscriber: subscriberObj.subscriber,
                                        status: subscriberObj.subStatus,
                                        ProductType: subscriberObj.productType
                                    };

                                    // detect suspended account
                                    if (subscriberObj.subStatus == 'S') {
                                        app.utils.Storage.setSessionItem('suspend-account-init', true);
                                    }

                                    // check default subscriber
                                    if (defaultSubscriber == null && subscriberObj.defaultSubscriber === 'Y') {
                                        defaultSubscriber = subscriberObj.subscriber;
                                    }

                                    subscribers[j] = subscriber;
                                });

                                // set subscribers
                                account.Subscribers = subscribers;

                                data.LoginAccounts[index] = account;
                            });

                            // check if default subscriber it's null
                            if (defaultSubscriber === null && response.accounts !== undefined && response.accounts !== null) {
                                defaultSubscriber = response.accounts[0].subscribers[0].subscriber;
                            }

                            var token = response.accounts[0].account + '||' +
                                defaultSubscriber +
                                '||' + moment().format('DD-MM-YYYY HH:mm:ss');

                            // set values for widget
                            window.aes(token, function(cipher) {

                                var prefs = plugins.appPreferences.iosSuite(app.appGroup);

                                // set url
                                prefs.store(
                                    // success callback
                                    function(e) {},
                                    // error callback
                                    function(e) {},
                                    app.appGroup,
                                    'api-url',
                                    app.apiUrl + 'widgets/getInvoiceInfo'
                                );

                                // store key => value pair
                                prefs.store(
                                    // success callback
                                    function(e) {},
                                    // error callback
                                    function(e) {},
                                    app.appGroup,
                                    'login',
                                    cipher
                                );

                                // set productType
                                prefs.store(
                                    // success callback
                                    function(e) {},
                                    // error callback
                                    function(e) {},
                                    app.appGroup,
                                    'productType',
                                    response.accounts[0].subscribers[0].productType
                                );

                            });

                            // remove touch id password
                            if (!touchId) {
                                app.utils.Storage.removeLocalItem('touch-id');
                            }

                            //save username on session
                            app.utils.Storage.setSessionItem('username', userLogin);

                            // save token into the session
                            app.utils.Storage.setSessionItem('token', data.token);

                            // set password timer if password doesn't exists
                            if (remember && app.utils.Storage.getLocalItem('remember') == null) {

                                // registered perpetual login
                                self.options.userModel.updatePerpetualLogin(
                                    // enable perpetual login
                                    true,
                                    // success
                                    function(successPL) {
                                        console.log('#success update perpetual value');
                                    },
                                    // error
                                    function(errorPL) {
                                        console.log('#error update perpetual value');
                                    }
                                );
                            }

                            // save user info
                            if (remember) {
                                app.utils.Storage.setLocalItem('remember', true);
                                app.utils.Storage.setLocalItem('user', userLogin);
                                app.utils.Storage.setLocalItem('password', password);

                            } else if (!remember && !touchId) {
                                app.utils.Storage.removeLocalItem('touch-id');
                                app.utils.Storage.removeLocalItem('remember');
                                app.utils.Storage.removeLocalItem('user');
                                app.utils.Storage.removeLocalItem('password');
                                app.utils.Storage.setSessionItem('password', password);

                            }

                            var now = new Date();
                            now.setTime(now.getTime() + (1000 * 60 * app.sessionPasswordTime));
                            app.utils.Storage.setSessionItem('confirmed-password-time', now.getTime());

                            // accounts
                            //app.session.AccountsList = data.LoginAccounts;
                            app.utils.Storage.setSessionItem('accounts-list', data.LoginAccounts);

                            // default account value
                            app.utils.Storage.setSessionItem('selected-account-value', data.LoginAccounts[0].Account);

                            // default account
                            app.utils.Storage.setSessionItem('selected-account', data.LoginAccounts[0]);

                            // 1 gb gift
                            $.each(data.LoginAccounts, function(i, account) {
                                var gifts = (app.utils.Storage.getSessionItem('gifts') !== null) ? app.utils.Storage.getSessionItem('gifts') : [];
                                // concat array
                                gifts = gifts.concat(account.Gift1GB);
                                // save new array
                                app.utils.Storage.setSessionItem('gifts', gifts);
                            });

                            // session
                            app.session.menuFunction = null;

                            // load account not load
                            app.utils.Storage.setSessionItem('load-accounts', true);

                            //register user device
                            self.options.userModel.registerUserDevice(

                                userLogin,
                                data.LoginAccounts[0].Subscribers[0].subscriber,

                                function(registerDevData) {

                                    // Success register in the push notification server
                                    if (!registerDevData.hasError) {
                                        app.utils.Storage.setSessionItem('enable-notifications', true);
                                    }
                                    console.log('Success APN register');
                                },

                                // error function
                                function(error) {
                                    //Not registered in the push notification server
                                    console.log('Error APN register');
                                }

                            );

                            // setup touch id
                            if (touchId && app.utils.Storage.getLocalItem('touch-id') == null) {
                                // save password
                                app.utils.Storage.setSessionItem('password', password);

                                app.router.navigate('touch_id_setup', {
                                    trigger: true
                                });
                            } else {

                                var navegationPath = app.utils.Storage.getSessionItem('navegation-path');

                                // remove from session
                                app.utils.Storage.removeSessionItem('navegation-path');

                                // widget quick access
                                switch (navegationPath) {
                                    case 'account':
                                        self.account();
                                        break;
                                    case 'invoice':
                                        self.invoice();
                                        break;
                                    case 'chat':
                                        self.chat();
                                        break;
                                    default:
                                        {

                                            var value = null;
                                            var time = window.sessionStorage.getItem('email-update-time');
                                            var now = new Date();

                                            // check if the email it's invalid
                                            if ((app.utils.Storage.getLocalItem('email-update-time') == null ||
                                                    (app.utils.Storage.getLocalItem('email-update-time') !== null &&
                                                        now.getTime() > app.utils.Storage.getLocalItem('email-update-time'))) &&
                                                response.emailInvalid === 'Y' && userInfo.passwordChange == 'N') {
                                                app.utils.Storage.removeLocalItem('email-update-time');
                                                app.router.navigate('email_update', {
                                                    trigger: true
                                                });
                                            } else if (userInfo.passwordChange == 'Y') {
                                                app.router.navigate('reset_password', {
                                                    trigger: true
                                                });
                                            } else {

                                                if (prepaid) {
                                                    app.router.navigate('passport', {
                                                        trigger: true
                                                    });
                                                } else {
                                                    app.router.navigate('menu', {
                                                        trigger: true
                                                    });
                                                }
                                            }

                                        }
                                }

                            }

                        }

                    },

                    // error function
                    function(error) {

                        // disable inputs
                        login.prop('disabled', false);
                        password.prop('disabled', false);

                        showAlert('Error', error.utils.network.errorMsg, 'Aceptar', function() {});

                    }

                ); // end login

            }); //end aes

        },

        forgot: function(e) {

            var forgot_url = app.registerAppUrl + '#recovery_password_1';
                                                      
            var browser = null;
                                                      
            // Escape, if the loader it's showing
            if (app.utils.Loader.isVisible()) {
                return;
            }
                                                      
            if (forgot_url != '') {
                                                      
                browser = app.utils.browser.show(forgot_url, true);
                                                    
                if (navigator.notification !== undefined) {
                                                      
                    app.utils.Loader.show();
                                                      
                    browser.addEventListener('loadstop', function() {
                        // hiden loader
                        app.utils.Loader.hide();
                                             
                        // show navegator
                        browser.show();
                    });
                                                      
                    browser.addEventListener('loaderror', function() {
                        // hiden loader
                        app.utils.Loader.hide();
                                             
                        showAlert('Error', 'Hubo un error al cargar olvidó contraseña, por favor intente nuevamente', 'Aceptar');
                    });
                                                      
                      browser.addEventListener('loadstart', function(event) {
                           if (event.url.indexOf('https://miclaro.clarotodo.com/') == 0){
                               console.log('Confirmada la contraseña');
                               browser.close();
                           }
                     });
                                                      
                                                      
                }
            } else {
                                                      
                showAlert('Error', 'En este momento no está disponible olvidó contraseña', 'Aceptar');
            }
                                                      
            return false;

        },

        signin: function(e) {
                                                      
            var register_url = app.registerAppUrl + '#register';
                                                      
            var browser = null;
                                                      
            // Escape, if the loader it's showing
            if (app.utils.Loader.isVisible()) {
                return;
            }
                                                      
            if (register_url != '') {
                                                    
                browser = app.utils.browser.show(register_url, true);
                                                      
                if (navigator.notification !== undefined) {
                                                      
                    app.utils.Loader.show();
                                                      
                    browser.addEventListener('loadstop', function() {
                        // hiden loader
                        app.utils.Loader.hide();
                                             
                        // show navegator
                        browser.show();
                    });
                                                      
                    browser.addEventListener('loaderror', function() {
                        // hiden loader
                        app.utils.Loader.hide();
                                             
                        showAlert('Error', 'Hubo un error al cargar el registro, por favor intente nuevamente', 'Aceptar');
                    });
                    
                    browser.addEventListener('loadstart', function(event) {
                           if (event.url.indexOf('https://miclaro.clarotodo.com/') == 0){
                                console.log('Registrado');
                                browser.close();
                           }
                    });

                }
            } else {
                                                      
                showAlert('Error', 'En este momento no está disponible el registro', 'Aceptar');
            }
                                                      
            return false;

        },

        clickTouchId: function(e) {

            var remember = $.mobile.activePage.find('#remember'),
                touchId = $.mobile.activePage.find('#touchId'),
                login = $.mobile.activePage.find('#login'),
                password = $.mobile.activePage.find('#password');

            if (touchId.is(':checked')) {
                //remember.prop('disabled', true);
                //remember.prop('checked', true);
            } else if (app.utils.Storage.getLocalItem('user') != null) {

                //remember.prop('disabled', false);
                //remember.prop('checked', true);
                login.prop('disabled', false);
                password.prop('disabled', false);

                // change the label text
                $('#touchIdLabel').html('Configurar Touch ID');

                app.utils.Storage.removeLocalItem('touch-id');
                app.utils.Storage.removeLocalItem('user');
                app.utils.Storage.removeLocalItem('password');

                password.val('');
                login.val('');

                //remove password
                if (!remember) {
                    app.utils.Storage.removeLocalItem('user');
                    app.utils.Storage.removeLocalItem('password');
                    password.val('');
                    login.val('');
                }

            } else {
                //remember.prop('disabled', false);
                //remember.prop('checked', false);
                app.utils.Storage.removeLocalItem('touch-id');
                // change the label text
                $('#touchIdLabel').html('Configurar Touch ID');

            }

        },

        loginTouchId: function(e) {

            var self = this;

            if (app.utils.Storage.getLocalItem('touch-id') !== null && app.utils.Storage.getLocalItem('remember') === null) {

                window.plugins.touchid.verifyFingerprint(
                    // this will be shown in the native scanner popup
                    'Ponga su huella dactilar por favor',
                    // success handler: fingerprint accepted
                    function(msg) {
                        self.login();
                    },
                    // error handler with errorcode and localised reason
                    function(msg) {
                        app.utils.Storage.setSessionItem('touch-id-enabled', true);
                    }
                );

            } else {
                self.login();
            }

        }

    });

});
