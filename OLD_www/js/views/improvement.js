$(function() {

    // Suggestions View
    // ---------------

    app.views.ImprovementView = app.views.CommonView.extend({

        name: 'improvement',

        // The DOM events specific.
        events: {

            // events
            'active': 'active',

            // header
            'click .btn-back': 'back',
            'click .chat-btn': 'chat',

            //content
            'click #send-improvement': 'sendImprovement',

            // menu
            'click .btn-account': 'account',
            'click .btn-consumption': 'consumption',
            'click .btn-service': 'service',
            'click .btn-change-plan': 'changePlan',
            'click .btn-add-aditional-data': 'aditionalDataPlan',
            'click .btn-device': 'device',
            'click .btn-profile': 'profile',
            'click .btn-gift': 'giftSend',
            'click .btn-invoice': 'invoice',
            'click .close-menu': 'closeMenu',
            'click .open-menu': 'openMenu',
            'click .btn-logout': 'logout',
            'click .btn-notifications': 'notifications',
            'click .btn-sva': 'sva',
            'click .btn-gift-send-recharge': 'giftSendRecharge',
			'click .btn-my-order': 'myOrder',
			'click .btn-my-store': 'myStore',

            // focus
            'focus #improvement-comment': 'focus',
            'focusout #improvement-comment': 'focusOut',

            // footer
            'click #btn-help': 'helpSection'

        },

        // Render the template elements        
        render: function(callback) {

            //validate if logued
            var isLogued = false;
            var wirelessAccount = null;

            if (app.utils.Storage.getSessionItem('selected-account') != null) {
                isLogued = true;
                wirelessAccount = (app.utils.Storage.getSessionItem('selected-account').prodCategory == 'WLS') ? true : false;
            }

            var self = this,
                variables = {
                    profiles: app.utils.Storage.getSessionItem('profiles'),
                    isLogued: isLogued,
                    wirelessAccount: wirelessAccount,
                    showBackBth: true
                };

            app.TemplateManager.get(self.name, function(code) {
                var template = cTemplate(code.html());
                $(self.el).html(template(variables));

                callback();
                return this;
            });

        },

        sendImprovement: function(e) {

            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

            var userEmail,
                message,
                userEmail,
                message,
                currentUser,
                userId,
                parameters;

            message = $.mobile.activePage.find('#improvement-comment').val();

            //set username as userid
            if (app.utils.Storage.getSessionItem('username') != null) {
                userId = app.utils.Storage.getSessionItem('username');
            } else {
                userId = app.uuid;
            }

            // set user email
            if (app.utils.Storage.getSessionItem('profiles') != null) {
                currentUser = app.utils.Storage.getSessionItem('profiles');
                userEmail = currentUser.Email;
            } else {
                userEmail = $.mobile.activePage.find('#email').val();
            }

            // Validate network
            var networkState = navigator.connection.type;

            if (networkState == Connection.NONE) {
                showAlert('Error', 'Revisa que tengas acceso a Internet.', 'Aceptar');
            } else if (networkState == Connection.UNKNOWN) {
                showAlert('Error', 'Por el momento el servicio no está disponible, intenta más tarde. Gracias.', 'Aceptar');
            } else {
                if (userEmail != '' && message != '') {
                    // Validate email
                    if (re.test(userEmail)) {
                        // Show loader
                        app.utils.loader.show();

                        //disable send button
                        $('#send-improvement').addClass('ui-disabled');

                        parameters = {
                            'userMail': userEmail,
                            'info': {
                                userAgent: device.platform,
                                versionCode: app.build,
                                versionName: app.version,
                                soVersion: device.version,
                                userId: userId
                            },
                            'message': message
                        };

                        setTimeout(function() {
                            window.sendPostForm(

                                // url
                                app.helpURL + '/improvement/' + app.country + '/' + app.id,

                                // parameters
                                parameters,

                                //success callback
                                function(data) {

                                    // Hide loading
                                    app.utils.Loader.hide();

                                    //enable send button
                                    ($('#send-improvement').hasClass('ui-disabled')) ? $('#send-improvement').removeClass('ui-disabled'): '';

                                    app.router.navigate('success_report', {
                                        trigger: true
                                    });
                                },

                                //error callback
                                function(data) {
                                    // Hide loader
                                    app.utils.loader.hide();
                                    showAlert('Error', 'No se puede enviar su mensaje en este momento, intente más tarde', 'Aceptar');
                                }
                            );
                        }, 1000);

                    } else {
                        showAlert('Error', 'Por favor introduzca un correo electrónico válido', 'Aceptar');
                    }

                } else {
                    if (message == '') {
                        showAlert('Error', 'Campo de "Detalle tu comentario" es requerido', 'Aceptar');
                    } else if (userEmail == '') {
                        showAlert('Error', 'Campo "Correo electrónico" es requerido', 'Aceptar');
                    }

                }
            }
        }
    });
});
