$(function() {

    // Home View
    // ---------------

    app.views.InvoiceView = app.views.CommonView.extend({

        name: 'invoice',

        // The DOM events specific.
        events: {

            // evets
            'active': 'active',
            'pagecreate': 'pageCreate',
            'show.bs.popover div[data-toggle="popover"]': 'showPopOver',

            // header
            'click .btn-back': 'menu',
            'click .btn-menu': 'menu',
            'click .btn-chat': 'chat',
            'click #btn-popup-close': 'popupClose',
            'click #invoice-link': 'popupAccept',

            // content	
            'change #select-account': 'changeAccountInfo',
            'click	#btn-view': 'viewInvoice',
            'click 	#btn-bill': 'billPayment',
            'click 	#btn-debit': 'debitDirect',

            'focus input[type=text]': 'focus',
            'focus input[type=password]': 'focus',
            'focusout input[type=text]': 'focusOut',
            'focusout input[type=password]': 'focusOut',

            // menu
            'click .btn-account': 'account',
            'click .btn-consumption': 'consumption',
            'click .btn-service': 'service',
            'click .btn-change-plan': 'changePlan',
            'click .btn-add-aditional-data': 'aditionalDataPlan',
            'click .btn-device': 'device',
            'click .btn-profile': 'profile',
            'click .btn-gift': 'giftSend',
            'click .btn-invoice': 'invoice',
            'click .close-menu': 'closeMenu',
            'click .open-menu': 'openMenu',
            'click .btn-logout': 'logout',
            'click .btn-notifications': 'notifications',
            'click .btn-sva': 'sva',
            'click .btn-gift-send-recharge': 'giftSendRecharge',
			'click .btn-my-order': 'myOrder',
			'click .btn-my-store': 'myStore',

            // footer
            'click #btn-help': 'helpSection'

        },


        // Render the template elements        
        render: function(callback) {

            // user hasn't logged in
            if (app.utils.Storage.getSessionItem('token') == null) {

                document.location.href = 'index.html';

            } else {

                var self = this,
                    variables = {
                        accounts: app.utils.Storage.getSessionItem('accounts-list'),
                        selectedAccountValue: app.utils.Storage.getSessionItem('selected-account-value'),
                        accountInfo: app.utils.Storage.getSessionItem('accounts-bill-info'),
                        amtDue: this.formatNumber(app.utils.Storage.getSessionItem('accounts-bill-info').AmtDue),
                        wirelessAccount: (app.utils.Storage.getSessionItem('selected-account').prodCategory == 'WLS') ? true : false,
                        showBackBth: true
                    };

                app.TemplateManager.get(self.name, function(code) {
                    var template = cTemplate(code.html());
                    $(self.el).html(template(variables));

                    callback();
                    return this;
                });
            }
        },

        pageCreate: function(e) {

            // allow only number and dot
            $.mobile.activePage.on('input', '#due_amount', function() {
                this.value = this.value.replace(/[^\d\.]/g, '') //Replace non-numeric with ''
                    .replace(/\./, 'x') //Replace first . with x
                    .replace(/\./g, '') //Replace remaining . with ''
                    .replace(/x/, '.'); //Replace x with .
            });

            // remove when amount  is zero 
            $.mobile.activePage.on('focus', '#due_amount', function() {
                if (this.value == 0) {
                    this.value = '';
                }
            });

            // add zero 
            $.mobile.activePage.on('focusout', '#due_amount', function() {
                if (this.value == '') {
                    this.value = '0.00';
                }
            });

            // enable tooltips
            $('[data-toggle="popover"]').popover({
                animation: false
            });

        },

        changeAccountInfo: function(e) {

            var self = this;
            app.utils.Storage.setSessionItem('selected-account-value', $.mobile.activePage.find('#select-account').val());
            $(this).blur();

            $.each(app.utils.Storage.getSessionItem('accounts-list'), function(index, value) {
                if (value.Account == app.utils.Storage.getSessionItem('selected-account-value')) {
                    app.utils.Storage.setSessionItem('selected-account', value);
                }
            });

            //data invoice
            self.options.accountModel.getAccountBill(
                //parameters
                app.utils.Storage.getSessionItem('token'),
                app.utils.Storage.getSessionItem('selected-account-value'),

                //success callback
                function(data) {

                    if (!data.HasError) {
                        // account info
                        //app.session.AccountsBillInfo = data;

                        // send GA statistics
                        analytics.trackEvent('select', 'change', 'select account number on invoice');

                        app.utils.Storage.setSessionItem('accounts-bill-info', data);

                        self.render(function() {
                            $.mobile.activePage.trigger('pagecreate');
                        });

                    } else {
                        showAlert('Error', data.Desc, 'Aceptar');
                    }

                },

                // error function
                app.utils.network.errorFunction
            );

        },

        viewInvoice: function(e) {

            var browser = null;

            // Escape, if the loader it's showing
            if (app.utils.Loader.isVisible()) {
                return;
            }
            
            var accountsBillInfo = app.utils.Storage.getSessionItem('accounts-bill-info');

            if (accountsBillInfo.pdf_url != '') {

                //browser = app.utils.browser.show(accountsBillInfo.pdf_url, true);
                browser = app.utils.browser.show('https://docs.google.com/viewer?url=' + encodeURIComponent(accountsBillInfo.pdf_url), true);

                if (navigator.notification !== undefined) {

                    app.utils.Loader.show();

                    browser.addEventListener('loadstop', function() {
                        // hiden loader
                        app.utils.Loader.hide();

                        // show navegator
                        browser.show();
                    });

                    browser.addEventListener('loaderror', function() {
                        // hiden loader
                        app.utils.Loader.hide();

                        showAlert('Error', 'Hubo un error al cargar su factura, por favor intente nuevamente', 'Aceptar');
                    });
                }
            } else {

                showAlert('Error', 'En este momento no está disponible esta factura', 'Aceptar');
            }

            return false;

        },

        debitDirect: function(e) {

            //data invoice
            var self = this;

            app.utils.Loader.show();
            
            // if (app.utils.Storage.getSessionItem('pay-debit-direct')) {
            //     app.router.navigate('confirm_debit', {
            //         trigger: true
            //     });
            // } else {



                // "780827250"
                // "692675395"
                self.options.accountModel.getDirectDebitInfo(
                    // parameters
                    app.utils.Storage.getSessionItem('token'),
                    data = {
                        'accountNumber': app.utils.Storage.getSessionItem('selected-account-value'),
                        // 'accountNumber': "780827250"
                    },
                    //success callback
                    function(data) {
                        console.log(data);
                        // if (!data.HasError) {
                        if (!data.HasError) {
//                        Falta IMPORTANTE
//                          Caso contrario se debe enviar null a pay-debit-direct
                            // data.mType = "";
                            console.log(data.mType);                            
                            var bool = (data.mType != "" && data.mType != null)? true : false;

                            // False -> No se tiene Data, entro en Form
                            // True -> Se tiene data y muestro contenido
                            
                            // app.utils.Storage.setSessionItem('pay-debit-direct-edit', bool);


                            // Data Test de un Update
                            // console.log("Llamada al servicio getDirectDebitInfo");
                            // var paydirect = { 
                            //     "ErrorDesc": "",
                            //     "ErrorNum": 0,
                            //     "ErrorType": "",
                            //     "HasError": false,
                            //     "mAccountNo": "4444",
                            //     "mBankCode": "021502095",
                            //     "mCreditCardExpDate": "",
                            //     "mCreditCardMemberName": "",
                            //     "mCreditCardNo": "",
                            //     "mCreditCardType": "",
                            //     "mEndDate": "",
                            //     "mReason": "",
                            //     "mStartDate": "2018-12-05T00:00:00.000-04:00",
                            //     "mStatus": "A",
                            //     "mType": "CH",
                            //     "paymentid": "",
                            // };
                            // console.log(paydirect);

                            // Cable test
                            // app.utils.Storage.setSessionItem('pay-debit-direct', paydirect);
                            // var bool = true;

                            // Flujo normal
                            app.utils.Storage.setSessionItem('pay-debit-direct-edit', bool);
                            app.utils.Storage.setSessionItem('pay-debit-direct', data);

                            app.router.navigate('debit_direct', {
                                trigger: true
                            });
                            // END flujo normal
                        } else {
                            // hiden loader
                            app.utils.Loader.hide();
                            
                            showAlert('Error', data.ErrorDesc, 'Aceptar');
                        }
                    },
                    // error callback
                    app.utils.network.errorFunction
                );
                // app.router.navigate('debit_direct', {
                //     trigger: true
                // });
            // }
            
        },

        billPayment: function(e) {

            var self = this,
                browser = null,
                dueAmount = parseFloat($('#due_amount').val()),
                accountsBillInfo = app.utils.Storage.getSessionItem('accounts-bill-info'),
                creditAmtDue = 0;

            // Escape, if the loader it's showing
            if (app.utils.loader.isVisible()) {
                return;
            }

            if (!$.isNumeric(dueAmount)) {
                showAlert('Error', 'El monto a pagar no es un número válido.', 'Aceptar');
                return;
            } else if (dueAmount < 5) {
                showAlert('Error', 'El monto no puede ser menor a $5.00', 'Aceptar');
                return;
            } else if (dueAmount > 500) {
                showAlert('Error', 'El monto no puede ser mayor a $500.00', 'Aceptar');
                return;
            } else if (parseFloat(selectedAccount.AmtDue) < 0) {
                //creditAmtDue = '$' + Math.abs(selectedAccount.AmtDue) + 'CR';
                creditAmtDue = Math.abs(selectedAccount.AmtDue);

                if ((creditAmtDue + dueAmount) >= 500) {
                    showAlert('Error', 'La cantidad a pagar no puede ser mayor a $500.00', 'Aceptar');
                    return;
                }

                self.doPayment();
            } else if (parseFloat(dueAmount) > parseFloat(selectedAccount.AmtDue)) {
                showConfirm(
                    'Confirmación',
                    'La cantidad ingresada es mayor al balance de su factura, la diferencia será acreditada a su cuenta.', ['Cancelar', 'Pagar'],
                    function(button) {
                        if (button == 2) {
                            self.doPayment();
                        }
                    }
                );
            } else {
                self.doPayment();
            }

            // Google analitycs track
            if (analytics != null) {
                analytics.trackEvent('button', 'click', 'view invoice');
            }

            return false;

        },

        popupAccept: function(e) {

            $('#pop-invoice').popup('close');
            app.utils.browser.show($('#invoice-link').data('paymentUrl'), true);
            return false;

        },

        popupClose: function(e) {

            $('#pop-invoice').popup('close');
            return false;

        },

        doPayment: function(e) {

            var self = this,
                browser = null,
                dueAmount = $('#due_amount').val(),
                selectedAccount = app.utils.Storage.getSessionItem('selected-account');

            this.options.accountModel.doPayment(

                //parameters
                app.utils.Storage.getSessionItem('token'),
                app.utils.Storage.getSessionItem('selected-account-value'),
                dueAmount,

                // success
                function(data) {

                    if (!data.HasError) {

                        // open payment url
                        browser = app.utils.browser.show(data.Url, true);

                        app.utils.loader.show();

                        // success event load url				
                        browser.addEventListener('loadstop', function(e) {

                            // hiden loader
                            app.utils.loader.hide();

                            // show navegator
                            browser.show();
                        });

                        // success event load url				
                        browser.addEventListener('loadstart', function(e) {

                            if (e.url == 'https://ebill.claropr.com/login/login.jsf' ||
                                e.url == 'https://ebill.claropr.com/login/home.jsf' ||
                                e.url == 'https://checkout.evertecinc.com/Close.aspx') {
                                browser.close();
                            }
                        });

                        // error event load url
                        browser.addEventListener('loaderror', function(e) {

                            // hiden loader
                            app.utils.loader.hide();

                            // close browser
                            browser.close();
                        });

                        browser.addEventListener('exit', function(e) {

                            var paymentId = data.PaymentId;

                            // get Bill info    
                            self.options.accountModel.getAccountBill(

                                //parameters
                                app.utils.Storage.getSessionItem('token'),

                                app.utils.Storage.getSessionItem('selected-account-value'),

                                //success callback
                                function(data) {

                                    if (!data.HasError) {

                                        // account bill info
                                        app.utils.Storage.setSessionItem('accounts-bill-info', data);

                                        self.render(function() {
                                            $.mobile.activePage.trigger('pagecreate');
                                        });

                                    } else {

                                        showAlert('Error', data.Desc, 'Aceptar');

                                    }

                                },

                                // error function
                                app.utils.network.errorFunction
                            );

                        });

                    } else {

                        showAlert('Error', data.Desc, 'Aceptar');

                    }

                    // send analytics statistics
                    if (analytics != null) {
                        analytics.trackEvent('button', 'click', 'billPayment button');
                    }

                },

                // error function
                app.utils.network.errorFunction

            );

        },

        showPopOver: function(e) {
            setTimeout(function() {
                $.mobile.activePage.find('[data-toggle="popover"]').popover('hide');
            }, 4000);
        }

    });

});
