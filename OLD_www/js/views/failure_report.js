$(function() {

    // Failure Report View
    // ---------------

    app.views.FailureReportView = app.views.CommonView.extend({

        name: 'failure_report',

        // The DOM events specific.
        events: {

            // header
            'click .btn-back': 'back',
            'click .chat-btn': 'chat',
            'click .tab': 'selectTab',


            //content
            'click #send-failure': 'sendFailure',

            // menu
            'click .btn-account': 'account',
            'click .btn-consumption': 'consumption',
            'click .btn-service': 'service',
            'click .btn-change-plan': 'changePlan',
            'click .btn-add-aditional-data': 'aditionalDataPlan',
            'click .btn-device': 'device',
            'click .btn-profile': 'profile',
            'click .btn-gift': 'giftSend',
            'click .btn-invoice': 'invoice',
            'click .close-menu': 'closeMenu',
            'click .open-menu': 'openMenu',
            'click .btn-logout': 'logout',
            'click .btn-notifications': 'notifications',
            'click .btn-notifications': 'notifications',
            'click .btn-sva': 'sva',
            'click .btn-gift-send-recharge': 'giftSendRecharge',
			'click .btn-my-order': 'myOrder',
			'click .btn-my-store': 'myStore',

            // focus
            'focus #failure-comment': 'focus',
            'focusout #failure-comment': 'focusOut',

            // footer
            'click #btn-help': 'helpSection'

        },

        // Render the template elements        
        render: function(callback) {

            //validate if logued
            var isLogued = false;
            var wirelessAccount = null;

            if (app.utils.Storage.getSessionItem('selected-account') != null) {
                isLogued = true;
                wirelessAccount = (app.utils.Storage.getSessionItem('selected-account').prodCategory == 'WLS') ? true : false;
            }

            var self = this,
                variables = {
                    profiles: app.utils.Storage.getSessionItem('profiles'),
                    isLogued: isLogued,
                    wirelessAccount: wirelessAccount,
                    showBackBth: true

                };

            app.TemplateManager.get(self.name, function(code) {
                var template = cTemplate(code.html());
                $(self.el).html(template(variables));
                callback();
                return this;
            });
        },

        sendFailure: function(e) {

            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

            var failureCategory,
                userEmail,
                failureComment,
                message,
                currentUser,
                userId;

            // set user message
            failureCategory = $('input[name=failure_category]:checked').val();
            failureComment = $.mobile.activePage.find('#failure-comment').val();
            message = failureCategory + '*' + failureComment;


            //set username as userid
            if (app.utils.Storage.getSessionItem('username') != null) {
                userId = app.utils.Storage.getSessionItem('username');
            } else {
                userId = app.uuid;
            }

            // set user email
            if (app.utils.Storage.getSessionItem('profiles') != null) {
                currentUser = app.utils.Storage.getSessionItem('profiles');
                userEmail = currentUser.Email;
            } else {
                userEmail = $.mobile.activePage.find('#email').val();
            }

            // Validate network
            var networkState = navigator.connection.type;

            if (networkState == Connection.NONE) {
                showAlert('Error', 'Revisa que tengas acceso a Internet.', 'Aceptar');
            } else if (networkState == Connection.UNKNOWN) {
                showAlert('Error', 'Por el momento el servicio no está disponible, intenta más tarde. Gracias.', 'Aceptar');
            } else {
                if (userEmail != '' && failureComment != '' && typeof failureCategory != 'undefined') {
                    // Validate email
                    if (re.test(userEmail)) {

                        // Show loader
                        app.utils.loader.show();

                        //disable send button
                        $('#send-failure').addClass('ui-disabled');

                        var parameters = {
                            'userMail': userEmail,
                            'info': {
                                userAgent: device.platform,
                                versionCode: app.build,
                                versionName: app.version,
                                soVersion: device.version,
                                userId: userId
                            },
                            'message': message
                        };

                        setTimeout(function() {

                            window.sendPostForm(

                                // url
                                app.helpURL + '/failure/' + app.country + '/' + app.id,

                                // parameters
                                parameters,

                                //success callback
                                function(data) {
                                    // Hide Loader
                                    app.utils.Loader.hide();

                                    //enable send button
                                    ($('#send-failure').hasClass('ui-disabled')) ? $('#send-failure').removeClass('ui-disabled'): '';

                                    app.router.navigate('success_report', {
                                        trigger: true
                                    });
                                },

                                //error callback
                                function(data) {
                                    app.utils.loader.hide();
                                    showAlert('Error', 'No se puede enviar su mensaje en este momento, intente más tarde', 'Aceptar');
                                }
                            );

                        }, 1000);

                    } else {
                        showAlert('Error', 'Por favor introduzca un correo electrónico válido', 'Aceptar');
                    }

                } else {
                    if (failureComment == '') {
                        showAlert('Error', 'Campo de "Detalle tu comentario" es requerido', 'Aceptar');
                    } else if (userEmail == '') {
                        showAlert('Error', 'Campo "Correo electrónico" es requerido', 'Aceptar');
                    } else if (typeof failureCategory == 'undefined') {
                        showAlert('Error', 'Debe seleccionar una categoría para su reporte', 'Aceptar');
                    }

                }
            }
        },

        selectTab: function(e) {
            $('.tab').removeClass('tab-on');
            $(e.currentTarget).addClass('tab-on');
            $(e.currentTarget).find('input[type="radio"]').prop('checked', true);
        }
    });
});
