$(function() {

    // Plan Change View
    // ---------------

    app.views.ChangePlan3View = app.views.CommonView.extend({

        name: 'change_plan_3',

        // The DOM events specific.
        events: {

            // header
            'click .btn-back': 'back',
            'click .btn-menu': 'menu',
            'click .btn-chat': 'chat',

            // menu
            'click .btn-account': 'account',
            'click .btn-consumption': 'consumption',
            'click .btn-service': 'service',
            'click .btn-change-plan': 'changePlan',
            'click .btn-add-aditional-data': 'aditionalDataPlan',
            'click .btn-device': 'device',
            'click .btn-profile': 'profile',
            'click .btn-gift': 'giftSend',
            'click .btn-invoice': 'invoice',
            'click .close-menu': 'closeMenu',
            'click .open-menu': 'openMenu',
            'click .btn-logout': 'logout',
            'click .btn-notifications': 'notifications',
            'click .btn-sva': 'sva',
            'click .btn-gift-send-recharge': 'giftSendRecharge',
			'click .btn-my-order': 'myOrder',
			'click .btn-my-store': 'myStore',

            // footer
            'click #btn-help': 'helpSection'

        },

        // Render the template elements        
        render: function(callback) {

            var self = this,
                user = app.utils.Storage.getLocalItem('user'),
                actualCicle = app.utils.Storage.getSessionItem('actual-cicle-date'),
                nextCicle = app.utils.Storage.getSessionItem('next-cicle-date'),
                activationDate = (app.utils.Storage.getSessionItem('soc-application') === 'now') ? actualCicle : nextCicle,
                variables = {
                    socCode: app.utils.Storage.getSessionItem('soc-code'),
                    socDesc: app.utils.Storage.getSessionItem('soc-desc'),
                    socRent: this.formatNumber(app.utils.Storage.getSessionItem('soc-rent')),
                    socDetails: app.utils.Storage.getSessionItem('soc-details'),
                    activationDate: activationDate,
                    showBackBth: true,
                    wirelessAccount: (app.utils.Storage.getSessionItem('selected-account').prodCategory == 'WLS') ? true : false
                };

            // delete from session
            //app.utils.Storage.removeSessionItem('actual-cicle-date');
            //app.utils.Storage.removeSessionItem('next-cicle-date');

            app.TemplateManager.get(self.name, function(code) {
                var template = cTemplate(code.html());
                $(self.el).html(template(variables));

                callback();
                return this;
            });

        }

    });

});
