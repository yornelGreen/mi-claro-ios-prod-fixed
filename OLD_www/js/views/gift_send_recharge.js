$(function() {

    // Gift Send R1R View
    // ---------------

    app.views.GiftSendR1RView = app.views.CommonView.extend({

        name: 'gift_send_recharge',

        // The DOM events specific.
        events: {

            //event
            'iframeload': 'resizeIframe',
            'pagecreate': 'pageCreate',

            // header
            'click .btn-back': 'back',
            'click .btn-menu': 'menu',
            'click .btn-chat': 'chat',

            // menu
            'click .btn-account': 'account',
            'click .btn-consumption': 'consumption',
            'click .btn-service': 'service',
            'click .btn-change-plan': 'changePlan',
            'click .btn-add-aditional-data': 'aditionalDataPlan',
            'click .btn-device': 'device',
            'click .btn-profile': 'profile',
            'click .btn-gift': 'giftSend',
            'click .btn-invoice': 'invoice',
            'click .close-menu': 'closeMenu',
            'click .open-menu': 'openMenu',
            'click .btn-logout': 'logout',
            'click .btn-notifications': 'notifications',
            'click .btn-gift-send-recharge': 'giftSendRecharge',

            // footer
            'click #btn-help': 'helpSection'

        },

        // Render the template elements        
        render: function(callback) {

            var self = this,
                account = app.utils.Storage.getSessionItem('selected-account'),
                channel = 'IOS',
                variables = {};

            var account = app.utils.Storage.getSessionItem('selected-account-value');
            var subscriber = app.utils.Storage.getSessionItem('selected-subscriber') != null ?
                app.utils.Storage.getSessionItem('selected-subscriber') :
                app.utils.Storage.getSessionItem('subscribers')[0].subscriber;

            variables = {
                url: app.giftSendR1R +
                    '/' + app.utils.Storage.getSessionItem('token') +
                    '/' + channel +
                    '/' + account +
                    '/' + subscriber,
                wirelessAccount: (app.utils.Storage.getSessionItem('selected-account').prodCategory == 'WLS'),
                showBackBth: true
            };

            // Show loading
            app.utils.loader.show();

            app.TemplateManager.get(self.name, function(code) {
                var template = cTemplate(code.html());
                $(self.el).html(template(variables));
                app.router.refreshPage();
                callback();
                return this;
            });


        },

        pageCreate: function(e) {

            console.log('pagecreate...');

            var self = this;

            // remove iframe event listener
            window.removeEventListener('message');

            // add iframe event listener
            window.addEventListener('message',
                function(e) {
                    if (app.giftSendR1R.indexOf(e.origin) < 0) {
                        return;
                    }

                    if (e.data == 'session') {
                        // close session
                        location.href = '#login';
                    } else {
                        // close iframe
                        self.closeIFrame();
                    }
                },
                false);

        },

        resizeIframe: function(e) {

            // hide loading
            app.utils.loader.hide();

            // Fixed bug for new version
            $('html, body, #maincont, .ui-page-active').css('height', '100%');

        },

        closeIFrame: function() {
            this.menu();
        }

    });

});
