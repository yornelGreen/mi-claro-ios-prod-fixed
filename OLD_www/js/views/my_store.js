$(function() {

    // Sva View
    // ---------------

    app.views.MyStoreView = app.views.CommonView.extend({

        name: 'my_store',

        selectedOffer: {
            'offerId': '',
            'displayName': '',
            'price': ''
        },

        // The DOM events specific.
        events: {

            // evets
            'active': 'active',
            'pagecreate': 'pageCreate',
            

            //header
            'click .btn-back': 'back',
            'click .btn-menu': 'menu',
            'click .btn-chat': 'chat',

            'change #select-account': 'changeAccount',

            // toggle
            'click .sectbar': 'toggleClass',
            'click .phonebar': 'toggleClass',

            // menu
            'click .btn-account': 'account',
            'click .btn-consumption': 'consumption',
            'click .btn-service': 'service',
            'click .btn-change-plan': 'changePlan',
            'click .btn-add-aditional-data': 'aditionalDataPlan',
            'click .btn-device': 'device',
            'click .btn-profile': 'profile',
            'click .btn-gift': 'giftSend',
            'click .btn-invoice': 'invoice',
            'click .close-menu': 'closeMenu',
            'click .open-menu': 'openMenu',
            'click .btn-logout': 'logout',
            'click .btn-notifications': 'notifications',
            'click .btn-sva': 'sva',
            'click .btn-gift-send-recharge': 'giftSendRecharge',
			'click .btn-my-order': 'myOrder',
			'click .btn-my-store': 'myStore',

            // content

            
            // footer
            'click #btn-help': 'helpSection'

        },

        // Render the template elements
        render: function(callback) {

            var self = this,
                variables = null;

            // user hasn't logged in
            if (app.utils.Storage.getSessionItem('token') == null) {
                document.location.href = 'index.html';
            } else {

                var variables = {
                    showBackBth: true
                };

                var ref2 = cordova.InAppBrowser.open('https://miclaro.clarotodo.com/', '_system', null);

                app.TemplateManager.get(self.name, function(code) {
                    var template = cTemplate(code.html());
                    $(self.el).html(template(variables));
                    callback();
                    return this;
                });

            }

        },
        
        pageCreate: function() {
                                                        
        },


    });
});
