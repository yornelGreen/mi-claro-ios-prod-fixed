$(function() {

    // Touch Id Setup View
    // ---------------

    app.views.TouchIdSetupView = app.views.CommonLogoutView.extend({

        name: 'touch_id_setup',

        // The DOM events specific.
        events: {

            // body
            'click #btn-setup': 'accept',
            'click #btn-cancel': 'cancel',

            // footer
            'click #btn-help': 'helpSection'

        },

        render: function(callback) {

            var self = this,
                subscriber = null,
                variables = {
                    login: app.utils.Storage.getSessionItem('username')
                };

            app.TemplateManager.get(self.name, function(code) {
                var template = cTemplate(code.html());
                $(self.el).html(template(variables));
                callback();
                return this;
            });

        },

        accept: function(e) {

            var username = app.utils.Storage.getSessionItem('username');
            var password = app.utils.Storage.getSessionItem('password');

            window.plugins.touchid.verifyFingerprint(
                // this will be shown in the native scanner popup
                'Ponga su huella dactilar por favor',
                // success handler: fingerprint accepted
                function(msg) {
                    app.utils.Storage.setLocalItem('user', username);
                    app.utils.Storage.setLocalItem('password', password);
                    app.utils.Storage.setLocalItem('touch-id', true);
                    app.router.navigate('menu', {
                        trigger: true
                    });
                },
                // error handler with errorcode and localised reason
                function(msg) {
                    var description = 'No fue posible autenticarlo con su huella, por favor intente nuevamente';
                    showAlert('Error', description, 'Aceptar');
                }
            );

            app.router.navigate('touch_id_setup', {
                trigger: true
            });

        },

        cancel: function(e) {

            app.router.navigate('menu', {
                trigger: true
            });

        }

    });
});