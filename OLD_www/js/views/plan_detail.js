$(function() {

    // Plan detail View
    // ---------------

    app.views.PlanDetailView = app.views.CommonView.extend({

        name: 'plan_detail',

        // The DOM events specific.
        events: {

            // events
            'active': 'active',

            // header
            'click .btn-back': 'back',
            'click .btn-menu': 'menu',
            'click .btn-chat': 'chat',

            // menu
            'click .btn-account': 'account',
            'click .btn-consumption': 'consumption',
            'click .btn-service': 'service',
            'click .btn-change-plan': 'changePlan',
            'click .btn-add-aditional-data': 'aditionalDataPlan',
            'click .btn-device': 'device',
            'click .btn-profile': 'profile',
            'click .btn-gift': 'giftSend',
            'click .btn-invoice': 'invoice',
            'click .close-menu': 'closeMenu',
            'click .open-menu': 'openMenu',
            'click .btn-logout': 'logout',
            'click .btn-notifications': 'notifications',
            'click .btn-sva': 'sva',
            'click .btn-gift-send-recharge': 'giftSendRecharge',
			'click .btn-my-order': 'myOrder',
			'click .btn-my-store': 'myStore',

            // footer
            'click #btn-help': 'helpSection'

        },

        // Render the template elements        
        render: function(callback) {

            // user hasn't logged in
            if (app.utils.Storage.getSessionItem('token') == null) {
                document.location.href = 'index.html';
            } else {

                var self = this,
                    variables = {
                        tops: app.utils.Storage.getSessionItem('tops'),
                        svas: app.utils.Storage.getSessionItem('svas'),
                        selectedSubscriberValue: app.utils.Storage.getSessionItem('selected-subscriber-result'),
                        showBackBth: true
                    };


                app.TemplateManager.get(self.name, function(code) {
                    var template = cTemplate(code.html());
                    $(self.el).html(template(variables));

                    callback();
                    return this;
                });

            }

        },

    });

});
