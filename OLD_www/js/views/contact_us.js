$(function() {

    // Contact Us View
    // ---------------

    app.views.ContactUsView = app.views.CommonView.extend({

        name: 'contact_us',

        // The DOM events specific.
        events: {

            // evets
            'active': 'active',

            // header
            'click .btn-back': 'back',
            'click .btn-menu': 'menu',
            'click .chat-btn': 'chat',

            // menu
            'click .btn-account': 'account',
            'click .btn-consumption': 'consumption',
            'click .btn-service': 'service',
            'click .btn-change-plan': 'changePlan',
            'click .btn-add-aditional-data': 'aditionalDataPlan',
            'click .btn-device': 'device',
            'click .btn-profile': 'profile',
            'click .btn-gift': 'giftSend',
            'click .btn-invoice': 'invoice',
            'click .close-menu': 'closeMenu',
            'click .open-menu': 'openMenu',
            'click .btn-logout': 'logout',
            'click .btn-notifications': 'notifications',
            'click .btn-sva': 'sva',
            'click .btn-gift-send-recharge': 'giftSendRecharge',
			'click .btn-my-order': 'myOrder',
			'click .btn-my-store': 'myStore',

            // footer
            'click #btn-help': 'helpSection'

        },

        // Render the template elements        
        render: function(callback) {

            //validate if logued
            var isLogued = false;
            var wirelessAccount = null;

            if (app.utils.Storage.getSessionItem('selected-account') != null) {
                isLogued = true;
                wirelessAccount = (app.utils.Storage.getSessionItem('selected-account').prodCategory == 'WLS') ? true : false;
            }

            var self = this,
                variables = {
                    residentialPhone: '(787) 775-0000',
                    mobilePhone: '(787) 763-3333',
                    supportPhone: '1-866-375-3375',
                    email: 'miclaro@claropr.com',
                    isLogued: isLogued,
                    wirelessAccount: wirelessAccount,
                    showBackBth: true
                };

            app.TemplateManager.get(self.name, function(code) {
                var template = cTemplate(code.html());
                $(self.el).html(template(variables));
                callback();
                return this;
            });

        },


    });
});
