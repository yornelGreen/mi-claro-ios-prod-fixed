$(function() {

    // Payment Step 2 View
    // ---------------

    app.views.PaymentStep2View = app.views.CommonView.extend({

        name: 'payment_step_2',

        // The DOM events specific.
        events: {

            // event
            'pagecreate': 'pageCreate',
            'active': 'active',

            //header
            'click .btn-back-step-1': 'backToPaymentStep1',
            'click .btn-menu': 'menu',

            // content
            'click .payment-step-3': 'goToPaymentStep3',

            // menu
            'click .btn-account': 'account',
            'click .btn-consumption': 'consumption',
            'click .btn-service': 'service',
            'click .btn-change-plan': 'changePlan',
            'click .btn-add-aditional-data': 'aditionalDataPlan',
            'click .btn-device': 'device',
            'click .btn-profile': 'profile',
            'click .btn-gift': 'giftSend',
            'click .btn-invoice': 'invoice',
            'click .close-menu': 'closeMenu',
            'click .open-menu': 'openMenu',
            'click .btn-logout': 'logout',
            'click .btn-notifications': 'notifications',
            'click .btn-sva': 'sva',
            'click select': 'fixedSelectInput',
            'click .btn-gift-send-recharge': 'giftSendRecharge',
			'click .btn-my-order': 'myOrder',
			'click .btn-my-store': 'myStore',

            // footer
            'click #btn-help': 'helpSection'

        },

        // Render the template elements
        render: function(callback) {

            var self = this;

            if (app.utils.Storage.getSessionItem('payment-data') !== null) {

                self.options.paymentModel.getStates(

                    // success callback
                    function(states) {

                        variables = {
                            selectedAccount: app.utils.Storage.getSessionItem('selected-account'),
                            selectedAccountValue: app.utils.Storage.getSessionItem('selected-account-value'),
                            paymentData: app.utils.Storage.getSessionItem('payment-data'),
                            selectedOffer: app.utils.Storage.getSessionItem('selected-offer'),
                            states: states,
                            defaultState: 'US-PR',
                            wirelessAccount: (selectedAccount.prodCategory == 'WLS'),
                            showBackBth: true
                        };

                        app.TemplateManager.get(self.name, function(code) {
                            var template = cTemplate(code.html());
                            $(self.el).html(template(variables));
                            callback();
                            return this;
                        });

                    },

                    // error callback
                    app.utils.network.errorFunction

                );

            } else {
                app.router.navigate('data_plan', {
                    trigger: true
                });
            }

        },

        pageCreate: function(e) {

            var self = this;

            $('#form-payment-step-2').validate({

                rules: {
                    'address_1': {
                        required: true
                    },
                    'state': {
                        required: true
                    },
                    'state': {
                        required: true
                    },
                    'postal_code': {
                        required: true,
                        digits: true,
                    }
                },
                messages: {
                    'address_1': {
                        required: 'Por favor indique una dirección'
                    },
                    'state': {
                        required: 'Por favor indique un estado'
                    },
                    'postal_code': {
                        required: 'Por favor indique un código postal',
                        digits: 'Por favor indique un código postal válido',
                    }

                },
                invalidHandler: function(event, validator) {

                    // show errors on alert
                    if (validator.errorList.length > 0) {
                        var err = validator.errorList[0];
                        //showAlert('Error', err.message, 'Aceptar');

                        showConfirmPopUp('Error', err.message,
                            function() {
                                hideConfirmPopUp();
                                $(err.element).focus();
                            });


                    }
                },
                submitHandler: function(form) {

                    var paymentData = {},
                        creditLimitData = {},
                        transactionData = {};

                    // get previous payment data
                    if (!_.isEmpty(app.utils.Storage.getSessionItem('payment-data'))) {
                        paymentData = app.utils.Storage.getSessionItem('payment-data');
                    }

                    // create payment data object
                    paymentData.address1 = $('input[name=address_1]').val();
                    paymentData.address2 = $('input[name=address_2]').val();
                    paymentData.state = $('input[name=state]').val();
                    paymentData.postalCode = $('input[name=postal_code]').val();
                    paymentData.amount = parseFloat(app.utils.Storage.getSessionItem('selected-offer').price.replace('$', '')).toFixed(2);
                    paymentData.accountNumber = new String(app.utils.Storage.getSessionItem('selected-account-value'));

                    // save payment data on session
                    app.utils.Storage.setSessionItem('payment-data', paymentData);

                    var offerData = {
                        'subscriberId': app.utils.Storage.getSessionItem('selected-subscriber-value'),
                        'offerId': app.utils.Storage.getSessionItem('selected-offer-id'),
                        'charge': '0',
                        'cicle': app.utils.Storage.getSessionItem('selected-account').BillCycle,
                    };


                    //add offer (plan) to subscriber on CREDIT CARD
                    self.options.offerModel.addOfferToSubscriber(

                        offerData,

                        //success callback
                        function(responseOffer) {

                            // get data payment data
                            paymentData = app.utils.Storage.getSessionItem('payment-data');

                            // FIXME
                            if (responseOffer.ResultCode == '0') {

                                // FIXME
                                paymentData.pcrftransaID = responseOffer.PCRFTransaID;

                                // Save provisioning
                                app.utils.Storage.setSessionItem('add-scubsriber-data', responseOffer);

                                // show loader
                                app.utils.loader.show();

                                // Save payment
                                self.options.paymentModel.addPayment(

                                    paymentData,

                                    // success callback
                                    function(PaymentData) {

                                        // hide loader
                                        app.utils.loader.hide();

                                        PaymentData = JSON.parse(PaymentData);

                                        // WS response with approved status
                                        if (PaymentData.error == 'true' || PaymentData.response.responseCode !== '0000') {

                                            var message = (PaymentData.error == 'true') ? PaymentData.message : PaymentData.response.message;

                                            showAlert('Error',
                                                message,
                                                'Ok',
                                                function() {}
                                            );

                                        } else {

                                            // Remove provisioning data
                                            app.utils.Storage.removeSessionItem('add-scubsriber-data');

                                            // FIXME
                                            transactionData.PCRFTransaID = responseOffer.PCRFTransaID;
                                            transactionData.transactionId = PaymentData.response.transactionId;

                                            self.options.offerModel.closeTransaction(

                                                transactionData,

                                                function(transactionResponse) {

                                                    // render payment step 3
                                                    app.router.navigate('payment_step_3', {
                                                        trigger: true,
                                                        replace: true
                                                    });

                                                },

                                                // error callback
                                                app.utils.network.errorFunction
                                            );

                                        }

                                    },

                                    // error callback
                                    app.utils.network.errorFunction

                                );

                            } else {

                                showAlert('Error',
                                    responseOffer.Description,
                                    'Ok',
                                    function() {});

                            }

                        },

                        // error callback
                        app.utils.network.errorFunction
                    );


                },

                showErrors: function(errorMap, errorList) {
                    // next  . . .
                }

            });

        },

        goToPaymentStep3: function(e) {

            $('#form-payment-step-2').submit();

        },

        backToPaymentStep1: function(e) {

            app.router.navigate('payment_step_1', {
                trigger: true,
                replace: true
            });

        }

    });
});
