$(function() {
  
  // Util For Tools
  // ---------------
  
  app.utils.Tools = {
  
		typeOfTelephony: function(productType){
            //O = wireline - fijo, I = IPTV - fijo, V = VOIP - fijo, N = ISP -fijo, J = DTH - fijo, S = DISH nuevo - fijo
            //C = cellular - móvil, G = GSM - móvil
  
            var telephony;
            if(productType=="O" || productType=="I" || productType=="V" || productType=="N" || productType=="J" || productType=="S"){
                telephony = "Fijo";
            }
            else if(productType=="C" || productType=="G"){
                telephony = "Móvil";
            }
            return telephony;
        }
  
  };
});
