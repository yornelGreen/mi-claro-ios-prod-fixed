$(function () {

    // Payment Model
    // ----------

    app.models.Payment = Backbone.Model.extend({

        initialize: function () {

        },

        getCountries: function (successCB, errorCB) {

            var jsonUrl = 'data/countries.json';

            // requesting countries JSON
            app.utils.network.requestJSON(jsonUrl, successCB, errorCB);

        },
        
        getStates: function (successCB, errorCB) {

            var jsonUrl = 'js/data/states.json';

            // requesting countries JSON
            app.utils.network.requestJSON(jsonUrl, successCB, errorCB);

        },

        addPayment: function (paymentData, successCB, errorCB) {

            var parameters = {
                cardNum: paymentData.cardNumber,
                expDate: paymentData.cardExpirationDate,
                nameOnCard: paymentData.fullName,
                amount: paymentData.amount,
                zip: paymentData.postalCode,
                street: paymentData.address1 + ' / ' + paymentData.address2,
                cvNum: paymentData.securityCode,
                aplicationID: app.gatewayAppId,
                pcrftransaID: paymentData.pcrftransaID,
                accountNumber: paymentData.accountNumber
            };
  			console.log(parameters);
            window.sendPaymentInfo(app.pcrfUrl, JSON.stringify(parameters), successCB, errorCB);

        },

        checkTransaction: function (transactionData, successCB, errorCB) {

            var method = '/payment/transaction/checkTransaction';

            var parameters = '{' +
                '"transactionId":"' + transactionData.transactionId + '",' +
                '"aplicationId":"' + transactionData.aplicationId + '"' +
                '}';


            app.utils.network.requestPCRF(method, parameters, successCB, errorCB);

        },

        addPay: function(parameters, successCB, errorCB) {
            var method = 'payment/pay',
                type = 'POST',
                authenticated = true;
                                   
            app.utils.network.requestnewAPI(method, type, JSON.stringify(parameters), authenticated, successCB, errorCB);
        }

    });

});
