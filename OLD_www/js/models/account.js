$(function() {

	// Application Model
	// ----------
	
	app.models.Account = Backbone.Model.extend({				
		
		initialize: function() {							
			
	    },

		/**********************************NEW METHODS****************************/
        getAccountsList: function (successCB, errorCB) {
            var method = 'user/accounts',
            	type = 'GET',
            	authenticated = true,
            	parameters = {};
           
            //requesting Account
            app.utils.network.requestAPI(method, type, parameters, authenticated, successCB, errorCB);
        },
        
        setDefault: function (accountNumber, subscriberNumber, successCB, errorCB) {
            var method = 'user/account/default/' + accountNumber + '/' + subscriberNumber,
            	type = 'PUT',
            	authenticated = true,
            	parameters = {};

            //setting Default Account
            app.utils.network.requestAPI(method, type, parameters, authenticated, successCB, errorCB);
        },

        deleteAccount: function (accountNumber, successCB, errorCB) {
            var method = 'user/account/' + accountNumber,
            	type = 'DELETE',
            	authenticated = true,
            	parameters = {};

            //deleting Account
            app.utils.network.requestAPI(method, type, parameters, authenticated, successCB, errorCB);
        },
        
        addAccount: function (accountNumber, ssn, defaultAccount, successCB, errorCB) {
        	var method = 'user/account',
            	type = 'POST',
            	authenticated = true,
            	parameters = '{' + '"accountNumber": "' + accountNumber + '", "ssn": "'  + ssn + '", "defaultAccount":' + defaultAccount + '}';
            	
        		console.log(parameters);
        	
            //http://184.106.10.165:9081/api-miclaro-services-dev/miclaro/user/account

            //deleting Account
            app.utils.network.requestAPI(method, type, parameters, authenticated, successCB, errorCB);
        },
        /*****************************END NEW METHODS*****************************/              
	    
	    getAccounts: function(token, successCB, errorCB){
	    	
        	if(!token){
	    		return;
	    	}

    		var method = 'Accounts',
    			parameters = '{token:"' + token+ '"}';
    		
    		// requesting Accounts
    		app.utils.network.request(method, parameters, successCB, errorCB);
     	    		
	    },
	    
	    getAccountInfo : function(token, account, successCB, errorCB){	   
	    	
	    	if(!token || !account){
	    		return;
	    	}

    		var method = 'MyAccount',
    			parameters = "{token:'" + token+ "', account: '" + account + "' }";    		

			// requesting account info
			app.utils.network.request(method, parameters, successCB, errorCB );
        	
        },
        
        getAccountUsage : function(token, account, subscriber, successCB, errorCB){
        	
        	if(!token || !account){
	    		return;
	    	}

    		var method = 'Usage',
    			parameters = "{token:'" + token+ "',account:'" + account + "',subscriber:'" + subscriber + "'}";
    		
    		// requesting account info
    		app.utils.network.request(method, parameters, successCB, errorCB);
        	
        },
        
	    getAccountBill : function(token, account, successCB, errorCB){
	    	
	    	if(!token || !account){
	    		return;
	    	}

    		var method = 'Bill',
    			parameters = "{token:'" + token+ "', account: '" + account + "'}";    		

			// requesting account info
			app.utils.network.request(method, parameters, successCB, errorCB );
        	
        },  

        getAccountSubscribers : function(token, account, successCB, errorCB){
        	
        	if(!token || !account){
	    		return;
	    	}

            var method = 'MySubscribers',
            	parameters = "{token:'" + token+ "', account: '" + account + "'}";
            
            // requesting account info
            app.utils.network.request(method, parameters, successCB, errorCB);
            
        },          
        
        doPayment : function(token, account, amount, successCB, errorCB){
        	
        	if(!token || !account){
	    		return;
	    	}
        	
    		var method = 'DoPayment',
    			parameters = "{token:'" + token + "', account: '" + account + "', amount: '" + amount + "'}";
    		
    		// requesting account info
    		app.utils.network.request(method, parameters, successCB, errorCB);
        	
        },    

        getPaymentInfo : function(token, paymentId, successCB, errorCB){
        	
        	if(!token || !paymentId){
	    		return;
	    	}
            
            var method = 'PaymentInfo',
            	parameters = "{token:'" + token+ "', PaymentId: '" + paymentId + "'}";
            
            // requesting account info
            app.utils.network.request(method, parameters, successCB, errorCB);
                
        },        
        
        validateSuscriber : function(subscriber, successCB, errorCB){
        	
        	if(!subscriber){
	    		return;
	    	}
        	
        	var method = 'RegisterValidateSubscriber',
            	parameters = "{subscriber: '" + subscriber + "'}";
            
            // requesting account info
            app.utils.network.request(method, parameters, successCB, errorCB);
        },
        
        getAccountDefaultSubscriber: function (account, successCB, errorCB) {
            var method = 'user/account/'+account+'/default-subscriber',
            	type = 'GET',
            	authenticated = true,
            	parameters = {};
           
            //requesting Account
            app.utils.network.requestAPI(method, type, parameters, authenticated, successCB, errorCB);
        },
            
        sendFailure: function (data, successCB, errorCB) {
            var method = 'fixed-fault-report/reportFailure',
            	type = 'POST',
            	authenticated = true,
            	parameters = {
                            accountNumber: data.accountNumber,
                            accountType: data.accountType,
                            accountSubType: data.accountSubType,
                            subscriber: data.subscriber,
                            token: data.token,
                            failureInfo: {
                                    type: data.failureType,
                                    detail: data.failureDetail
                            }
                        };
                                             
            //requesting Account
            app.utils.network.requestnewAPI(method, type, JSON.stringify(parameters), authenticated, successCB, errorCB);
        },
                                               
        getFailure: function (data, successCB, errorCB) {
            var method = 'fixed-fault-report/getFailure',
            	type = 'POST',
            	authenticated = true,
            	parameters = {
                            subscriber: data.subscriber
                            };
           
            //requesting Account
            app.utils.network.requestnewAPI(method, type, JSON.stringify(parameters), authenticated, successCB, errorCB);
        },
                                               
        getDirectDebitInfo: function (token, data, successCB, errorCB) {
            var method = 'GetDirectDebit',
            	type = 'POST',
            	authenticated = true,
            	parameters = {
                    token: token,
                    Ban: data.accountNumber
                };
           
            //requesting Account
            app.utils.network.newServiceV3API(method, type, JSON.stringify(parameters), authenticated, successCB, errorCB);

            // var method = 'direct-debit/getDirectDebitInfo',
            // type = 'POST',
            // authenticated = true,
            // parameters = {
            //              accountNumber: data.accountNumber
            //              };

            // app.utils.network.requestnewAPI(method, type, JSON.stringify(parameters), authenticated, successCB, errorCB);
        },
                                               
        updateDirectDebit: function (data, successCB, errorCB) {
            var method = 'UpdateDirectDebit',
            	type = 'POST',
            	authenticated = true,
            	parameters = {
                    token: data.token,
                    Ban: data.Ban,
                    mAccountNo: data.mAccountNo,
                    mBankCode: data.mBankCode,
                    mEndDate: data.mEndDate,
                    mCreditCardMemberName: data.mCreditCardMemberName,
                    mCreditCardNo: data.mCreditCardNo,
                    mCreditCardType: data.mCreditCardType,
                    mCreditCardExpDate: data.mCreditCardExpDate,
                    mReason: data.mReason,
                    mStartDate: data.mStartDate,
                    mStatus: data.mStatus,
                    mType: data.mType,                    
                };
           
            // requesting Account
            app.utils.network.newServiceV3API(method, type, JSON.stringify(parameters), authenticated, successCB, errorCB);

            // var method = 'direct-debit/updateDirectDebit',
            // type = 'POST',
            // authenticated = true,
            // parameters = {
            //              type: data.type,
            //              creditCardNo: data.creditCardNo,
            //              creditCardMemberName: data.creditCardMemberName,
            //              creditCardType: data.creditCardType,
            //              creditCardExpDate: data.creditCardExpDate,
            //              startDate: data.startDate,
            //              bankAccountNo: data.bankAccountNo,
            //              endDate: data.endDate,
            //              bankCode: data.bankCode,
            //              accountNumber: data.accountNumber
            //              };

            // //requesting Account
            // app.utils.network.requestnewAPI(method, type, JSON.stringify(parameters), authenticated, successCB, errorCB);
        },

        accountPackagesInfo: function(token, accountNumber, successCB, errorCB){

           console.log(token);
           console.log(accountNumber);
           var method = 'AccountPackagesInfo',
               type = 'POST',
               authenticated = true;

           var parameters = {
               token: token,
               Ban: accountNumber
           };


           //requesting Account
           // app.utils.network.requestAPI(method, type, JSON.stringify(parameters), authenticated, successCB, errorCB);

           // serviceRequest
           app.utils.network.newServiceV3API(method, type, JSON.stringify(parameters), authenticated, successCB, errorCB);
       },

       subscriptionAutomaticRenewalAdd: function(token, data, successCB, errorCB){

        console.log(token);
        console.log(accountNumber);
        var method = 'SubscriptionAutomaticRenewal',
            type = 'POST',
            authenticated = true;

        var parameters = {
            token: token,
            SubscriberId: data.subscriber,
            OfferID: data.OfferId,
            BaseOfferId: data.baseOfferId,
        };


        //requesting Account
        // app.utils.network.requestAPI(method, type, JSON.stringify(parameters), authenticated, successCB, errorCB);

        // serviceRequest
        app.utils.network.newServiceV3API(method, type, JSON.stringify(parameters), authenticated, successCB, errorCB);
    },

     subscriptionAutomaticRenewalRemove: function(token, data, successCB, errorCB){

        console.log(token);
        console.log(accountNumber);
        var method = 'SubscriptionAutomaticRenewalRemove',
            type = 'POST',
            authenticated = true;

        var parameters = {
            token: token,
            SubscriberId: data.subscriber,
            OfferID: data.OfferId,
            BaseOfferId: data.baseOfferId,
        };


        //requesting Account
        // app.utils.network.requestAPI(method, type, JSON.stringify(parameters), authenticated, successCB, errorCB);

        // serviceRequest
        app.utils.network.newServiceV3API(method, type, JSON.stringify(parameters), authenticated, successCB, errorCB);
    },
                                               
	});
	
});
